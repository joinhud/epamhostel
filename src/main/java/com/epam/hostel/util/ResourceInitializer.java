package com.epam.hostel.util;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.MissingResourceException;
import java.util.ResourceBundle;
/**
 * The {@code ResourceInitializer} class initializes resources
 * files in project
 *
 * @author Eugene Dubouskiy
 * @version 1.0*/
public class ResourceInitializer {
    /* Logger*/
    private static final Logger LOG = LogManager.getLogger();

    /* Property files names*/
    private static final String RESOURCE_CONFIG = "properties/config";
    private static final String RESOURCE_DATABASE = "properties/database";
    private static final String RESOURCE_TEXT = "properties/text";

    /**
     * Initialize resource files in project.
     */
    public static void init() {
        try {
            ResourceBundle.getBundle(RESOURCE_CONFIG);
            ResourceBundle.getBundle(RESOURCE_DATABASE);
            ResourceBundle.getBundle(RESOURCE_TEXT);
        } catch (MissingResourceException e) {
            LOG.log(Level.FATAL, e);
            throw new RuntimeException(e);
        }
    }
}
