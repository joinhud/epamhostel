package com.epam.hostel.util;

import javax.servlet.http.HttpServletRequest;

/**
 * The {@code ControllerUtil} class for checking request
 * values.
 *
 * @author Eugene Dubouskiy
 * @version 1.0
 */
public class ControllerUtil {
    private static final String PARAM_SUCCESS = "success";

    /**
     * Check success attribute in request
     *
     * Return {@code true} if success attribute
     * in request is {@code true}
     *
     * @param request {@code HttpServletRequest} request object
     * @return {@code true} if success attribute
     *          in request is {@code true} else
     *          return {@code false}
     *@see HttpServletRequest
     */
    public boolean checkSuccess(HttpServletRequest request) {
        boolean result = false;
        Object success = request.getAttribute(PARAM_SUCCESS);

        if (success != null) {
            result = (boolean) success;
        }

        return result;
    }
}
