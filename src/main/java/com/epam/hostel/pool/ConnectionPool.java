package com.epam.hostel.pool;

import com.epam.hostel.exception.DAOException;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.SQLException;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Connection pool class for
 *
 * Thread safe
 *
 * @author Eugene Dubouskiy
 * @version 1.0
 */
public class ConnectionPool {
    private static final Logger LOG = LogManager.getLogger();
    private static final Lock LOCK = new ReentrantLock();
    private static final String COUNT_PARAM = "pool.connections.count";
    private static final String CONFIG_PATH = "properties/config";
    private static final int DEFAULT_COUNT = 20;

    private static ConnectionPool instance;
    private static AtomicBoolean instanceCreated = new AtomicBoolean(false);
    private static AtomicInteger connectionsCount = new AtomicInteger();

    private static BlockingQueue<ProxyConnection> connections;

    private ConnectionPool() throws DAOException {
        init();
    }

    // Initialise connections in pool
    private static void init() {
        try {
            ResourceBundle rb = ResourceBundle.getBundle(CONFIG_PATH);
            int count = Integer.parseInt(rb.getString(COUNT_PARAM));
            connectionsCount.getAndSet(count > 0 ? count : DEFAULT_COUNT);
            connections = new ArrayBlockingQueue<>(connectionsCount.get());

            for (int i = 0; i < connectionsCount.get(); i++) {
                try {
                    ProxyConnection proxyConnection = new ProxyConnection(ConnectorDB.getConnection());
                    connections.offer(proxyConnection);
                } catch (DAOException e) {
                    LOG.log(Level.ERROR, e);
                }
            }
        }catch (MissingResourceException e) {
            LOG.log(Level.FATAL, e);
            throw new RuntimeException(e);
        }
    }

    // Method which return ConnectionPool object
    public static ConnectionPool getInstance() {
        if (!instanceCreated.get()) {
            try {
                LOCK.lock();

                if (instance == null) {
                    instance = new ConnectionPool();
                    instanceCreated.getAndSet(true);
                }
            } catch (DAOException e) {
                LOG.log(Level.ERROR, e);
            } finally {
                LOCK.unlock();
            }
        }
        return instance;
    }

    // Method for getting connection from pool
    public ProxyConnection getConnection() throws DAOException {
        try {
            return connections.take();
        } catch (InterruptedException e) {
            throw new DAOException(e);
        }
    }

    // Method for returning connection to pool
    public void closeConnection(ProxyConnection connection) throws DAOException {
        try {
            connections.put(connection);
        } catch (InterruptedException e) {
            throw new DAOException(e);
        }
    }

    // Method for closing all connections
    public void closeConnections() {
        int count = connectionsCount.get() > 0 ? connectionsCount.get() : DEFAULT_COUNT;

        for (int i = 0; i < count; i++) {
            try {
                connections.take().reallyClose();
            } catch (SQLException | InterruptedException e) {
                LOG.log(Level.ERROR, e);
            }
        }
    }
}
