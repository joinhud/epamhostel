package com.epam.hostel.pool;

import com.epam.hostel.exception.DAOException;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.MissingResourceException;
import java.util.Properties;
import java.util.ResourceBundle;

// Static class for setting and getting connection from database
public class ConnectorDB {
    //Logger
    private static final Logger LOG = LogManager.getLogger();

    private static final String PROPERTY_URL = "properties/database";

    private static boolean registered;

    // Method in which setting connection to database and returning connection
    public static Connection getConnection() throws DAOException {
        Connection connection = null;

        try {
            // register JDBC driver
            registerDriver();
            // getting properties from property file
            Properties properties = new Properties();
            // get settings for connection from properties file and setting them into properties object
            String url = settingPropertiesForDB(properties);
            connection = DriverManager.getConnection(url, properties);
        } catch (SQLException e) {
            throw new DAOException(e);
        }

        return connection;
    }

    private static void registerDriver() throws SQLException {
        if (!registered) {
            DriverManager.registerDriver(new com.mysql.cj.jdbc.Driver());
            registered = true;
        }
    }

    private static String settingPropertiesForDB(Properties properties) {
        String dbUrl;

        try {
            ResourceBundle bundle = ResourceBundle.getBundle(PROPERTY_URL);
            dbUrl = bundle.getString("db.url");

            properties.put("user", bundle.getString("db.user"));
            properties.put("password", bundle.getString("db.password"));
            properties.put("autoReconnect", bundle.getString("db.autoReconnect"));
            properties.put("characterEncoding", bundle.getString("db.encoding"));
            properties.put("useUnicode", bundle.getString("db.useUnicode"));
        } catch (MissingResourceException e) {
            LOG.log(Level.FATAL, e);
            throw new RuntimeException(e);
        }

        return dbUrl;
    }
}
