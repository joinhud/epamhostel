package com.epam.hostel.service;

import com.epam.hostel.dao.BookingDAO;
import com.epam.hostel.dao.RoomDAO;
import com.epam.hostel.entity.Booking;
import com.epam.hostel.entity.Room;
import com.epam.hostel.exception.DAOException;
import com.epam.hostel.exception.ServiceException;
import com.epam.hostel.pool.ConnectionPool;
import com.epam.hostel.pool.ProxyConnection;
import com.epam.hostel.type.BookingState;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class IndexService extends AbstractService {
    public List<Room> collectRoomsByType(boolean type) throws ServiceException {
        List<Room> rooms = null;
        ConnectionPool pool = null;
        ProxyConnection connection = null;

        try {
            pool = ConnectionPool.getInstance();
            connection = pool.getConnection();

            RoomDAO dao = new RoomDAO(connection);
            rooms = dao.findAllByType(type);
        } catch (DAOException e) {
            throw new ServiceException("Can not find rooms by type in database.",e);
        } finally {
            this.closePoolConnection(pool, connection);
        }

        return rooms;
    }

    public Map<Booking, String> collectProcessingTodayBookings() throws ServiceException {
        Map<Booking, String> bookings = new LinkedHashMap<>();
        ConnectionPool pool = null;
        ProxyConnection connection = null;

        try {
            pool = ConnectionPool.getInstance();
            connection = pool.getConnection();

            BookingDAO dao = new BookingDAO(connection);
            bookings = dao.findAllTodayBookingsByState(BookingState.PROCESSING);
        } catch (DAOException e) {
            throw new ServiceException("Can not collect today bookings.", e);
        } finally {
            this.closePoolConnection(pool, connection);
        }

        return bookings;
    }
}
