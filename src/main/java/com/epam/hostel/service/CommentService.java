package com.epam.hostel.service;

import com.epam.hostel.dao.BanDAO;
import com.epam.hostel.dao.CommentDAO;
import com.epam.hostel.dao.UserDAO;
import com.epam.hostel.entity.Ban;
import com.epam.hostel.entity.Comment;
import com.epam.hostel.entity.User;
import com.epam.hostel.exception.DAOException;
import com.epam.hostel.exception.ServiceException;
import com.epam.hostel.pool.ConnectionPool;
import com.epam.hostel.pool.ProxyConnection;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class CommentService extends AbstractService {
    private static final String DELETED_TYPE = "deleted";
    private static final String NOT_DELETED_TYPE = "notDeleted";
    private static final int COMMENTS_PER_PAGE = 8;

    public Map<Comment, String> collectNotDeletedComments() throws ServiceException {
        Map<Comment, String> comments = new LinkedHashMap<>();
        ConnectionPool pool = null;
        ProxyConnection connection = null;

        try {
            pool = ConnectionPool.getInstance();
            connection = pool.getConnection();

            CommentDAO commentDAO = new CommentDAO(connection);
            UserDAO userDAO = new UserDAO(connection);
            List<Comment> commentList = commentDAO.findAllNotDeleted();

            for (Comment c : commentList) {
                User user = userDAO.findEntityById(c.getUserId());

                if (user != null) {
                    comments.put(c, user.getEmail());
                }
            }
        } catch (DAOException e) {
            throw new ServiceException("Can not collect not deleted comments.", e);
        } finally {
            this.closePoolConnection(pool, connection);
        }

        return comments;
    }

    public boolean addNewComment(Comment comment) throws ServiceException {
        boolean added = false;
        ConnectionPool pool = null;
        ProxyConnection connection = null;

        try {
            pool = ConnectionPool.getInstance();
            connection = pool.getConnection();

            CommentDAO dao = new CommentDAO(connection);
            added = dao.create(comment);
        } catch (DAOException e) {
            throw new ServiceException("Can not add new comment.", e);
        } finally {
            this.closePoolConnection(pool, connection);
        }

        return added;
    }

    public List<Comment> collectCommentsByUserId(long userId) throws ServiceException {
        List<Comment> comments = null;
        ConnectionPool pool = null;
        ProxyConnection connection = null;

        try {
            pool = ConnectionPool.getInstance();
            connection = pool.getConnection();

            CommentDAO dao = new CommentDAO(connection);
            comments = dao.findCommentsByUserId(userId);
        } catch (DAOException e) {
            throw new ServiceException("Can not collect comments by id.", e);
        } finally {
            this.closePoolConnection(pool, connection);
        }

        return comments;
    }

    public boolean deleteCommentById(long commentId) throws ServiceException {
        boolean removed = false;
        ConnectionPool pool = null;
        ProxyConnection connection = null;

        try {
            pool = ConnectionPool.getInstance();
            connection = pool.getConnection();

            CommentDAO dao = new CommentDAO(connection);
            Comment comment = dao.findEntityById(commentId);
            comment.setDeleted(true);
            comment = dao.update(comment);

            if (comment != null) {
                removed = true;
            }
        } catch (DAOException e) {
            throw new ServiceException("Can not delete comment by id.", e);
        } finally {
            this.closePoolConnection(pool, connection);
        }

        return removed;
    }

    public int defineCommentsPagesCountByType(String type) throws ServiceException {
        int pagesCount = 1;
        ConnectionPool pool = null;
        ProxyConnection connection = null;

        try {
            pool = ConnectionPool.getInstance();
            connection = pool.getConnection();

            CommentDAO dao = new CommentDAO(connection);

            int commentsCount = 0;

            if (type == null || type.isEmpty()) {
                commentsCount = dao.findCountOfAllComments();
            } else {
                switch (type) {
                    case DELETED_TYPE:
                        commentsCount = dao.findCountCommentsByDeleted(true);
                        break;
                    case NOT_DELETED_TYPE:
                        commentsCount = dao.findCountCommentsByDeleted(false);
                        break;
                    default:
                        commentsCount = dao.findCountTodayComments();
                }
            }

            if (commentsCount != 0) {
                if (commentsCount % COMMENTS_PER_PAGE == 0) {
                    pagesCount = commentsCount / COMMENTS_PER_PAGE;
                } else {
                    pagesCount = (commentsCount / COMMENTS_PER_PAGE) + 1;
                }
            }
        } catch (DAOException e) {
            throw new ServiceException("Can not define pages count.", e);
        } finally {
            this.closePoolConnection(pool, connection);
        }

        return pagesCount;
    }

    public Map<Comment, String> defineCommentsByPageAndType(int page, String type) throws ServiceException {
        Map<Comment, String> comments = new LinkedHashMap<>();
        ConnectionPool pool = null;
        ProxyConnection connection = null;

        try {
            pool = ConnectionPool.getInstance();
            connection = pool.getConnection();

            CommentDAO dao = new CommentDAO(connection);
            int offset = COMMENTS_PER_PAGE * page;

            if (page >= 0) {

                if (type == null || type.isEmpty()) {
                    comments = dao.findAllLimitComments(COMMENTS_PER_PAGE, offset);
                } else {
                    switch (type) {
                        case NOT_DELETED_TYPE:
                            comments = dao.findAllLimitCommentsByDeletedValue(COMMENTS_PER_PAGE, offset, false);
                            break;
                        case DELETED_TYPE:
                            comments = dao.findAllLimitCommentsByDeletedValue(COMMENTS_PER_PAGE, offset, true);
                            break;
                        default:
                            comments = dao.findLimitTodayComments(COMMENTS_PER_PAGE, offset);
                            break;
                    }
                }
            }
        } catch (DAOException e) {
            throw new ServiceException("Can not define comments by type and page index.", e);
        } finally {
            this.closePoolConnection(pool, connection);
        }

        return comments;
    }

    public boolean changeDeletedValue(long commentId, boolean deleted) throws ServiceException {
        boolean result = false;
        ConnectionPool pool = null;
        ProxyConnection connection = null;

        try {
            pool = ConnectionPool.getInstance();
            connection = pool.getConnection();

            CommentDAO dao = new CommentDAO(connection);
            Comment comment = dao.findEntityById(commentId);
            comment.setDeleted(deleted);
            comment = dao.update(comment);

            if (comment != null) {
                result = true;
            }
        } catch (DAOException e) {
            throw new ServiceException("Can not change deleted value.", e);
        } finally {
            this.closePoolConnection(pool, connection);
        }

        return result;
    }

    public boolean isUserBaned(long id) throws ServiceException {
        boolean result = false;
        ConnectionPool pool = null;
        ProxyConnection connection = null;

        try {
            pool = ConnectionPool.getInstance();
            connection = pool.getConnection();

            BanDAO banDAO = new BanDAO(connection);
            Ban ban = banDAO.findLastBanByUserId(id);

            result = ban.getToDate().isAfter(LocalDateTime.now());
        } catch (DAOException e) {
            throw new ServiceException("Can not define user id baned.", e);
        } finally {
            this.closePoolConnection(pool, connection);
        }

        return result;
    }
}
