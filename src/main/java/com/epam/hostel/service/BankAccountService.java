package com.epam.hostel.service;

import com.epam.hostel.dao.BankAccountDAO;
import com.epam.hostel.entity.BankAccount;
import com.epam.hostel.exception.DAOException;
import com.epam.hostel.exception.ServiceException;
import com.epam.hostel.pool.ConnectionPool;
import com.epam.hostel.pool.ProxyConnection;
import org.apache.commons.codec.digest.DigestUtils;

import java.math.BigDecimal;

/**
 * The {@code BankAccountService} class
 * which include business logic for working with
 * {@code BankAccount} entity
 *
 * @author Eugene Dubouskiy
 * @version 1.0
 * @see AbstractService
 * @see BankAccount
 */
public class BankAccountService extends AbstractService {
    private static final String ACCOUNT_EXIST_ERROR = "Account with this number is already exist!";
    private static final String ERROR_PASSWORD = "Incorrect password! Try again.";
    private static final String ERROR_ENOUGH_MONEY = "Not enough money on the account!";

    /**
     * Verifies the existence of the account by account id
     *
     * @param id {@code long} value of account identification
     *
     * @return {@code String} error message if account
     *          is already exist in database
     * @throws ServiceException
     *         if can not connect to the database
     * @see ServiceException
     */
    public String isAccountExist(long id) throws ServiceException {
        String errorMsg = "";
        ConnectionPool pool = null;
        ProxyConnection connection = null;

        try {
            pool = ConnectionPool.getInstance();
            connection = pool.getConnection();

            BankAccountDAO dao = new BankAccountDAO(connection);
            BankAccount account = dao.findEntityById(id);

            if (account != null) {
                errorMsg = ACCOUNT_EXIST_ERROR;
            }
        } catch (DAOException e) {
            throw new ServiceException("Can not find bank account by id.",e);
        } finally {
            this.closePoolConnection(pool, connection);
        }

        return errorMsg;
    }

    /**
     * Create new bank account
     *
     * @param number account number
     * @param userId user identifier
     * @param money money in the account
     * @param password account password
     *
     * @return {@code true} if account was created
     *          else {@code false}
     * @throws ServiceException
     *          if can not connect to database
     *          and execute query
     * @see ServiceException
     */
    public boolean createAccount(long number, long userId, BigDecimal money, String password) throws ServiceException {
        boolean created = false;
        ConnectionPool pool = null;
        ProxyConnection connection = null;

        try {
            pool = ConnectionPool.getInstance();
            connection = pool.getConnection();

            BankAccountDAO dao = new BankAccountDAO(connection);
            String sha1Pass = DigestUtils.sha1Hex(password);
            created = dao.create(new BankAccount(number, userId, money, sha1Pass));
        } catch (DAOException e) {
            throw new ServiceException("Can not create bank account.",e);
        } finally {
            this.closePoolConnection(pool, connection);
        }

        return created;
    }

    /**
     * Checking password for bank account
     *
     * @param accountNumber account number
     * @param password password
     * @return {@code String} error message if
     *          password incorrect for account number
     * @throws ServiceException
     *          if can not connect to database
     *          and execute query
     * @see ServiceException
     */
    public String isCorrectPassword(long accountNumber, String password) throws ServiceException {
        String errorMsg = "";
        ConnectionPool pool = null;
        ProxyConnection connection = null;

        try {
            pool = ConnectionPool.getInstance();
            connection = pool.getConnection();

            BankAccountDAO dao = new BankAccountDAO(connection);
            BankAccount account = dao.findEntityById(accountNumber);

            if (account != null) {
                String sha1Password = DigestUtils.sha1Hex(password);

                if (!account.getPassword().equals(sha1Password)) {
                    errorMsg = ERROR_PASSWORD;
                }
            }
        } catch (DAOException e) {
            throw new ServiceException("Can not find bank account by id.",e);
        } finally {
            this.closePoolConnection(pool, connection);
        }

        return errorMsg;
    }

    /**
     * Withdrawing money from a bank account
     *
     * @param accountNumber account number
     * @param cost money that will be withdrawn
     * @return {@code String} error message
     *         if monew was not withdraw
     * @throws ServiceException
     *          if can not connect to database
     *          and execute query
     * @see ServiceException
     * @see BigDecimal
     * */
    public String payBooking(long accountNumber, BigDecimal cost) throws ServiceException {
        String errorMsg = "";
        ConnectionPool pool = null;
        ProxyConnection connection = null;

        try {
            pool = ConnectionPool.getInstance();
            connection = pool.getConnection();

            BankAccountDAO dao = new BankAccountDAO(connection);
            BankAccount account = dao.findEntityById(accountNumber);

            if (account != null) {
                BigDecimal money = account.getMoney().subtract(cost);

                if (money.doubleValue() < 0) {
                    errorMsg = ERROR_ENOUGH_MONEY;
                } else {
                    account.setMoney(money);
                    dao.update(account);
                }
            }
        } catch (DAOException e) {
            throw new ServiceException("The payment  transaction is not complete.",e);
        } finally {
            this.closePoolConnection(pool, connection);
        }

        return errorMsg;
    }

    /**
     * Remove bank account from database
     * by account number.
     *
     * @param number account number
     * @return {@code true} if account was removed
     *          else {@code false}
     * @throws ServiceException
     *          if can not connect to database
     *          and execute query
     * @see ServiceException
     */
    public boolean removeByAccountNumber(long number) throws ServiceException {
        boolean removed = false;
        ConnectionPool pool = null;
        ProxyConnection connection = null;

        try {
            pool = ConnectionPool.getInstance();
            connection = pool.getConnection();

            BankAccountDAO dao = new BankAccountDAO(connection);
            removed = dao.remove(number);
        } catch (DAOException e) {
            throw new ServiceException("Can not remove bank account by number.", e);
        } finally {
            this.closePoolConnection(pool, connection);
        }

        return removed;
    }

    /**
     * Define {@code BankAccount} form database
     * by account number.
     *
     * @param accountNumber account number
     * @return {@code BankAccount} if object
     *          defined by account number
     * @throws ServiceException
     *          if can not connect to database
     *          and execute query
     * @see ServiceException
     * @see BankAccount
     * */
    public BankAccount defineAccountByNumber(long accountNumber) throws ServiceException {
        BankAccount account = null;
        ConnectionPool pool = null;
        ProxyConnection connection = null;

        try {
            pool = ConnectionPool.getInstance();
            connection = pool.getConnection();

            BankAccountDAO dao = new BankAccountDAO(connection);
            account = dao.findEntityById(accountNumber);
        } catch (DAOException e) {
            throw new ServiceException("Can not define bank account by account number", e);
        } finally {
            this.closePoolConnection(pool, connection);
        }

        return account;
    }

    /**
     * Refill bank account.
     *
     * @param accountNumber bank account number
     * @param refilling refilling value
     *
     * @return {@code true} if refilled bank account
     * @throws ServiceException
     *          if can not connect to database
     *          and execute query
     * @see ServiceException
     */
    public boolean refillAccount(long accountNumber, BigDecimal refilling) throws ServiceException {
        boolean result = false;
        ConnectionPool pool = null;
        ProxyConnection connection = null;

        try {
            pool = ConnectionPool.getInstance();
            connection = pool.getConnection();

            BankAccountDAO dao = new BankAccountDAO(connection);
            BankAccount account = dao.findEntityById(accountNumber);
            BigDecimal money = account.getMoney();
            account.setMoney(money.add(refilling));
            account = dao.update(account);

            if (account != null) {
                result = true;
            }
        } catch (DAOException e) {
            throw new ServiceException("Can not refill account.", e);
        } finally {
            this.closePoolConnection(pool, connection);
        }

        return result;
    }
}
