package com.epam.hostel.service;

import com.epam.hostel.dao.UserDAO;
import com.epam.hostel.entity.User;
import com.epam.hostel.exception.DAOException;
import com.epam.hostel.exception.ServiceException;
import com.epam.hostel.pool.ConnectionPool;
import com.epam.hostel.pool.ProxyConnection;
import org.apache.commons.codec.digest.DigestUtils;

public class RegisterService extends AbstractService {
    private static final String USER_EXIST_ERROR = "User with this email is already exist!";

    public String isUserExistByEmail(String email) throws ServiceException {
        String errorMsg = "";
        ConnectionPool pool = null;
        ProxyConnection connection = null;

        try {
            pool = ConnectionPool.getInstance();
            connection = pool.getConnection();

            UserDAO dao = new UserDAO(connection);
            User user = dao.findEntityByEmail(email);
            if (user != null) {
                errorMsg = USER_EXIST_ERROR;
            }
        } catch (DAOException e) {
            throw new ServiceException("Can not define user by email address.",e);
        } finally {
            this.closePoolConnection(pool, connection);
        }

        return errorMsg;
    }

    public boolean createUser(String email, String password, String firstName, String lastName)
            throws ServiceException {
        boolean created;
        ConnectionPool pool = null;
        ProxyConnection connection = null;

        try {
            pool = ConnectionPool.getInstance();
            connection = pool.getConnection();

            UserDAO dao = new UserDAO(connection);
            String sha1Password = DigestUtils.sha1Hex(password);
            created = dao.create(new User(email, sha1Password, firstName, lastName));
        } catch (DAOException e) {
            throw new ServiceException("Can not create user with current parameters.",e);
        } finally {
            this.closePoolConnection(pool, connection);
        }

        return created;
    }
}
