package com.epam.hostel.service;

import com.epam.hostel.dao.BankAccountDAO;
import com.epam.hostel.dao.BookingDAO;
import com.epam.hostel.dao.RoomDAO;
import com.epam.hostel.entity.BankAccount;
import com.epam.hostel.entity.Booking;
import com.epam.hostel.entity.Room;
import com.epam.hostel.exception.DAOException;
import com.epam.hostel.exception.ServiceException;
import com.epam.hostel.pool.ConnectionPool;
import com.epam.hostel.pool.ProxyConnection;
import com.epam.hostel.type.BookingState;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class BookService extends AbstractService {

    private static final String ERROR_AVAILABILITY = "This room is not available on " +
            "this period with this number of livings!<br/>";
    private static final String ERROR_BANK_ACCOUNT = "Please choose the account number for payment or add new bank account!<br/>";
    private static final int BOOKINGS_PER_PAGE = 8;

    public List<BankAccount> collectAccountsByUserId(long userId) throws ServiceException {
        List<BankAccount> accounts;
        ConnectionPool pool = null;
        ProxyConnection connection = null;

        try {
            pool = ConnectionPool.getInstance();
            connection = pool.getConnection();

            BankAccountDAO dao = new BankAccountDAO(connection);
            accounts = dao.findAccountsByUserId(userId);
        } catch (DAOException e) {
            throw new ServiceException("Can not find bank account by user id.",e);
        } finally {
            this.closePoolConnection(pool, connection);
        }

        return accounts;
    }

    public String checkAvailability(long room, int countOfLivings, LocalDate fromDate, LocalDate toDate)
            throws ServiceException {
        String errorMsg = "";
        ConnectionPool pool = null;
        ProxyConnection connection = null;

        try {
            pool = ConnectionPool.getInstance();
            connection = pool.getConnection();

            BookingDAO dao = new BookingDAO(connection);
            int freeSleeps = dao.findFreeSleeps(room, fromDate, toDate);

            if (freeSleeps < countOfLivings) {
                errorMsg = ERROR_AVAILABILITY;
            }
        } catch (DAOException e) {
            throw new ServiceException("Can not find free sleeps in room.",e);
        } finally {
            this.closePoolConnection(pool, connection);
        }

        return errorMsg;
    }

    public boolean createBooking(long roomNumber, long userId,
                                 int numberOfLivings, LocalDate fromDate,
                                 LocalDate toDate, boolean paid, BigDecimal totalSum) throws ServiceException {
        boolean created = false;
        ConnectionPool pool = null;
        ProxyConnection connection = null;

        try {
            pool = ConnectionPool.getInstance();
            connection = pool.getConnection();

            BookingDAO dao = new BookingDAO(connection);
            Booking booking = new Booking(roomNumber, userId, LocalDateTime.now(),
                    numberOfLivings, fromDate, toDate, paid, totalSum, BookingState.PROCESSING);
            String date = booking.getBookingDate().toString();
            created = dao.create(booking);
        } catch (DAOException e) {
            throw new ServiceException("Can not create booking.",e);
        } finally {
            this.closePoolConnection(pool, connection);
        }

        return created;
    }

    public String checkBankAccount(String account) {
        String errorMsg = "";

        if (account == null) {
            errorMsg = ERROR_BANK_ACCOUNT;
        }

        return errorMsg;
    }

    public BigDecimal calculateTotalSum(long number, float discount, LocalDate fromDate, LocalDate toDate)
            throws ServiceException {
        BigDecimal sum = BigDecimal.ZERO;
        int days = toDate.getDayOfYear() - fromDate.getDayOfYear();
        ConnectionPool pool = null;
        ProxyConnection connection = null;

        try {
            pool = ConnectionPool.getInstance();
            connection = pool.getConnection();

            RoomDAO roomDAO = new RoomDAO(connection);
            Room room = roomDAO.findEntityById(number);

            if (room != null) {
                sum = room.getCost().multiply(new BigDecimal(days));
                sum = sum.subtract(sum.multiply(new BigDecimal(discount * 100)));
            }
        } catch (DAOException e) {
            throw new ServiceException("Can not find room by id.",e);
        } finally {
            this.closePoolConnection(pool, connection);
        }

        return sum;
    }

    public List<Booking> defineBookingsByUserId(long userId) throws ServiceException {
        List<Booking> bookings = null;
        ConnectionPool pool = null;
        ProxyConnection connection = null;

        try {
            pool = ConnectionPool.getInstance();
            connection = pool.getConnection();

            BookingDAO dao = new BookingDAO(connection);
            bookings = dao.findAllByUserId(userId);
        } catch (DAOException e) {
            throw new ServiceException("Can not find booking by users id.",e);
        } finally {
            this.closePoolConnection(pool, connection);
        }

        return bookings;
    }

    public boolean removeByBookingId(long bookingId) throws ServiceException {
        boolean removed = false;
        ConnectionPool pool = null;
        ProxyConnection connection = null;

        try {
            pool = ConnectionPool.getInstance();
            connection = pool.getConnection();

            BookingDAO dao = new BookingDAO(connection);
            removed = dao.remove(bookingId);
        } catch (DAOException e) {
            throw new ServiceException("Can not remove booking by id.", e);
        } finally {
            this.closePoolConnection(pool, connection);
        }

        return removed;
    }

    public Booking defineById(long id) throws ServiceException {
        Booking booking = null;
        ConnectionPool pool = null;
        ProxyConnection connection = null;

        try {
            pool = ConnectionPool.getInstance();
            connection = pool.getConnection();

            BookingDAO dao = new BookingDAO(connection);
            booking = dao.findEntityById(id);
        } catch (DAOException e) {
            throw new ServiceException("Can not define user by id.", e);
        } finally {
            this.closePoolConnection(pool, connection);
        }

        return booking;
    }

    public boolean returnMoney(long bankAccount, BigDecimal sum) throws ServiceException {
        boolean moneyReturned = false;
        ConnectionPool pool = null;
        ProxyConnection connection = null;

        try {
            pool = ConnectionPool.getInstance();
            connection = pool.getConnection();

            BankAccountDAO bankAccountDAO = new BankAccountDAO(connection);
            BankAccount account = bankAccountDAO.findEntityById(bankAccount);
            BigDecimal money = account.getMoney();
            account.setMoney(money.add(sum));
            account = bankAccountDAO.update(account);

            moneyReturned = account != null;
        } catch (DAOException e) {
            throw new ServiceException("Can not returned money to bank account.", e);
        } finally {
            this.closePoolConnection(pool, connection);
        }

        return moneyReturned;
    }

    public Map<Booking, String> defineBookingsByPageAndState(int page, String stateValue) throws ServiceException {
        Map<Booking, String> bookings = new LinkedHashMap<>();
        ConnectionPool pool = null;
        ProxyConnection connection = null;

        try {
            pool = ConnectionPool.getInstance();
            connection = pool.getConnection();

            BookingDAO dao = new BookingDAO(connection);

            if (page >= 0) {

                if (stateValue == null || stateValue.isEmpty()) {
                    bookings = dao.findAllLimitBookings(BOOKINGS_PER_PAGE, BOOKINGS_PER_PAGE * page);
                } else {
                    BookingState state = BookingState.valueOf(stateValue.toUpperCase());
                    bookings = dao.findLimitBookingsByState(BOOKINGS_PER_PAGE, BOOKINGS_PER_PAGE * page, state);
                }
            }
        } catch (DAOException e) {
            throw new ServiceException("Can not define bokings by page.", e);
        } finally {
            this.closePoolConnection(pool, connection);
        }

        return bookings;
    }

    public int defineBookingsPagesCountByState(String stateValue) throws ServiceException {
        int pagesCount = 1;
        ConnectionPool pool = null;
        ProxyConnection connection = null;

        try {
            pool = ConnectionPool.getInstance();
            connection = pool.getConnection();

            BookingDAO dao = new BookingDAO(connection);
            int bookingsCount = 0;

            if (stateValue == null || stateValue.isEmpty()) {
                bookingsCount = dao.findCountOfAllBookings();
            } else {
                bookingsCount = dao.findCountBookingsByState(BookingState.valueOf(stateValue.toUpperCase()));
            }

            if (bookingsCount != 0) {
                if (bookingsCount % BOOKINGS_PER_PAGE == 0) {
                    pagesCount = bookingsCount / BOOKINGS_PER_PAGE;
                } else {
                    pagesCount = (bookingsCount / BOOKINGS_PER_PAGE) + 1;
                }
            }
        } catch (DAOException e) {
            throw new ServiceException("Can not define bookings pages count", e);
        } finally {
            this.closePoolConnection(pool, connection);
        }

        return pagesCount;
    }

    public boolean updateBookingState(long id, BookingState state) throws ServiceException {
        boolean updated = false;
        ConnectionPool pool = null;
        ProxyConnection connection = null;

        try {
            pool = ConnectionPool.getInstance();
            connection = pool.getConnection();

            BookingDAO dao = new BookingDAO(connection);
            Booking booking = dao.findEntityById(id);
            booking.setState(state);
            booking = dao.update(booking);

            if (booking != null) {
                updated = true;
            }
        } catch (DAOException e) {
            throw new ServiceException("Can not update booking state.", e);
        } finally {
            this.closePoolConnection(pool, connection);
        }

        return updated;
    }
}
