package com.epam.hostel.service;

import com.epam.hostel.dao.BanDAO;
import com.epam.hostel.dao.UserDAO;
import com.epam.hostel.entity.Ban;
import com.epam.hostel.entity.User;
import com.epam.hostel.exception.DAOException;
import com.epam.hostel.exception.ServiceException;
import com.epam.hostel.pool.ConnectionPool;
import com.epam.hostel.pool.ProxyConnection;

import java.util.List;

public class ProfileService extends AbstractService {
    public boolean updateUserData(User user) throws ServiceException {
        boolean updated = false;
        ConnectionPool pool = null;
        ProxyConnection connection = null;

        try {
            pool = ConnectionPool.getInstance();
            connection = pool.getConnection();

            UserDAO dao = new UserDAO(connection);
            User updatedUser = dao.update(user);

            updated = updatedUser != null;
        } catch (DAOException e) {
            throw new ServiceException("Can not update user data.", e);
        } finally {
            this.closePoolConnection(pool, connection);
        }

        return updated;
    }

    public List<Ban> defineBlockingByUserId(long userId) throws ServiceException {
        List<Ban> blocking = null;
        ConnectionPool pool = null;
        ProxyConnection connection = null;

        try {
            pool = ConnectionPool.getInstance();
            connection = pool.getConnection();

            BanDAO dao = new BanDAO(connection);
            blocking = dao.findBansByUserId(userId);
        } catch (DAOException e) {
            throw new ServiceException("Can not define blocking by user id", e);
        } finally {
            this.closePoolConnection(pool, connection);
        }

        return blocking;
    }
}
