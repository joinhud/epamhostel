package com.epam.hostel.service;

import com.epam.hostel.dao.UserDAO;
import com.epam.hostel.entity.User;
import com.epam.hostel.exception.DAOException;
import com.epam.hostel.exception.ServiceException;
import com.epam.hostel.pool.ConnectionPool;
import com.epam.hostel.pool.ProxyConnection;
import org.apache.commons.codec.digest.DigestUtils;

public class SignInService extends AbstractService {
    private static final String EMAIL_ERROR_EXIST_MSG = "This email does not exist!";
    private static final String PASSWORD_ERROR_MSG = "The password does not match the email!";
    private static final String ADMIN_EMAIL = "admin@admin.com";

    public String isEmailExist(String email) throws ServiceException {
        String errorMsg = "";

        if (defineUserByEmail(email) == null) {
            errorMsg = EMAIL_ERROR_EXIST_MSG;
        }

        return errorMsg;
    }

    public String isCorrectPassword(String email, String password) throws ServiceException {
        String errorMsg = "";

        User user = defineUserByEmail(email);

        if (user != null) {
            String sha1UserPass = DigestUtils.sha1Hex(password);

            if (!user.getPassword().equals(sha1UserPass)) {
                errorMsg = PASSWORD_ERROR_MSG;
            }
        }

        return errorMsg;
    }

    public User defineUserByEmail(String email) throws ServiceException {
        User user = null;
        ConnectionPool pool = null;
        ProxyConnection connection = null;

        try {
            pool = ConnectionPool.getInstance();
            connection = pool.getConnection();

            UserDAO dao = new UserDAO(connection);
            user = dao.findEntityByEmail(email);
        } catch (DAOException e) {
            throw new ServiceException("Can not define User by email address.",e);
        } finally {
            this.closePoolConnection(pool, connection);
        }

        return user;
    }

    public boolean isAdmin(String email) {
        return ADMIN_EMAIL.equals(email);
    }
}
