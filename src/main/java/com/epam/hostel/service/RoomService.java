package com.epam.hostel.service;

import com.epam.hostel.dao.RoomDAO;
import com.epam.hostel.entity.Room;
import com.epam.hostel.exception.DAOException;
import com.epam.hostel.exception.ServiceException;
import com.epam.hostel.pool.ConnectionPool;
import com.epam.hostel.pool.ProxyConnection;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import java.io.File;
import java.math.BigDecimal;
import java.util.List;

public class RoomService extends AbstractService {
    private static final int MEMORY_THRESHOLD = 1024 * 1024 * 3;  // 3MB
    private static final int MAX_FILE_SIZE = 1024 * 1024 * 40; // 40MB
    private static final int MAX_REQUEST_SIZE = 1024 * 1024 * 50; // 50MB

    private static final String ERROR_EXIST_ROOM = "Room with this number is already exist.";

    public Room defineRoomById(long id) throws ServiceException {
        Room room = null;
        ConnectionPool pool = null;
        ProxyConnection connection = null;

        try {
            pool = ConnectionPool.getInstance();
            connection = pool.getConnection();

            RoomDAO dao = new RoomDAO(connection);
            room = dao.findEntityById(id);
        } catch (DAOException e) {
            throw new ServiceException("Can not define room by id", e);
        } finally {
            this.closePoolConnection(pool, connection);
        }

        return room;
    }

    public boolean updateRoomData(Room room) throws ServiceException {
        boolean updated = false;
        ConnectionPool pool = null;
        ProxyConnection connection = null;

        try {
            pool = ConnectionPool.getInstance();
            connection = pool.getConnection();

            RoomDAO dao = new RoomDAO(connection);
            Room updatedRoom = dao.update(room);

            updated = updatedRoom != null;
        } catch (DAOException e) {
            throw new ServiceException("Can not update room date.", e);
        } finally {
            this.closePoolConnection(pool, connection);
        }

        return updated;
    }

    public List<Room> defineAllRooms() throws ServiceException {
        List<Room> rooms = null;
        ConnectionPool pool = null;
        ProxyConnection connection = null;

        try {
            pool = ConnectionPool.getInstance();
            connection = pool.getConnection();

            RoomDAO dao = new RoomDAO(connection);
            rooms = dao.findAll();
        } catch (DAOException e) {
            throw new ServiceException("Can not define rooms.", e);
        } finally {
            this.closePoolConnection(pool, connection);
        }

        return rooms;
    }

    public boolean addNewRoom(long id, short maxLivings, boolean type,
                              BigDecimal cost, boolean onRepair, String grade, String description)
            throws ServiceException {
        boolean created = false;
        ConnectionPool pool = null;
        ProxyConnection connection = null;

        try {
            pool = ConnectionPool.getInstance();
            connection = pool.getConnection();

            RoomDAO dao = new RoomDAO(connection);
            Room room = new Room(id, new String(), maxLivings, type, cost, onRepair, grade, description);
            created = dao.create(room);
        } catch (DAOException e) {
            throw new ServiceException("Can not create new room.", e);
        } finally {
            this.closePoolConnection(pool, connection);
        }

        return created;
    }

    public boolean deleteRoom(long id) throws ServiceException {
        boolean deleted = false;
        ConnectionPool pool = null;
        ProxyConnection connection = null;

        try {
            pool = ConnectionPool.getInstance();
            connection = pool.getConnection();

            RoomDAO dao = new RoomDAO(connection);
            deleted = dao.remove(id);
        } catch (DAOException e) {
            throw new ServiceException("Can not delete room.", e);
        }finally {
            this.closePoolConnection(pool, connection);
        }

        return deleted;
    }

    public String isRoomNumberExist(String roomNumber) throws ServiceException {
        String errorMsg = "";
        ConnectionPool pool = null;
        ProxyConnection connection = null;

        try {
            pool = ConnectionPool.getInstance();
            connection = pool.getConnection();

            RoomDAO dao = new RoomDAO(connection);
            long id = Long.parseLong(roomNumber);
            Room room = dao.findEntityById(id);

            if (room != null) {
                errorMsg = ERROR_EXIST_ROOM;
            }
        } catch (DAOException e) {
            throw new ServiceException("Can not define room in database.", e);
        } finally {
            this.closePoolConnection(pool, connection);
        }

        return errorMsg;
    }

    public ServletFileUpload initFileUploader(File tempDir) {
        DiskFileItemFactory factory = new DiskFileItemFactory();
        factory.setSizeThreshold(MEMORY_THRESHOLD);
        factory.setRepository(tempDir);

        ServletFileUpload upload = new ServletFileUpload(factory);
        upload.setFileSizeMax(MAX_FILE_SIZE);
        upload.setSizeMax(MAX_REQUEST_SIZE);

        return upload;
    }

    public String getParameter(String paramName, List<FileItem> formItems) {
        String result = null;

        if (formItems != null && !formItems.isEmpty()) {
            for (FileItem item : formItems) {
                if (item.getFieldName().equals(paramName)) {
                    result = item.getString();
                }
            }
        }

        return result;
    }

    public String uploadFile(String param, List<FileItem> formItems, String uploadPath) throws ServiceException {
        String fileName = null;

        try {
            if (formItems != null && !formItems.isEmpty()) {
                for (FileItem item : formItems) {
                    if (item.getFieldName().equals(param)) {
                        fileName = new File(item.getName()).getName();
                        String filePath = uploadPath + File.separator + fileName;
                        File storeFile = new File(filePath);

                        item.write(storeFile);
                    }
                }
            }
        } catch (Exception e) {
            throw new ServiceException("Can not upload file.", e);
        }

        return fileName;
    }
}
