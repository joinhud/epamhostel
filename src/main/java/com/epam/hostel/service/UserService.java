package com.epam.hostel.service;

import com.epam.hostel.dao.BanDAO;
import com.epam.hostel.dao.UserDAO;
import com.epam.hostel.entity.Ban;
import com.epam.hostel.entity.Booking;
import com.epam.hostel.entity.User;
import com.epam.hostel.exception.DAOException;
import com.epam.hostel.exception.ServiceException;
import com.epam.hostel.pool.ConnectionPool;
import com.epam.hostel.pool.ProxyConnection;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class UserService extends AbstractService {
    private static final int USERS_PER_PAGE = 8;
    private static final String ERROR_DATES = "To date must be earlier than from date!";
    private static final String ERROR_BAN_DATES = "Incorrect date interval!";

    public int defineUsersPagesCountByType(String type) throws ServiceException {
        int pagesCount = 1;
        ConnectionPool pool = null;
        ProxyConnection connection = null;

        try {
            pool = ConnectionPool.getInstance();
            connection = pool.getConnection();

            UserDAO dao = new UserDAO(connection);

            int commentsCount = 0;

            if (type == null || type.isEmpty()) {
                commentsCount = dao.findCountOfAllUsers();
            } else {
                commentsCount = dao.findCountOfBanedUsers();
            }

            if (commentsCount != 0) {
                if (commentsCount % USERS_PER_PAGE == 0) {
                    pagesCount = commentsCount / USERS_PER_PAGE;
                } else {
                    pagesCount = (commentsCount / USERS_PER_PAGE) + 1;
                }
            }
        } catch (DAOException e) {
            throw new ServiceException("Can not define pages count.", e);
        } finally {
            this.closePoolConnection(pool, connection);
        }

        return pagesCount;
    }

    public List<User> defineUsersByPageAndType(int page, String type) throws ServiceException {
        List<User> users = new LinkedList<>();
        ConnectionPool pool = null;
        ProxyConnection connection = null;

        try {
            pool = ConnectionPool.getInstance();
            connection = pool.getConnection();

            UserDAO dao = new UserDAO(connection);
            int offset = USERS_PER_PAGE * page;

            if (page >= 0) {
                if (type == null || type.isEmpty()) {
                    users = dao.findAllLimitUsers(USERS_PER_PAGE, offset);
                } else {
                    users = dao.findBanedLimitUsers(USERS_PER_PAGE, offset);
                }
            }
        } catch (DAOException e) {
            throw new ServiceException("Can not define comments by type and page index.", e);
        } finally {
            this.closePoolConnection(pool, connection);
        }

        return users;
    }

    public User defineUserById(long id) throws ServiceException {
        User user = null;
        ConnectionPool pool = null;
        ProxyConnection connection = null;

        try {
            pool = ConnectionPool.getInstance();
            connection = pool.getConnection();

            UserDAO dao = new UserDAO(connection);
            user = dao.findEntityById(id);
        } catch (DAOException e) {
            throw new ServiceException("Can not define user by id.", e);
        } finally {
            this.closePoolConnection(pool, connection);
        }

        return user;
    }

    public List<Ban> defineBlockingsByUserId(long id) throws ServiceException {
        List<Ban> bans = new ArrayList<>();
        ProfileService profileService = new ProfileService();
        bans = profileService.defineBlockingByUserId(id);

        return bans;
    }

    public List<Booking> defineBookingsByUserId(long id) throws ServiceException {
        List<Booking> bookings = new ArrayList<>();
        BookService service = new BookService();
        bookings = service.defineBookingsByUserId(id);

        return bookings;
    }

    public String checkDatesUserBan(long id, LocalDateTime fromDate, LocalDateTime toDate) throws ServiceException {
        String errorMsg = "";
        ConnectionPool pool = null;
        ProxyConnection connection = null;

        try {
            pool = ConnectionPool.getInstance();
            connection = pool.getConnection();

            BanDAO banDAO = new BanDAO(connection);
            Ban lastUserBan = banDAO.findLastBanByUserId(id);

            if (toDate.isBefore(fromDate)) {
                errorMsg = ERROR_DATES;
            } else if (fromDate.isBefore(lastUserBan.getToDate())) {
                errorMsg = ERROR_BAN_DATES;
            }
        } catch (DAOException e) {
            throw new ServiceException("Can not check from date value.", e);
        } finally {
            this.closePoolConnection(pool, connection);
        }

        return errorMsg;
    }

    public boolean addUserBan(long id, LocalDateTime fromDate, LocalDateTime toDate) throws ServiceException {
        boolean result = false;
        ConnectionPool pool = null;
        ProxyConnection connection = null;

        try {
            pool = ConnectionPool.getInstance();
            connection = pool.getConnection();

            BanDAO banDAO = new BanDAO(connection);
            Ban ban = new Ban(id, fromDate, toDate);
            result = banDAO.create(ban);
        } catch (DAOException e) {
            throw new ServiceException("Can not add new ban for user.", e);
        } finally {
            this.closePoolConnection(pool, connection);
        }

        return result;
    }

    public boolean changeUserDiscount(long userId, float discount) throws ServiceException {
        boolean result = false;
        ConnectionPool pool = null;
        ProxyConnection connection = null;

        try {
            pool = ConnectionPool.getInstance();
            connection = pool.getConnection();

            UserDAO dao = new UserDAO(connection);
            User user = dao.findEntityById(userId);
            user.setDiscount(discount);
            user = dao.update(user);

            if (user != null) {
                result = true;
            }
        } catch (DAOException e) {
            throw new ServiceException("Can not change users discount.", e);
        } finally {
            this.closePoolConnection(pool, connection);
        }

        return result;
    }
}
