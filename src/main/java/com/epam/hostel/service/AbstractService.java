package com.epam.hostel.service;

import com.epam.hostel.exception.DAOException;
import com.epam.hostel.exception.ServiceException;
import com.epam.hostel.pool.ConnectionPool;
import com.epam.hostel.pool.ProxyConnection;

/**
 * The {@code AbstractService} absctract class
 *
 * @author Eugene Dubouskiy
 * @version 1.0
 */
public abstract class AbstractService {

    /**
     * Close connection from pool
     *
     * @param pool {@code ConnectionPool} pool
     * @param connection {@code ProxyConnection} connection
     *
     * @see ConnectionPool
     * @see ProxyConnection
     * @exception ServiceException
     *            if can not return connection to pool*/
    void closePoolConnection(ConnectionPool pool, ProxyConnection connection) throws ServiceException {
        if (pool != null && connection != null) {
            try {
                pool.closeConnection(connection);
            } catch (DAOException e) {
                throw new ServiceException(e);
            }
        }
    }
}
