package com.epam.hostel.type;

/* Booking state*/
public enum BookingState {
    PROCESSING, DENIED, ACCEPTED
}
