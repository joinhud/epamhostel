package com.epam.hostel.type;

import com.epam.hostel.command.*;

/*
* Commands enum with commands objects*/
public enum CommandEnum {
    CHANGE_LANGUAGE {
        {
            this.command = new ChangeLanguageCommand();
        }
    },
    TO_SIGNIN {
        {
            this.command = new ToSignInCommand();
        }
    },
    SIGNIN {
        {
            this.command = new SignInCommand();
        }
    },
    TO_REGISTER {
        {
            this.command = new ToRegisterCommand();
        }
    },
    REGISTER {
        {
            this.command = new RegisterCommand();
        }
    },
    TO_COMMENTS {
        {
            this.command = new ToCommentsCommand();
        }
    },
    SIGNOUT {
        {
            this.command = new SignOutCommand();
        }
    },
    TO_BOOK {
        {
            this.command = new ToBookCommand();
        }
    },
    BOOK {
        {
            this.command = new BookCommand();
        }
    },
    TO_ADD_ACCOUNT {
        {
            this.command = new ToAddAccountCommand();
        }
    },
    ADD_ACCOUNT {
        {
            this.command = new AddAccountCommand();
        }
    },
    DELETE_ACCOUNT {
        {
            this.command = new DeleteBankAccountCommand();
        }
    },
    TO_REFILL_ACCOUNT {
        {
            this.command = new ToRefillBankAccountCommand();
        }
    },
    REFILL_ACCOUNT {
        {
            this.command = new RefillBankAccountCommand();
        }
    },
    PAY_BOOKING {
        {
            this.command = new PayBookingCommand();
        }
    },
    TO_BOOKINGS {
        {
            this.command = new ToBookingsCommand();
        }
    },
    CHANGE_BOOKING_STATE {
        {
            this.command = new ChangeBookingStateCommand();
        }
    },
    TO_CANCEL_RESERVATION {
        {
            this.command = new ToCancelReservationCommand();
        }
    },
    CANCEL_RESERVATION {
        {
            this.command = new CancelReservationCommand();
        }
    },
    TO_PROFILE {
        {
            this.command = new ToProfileCommand();
        }
    },
    CHANGE_PASSWORD {
        {
            this.command = new ChangePasswordCommand();
        }
    },
    CHANGE_FIRSTNAME {
        {
            this.command = new ChangeFirstNameCommand();
        }
    },
    CHANGE_LASTNAME {
        {
            this.command = new ChangeLastNameCommand();
        }
    },
    DELETE_COMMENT {
        {
            this.command = new DeleteCommentCommand();
        }
    },
    ADD_COMMENT {
        {
            this.command = new AddCommentCommand();
        }
    },
    TO_ROOMS {
        {
            this.command = new ToRoomsCommand();
        }
    },
    TO_ROOM {
        {
            this.command = new ToRoomCommand();
        }
    },
    TO_EDIT_ROOM {
        {
            this.command = new ToEditRoomCommand();
        }
    },
    TO_ADD_ROOM {
        {
            this.command = new ToAddRoomCommand();
        }
    },
    ADD_ROOM {
        {
            this.command = new AddRoomCommand();
        }
    },
    EDIT_ROOM {
        {
            this.command = new EditRoomCommand();
        }
    },
    DELETE_ROOM {
        {
            this.command = new DeleteRoomCommand();
        }
    },
    TO_UPLOAD_IMAGE {
        {
            this.command = new ToUploadImageCommand();
        }
    },
    UPLOAD_IMG {
        {
            this.command = new UploadImageCommand();
        }
    },
    TO_USERS {
        {
            this.command = new ToUsersCommand();
        }
    },
    VIEW_USER {
        {
            this.command = new ToUserDetailsCommand();
        }
    },
    SET_BAN {
        {
            this.command = new SetUserBanCommand();
        }
    },
    CHANGE_DISCOUNT {
        {
            this.command = new ChangeUserDiscountCommand();
        }
    },
    CHANGE_DELETED_COMMENT {
        {
            this.command = new ChangeDeletedCommentCommand();
        }
    };

    ActionCommand command;
    public ActionCommand getCurrentCommand() {
        return command;
    }
}
