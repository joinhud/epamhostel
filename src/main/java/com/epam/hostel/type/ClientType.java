package com.epam.hostel.type;

/* Project roles*/
public enum ClientType {
    GUEST, USER, ADMIN
}
