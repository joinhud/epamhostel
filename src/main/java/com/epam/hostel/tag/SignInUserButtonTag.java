package com.epam.hostel.tag;

import com.epam.hostel.entity.User;
import com.epam.hostel.manager.TextManager;
import com.epam.hostel.type.ClientType;

import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;
import java.util.Locale;

/* Custom tag for displaying Sign In button or
* user profile button*/
@SuppressWarnings("serial")
public class SignInUserButtonTag extends TagSupport {
    private static final int PARAM_FIRST = 0;
    private static final int PARAM_SECOND = 1;

    private static final String PARAM_ROLE = "role";
    private static final String PARAM_USER = "user";
    private static final String PARAM_LOCALE = "locale";

    private static final String SPLITTER = "_";

    private static final String KEY_SIGN_IN = "label.button.sign.in";

    private static final String OUT_SIGN_IN = "<a href=\"/EpamHostel?command=to_signin\" class=\"link\">";
    private static final String OUT_SIGN_IN_END = "</a>";
    private static final String OUT_PROFILE = "<a href=\"/EpamHostel?command=to_profile\"><i class=\"material-icons md-light md-48\">account_circle</i><p>";
    private static final String OUT_PROFILE_END = "</p></a>";

    @Override
    public int doStartTag() throws JspException {
        HttpSession session = pageContext.getSession();
        ClientType role = (ClientType) session.getAttribute(PARAM_ROLE);
        User user = (User) session.getAttribute(PARAM_USER);
        String pageLocale = (String) session.getAttribute(PARAM_LOCALE);
        JspWriter out = pageContext.getOut();
        String outContent = new String();

        if (ClientType.GUEST.equals(role)) {
            String[] localeValues = pageLocale.split(SPLITTER);
            Locale locale = new Locale(localeValues[PARAM_FIRST], localeValues[PARAM_SECOND]);
            TextManager manager = new TextManager(locale);

            outContent = OUT_SIGN_IN + manager.getProperty(KEY_SIGN_IN) + OUT_SIGN_IN_END;
        } else {
            outContent = OUT_PROFILE + user.getEmail() + OUT_PROFILE_END;
        }

        try {
            out.write(outContent);
        } catch (IOException e) {
            throw new JspException(e);
        }

        return SKIP_BODY;
    }

    @Override
    public int doEndTag() throws JspException {
        return EVAL_PAGE;
    }
}
