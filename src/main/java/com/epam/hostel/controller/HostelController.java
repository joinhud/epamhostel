package com.epam.hostel.controller;

import com.epam.hostel.command.ActionCommand;
import com.epam.hostel.factory.ActionFactory;
import com.epam.hostel.pool.ConnectionPool;
import com.epam.hostel.util.ControllerUtil;
import com.epam.hostel.util.ResourceInitializer;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet("/EpamHostel")
public class HostelController extends HttpServlet {
    @Override
    public void init() throws ServletException {
        super.init();

        ResourceInitializer.init();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String url = processRequest(req, resp);
        RequestDispatcher dispatcher = req.getRequestDispatcher(url);
        dispatcher.forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String url = processRequest(req, resp);
        ControllerUtil util = new ControllerUtil();

        if (util.checkSuccess(req)) {
            resp.sendRedirect(url);
        } else {
            RequestDispatcher dispatcher = req.getRequestDispatcher(url);
            dispatcher.forward(req, resp);
        }
    }

    @Override
    public void destroy() {
        super.destroy();

        ConnectionPool pool = ConnectionPool.getInstance();
        pool.closeConnections();
    }

    private String processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String url = null;

        ActionFactory client = new ActionFactory();
        ActionCommand command = client.defineCommand(request);

        url = command.execute(request);

        return url;
    }
}
