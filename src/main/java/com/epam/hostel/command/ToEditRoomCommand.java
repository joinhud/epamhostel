package com.epam.hostel.command;

import com.epam.hostel.entity.Room;
import com.epam.hostel.exception.ServiceException;
import com.epam.hostel.manager.ConfigurationManager;
import com.epam.hostel.service.RoomService;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

public class ToEditRoomCommand implements ActionCommand {
    private static final Logger LOG = LogManager.getLogger();

    private static final String PARAM_ID = "id";
    private static final String PARAM_ROOM = "room";
    private static final String PARAM_ERROR = "error";

    private static final String PATH_PAGE_EDIT_ROOM = "path.page.admin.edit.room";
    private static final String PATH_PAGE_ERROR = "path.page.error.common";

    @Override
    public String execute(HttpServletRequest request) {
        RoomService roomService = new RoomService();
        ConfigurationManager manager = new ConfigurationManager();

        long id = Long.parseLong(request.getParameter(PARAM_ID));
        String page = manager.getProperty(PATH_PAGE_EDIT_ROOM);

        try {
            Room room = roomService.defineRoomById(id);
            request.setAttribute(PARAM_ROOM, room);
        } catch (ServiceException e) {
            LOG.log(Level.ERROR, e);
            request.setAttribute(PARAM_ERROR, e);
            page = manager.getProperty(PATH_PAGE_ERROR);
        }

        return page;
    }
}
