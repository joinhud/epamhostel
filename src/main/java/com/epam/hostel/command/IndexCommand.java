package com.epam.hostel.command;

import com.epam.hostel.exception.ServiceException;
import com.epam.hostel.manager.ConfigurationManager;
import com.epam.hostel.service.IndexService;
import com.epam.hostel.type.ClientType;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class IndexCommand implements ActionCommand {
    private static final Logger LOG = LogManager.getLogger();

    private static final String PARAM_SHARED_ROOMS = "sharedRooms";
    private static final String PARAM_PRIVATE_ROOMS = "privateRooms";
    private static final String PARAM_ERROR = "error";
    private static final String PARAM_ROLE = "role";
    private static final String PARAM_TODAY_BOOKINGS = "todayBookings";

    private static final String PATH_PAGE_INDEX = "path.page.index";
    private static final String PATH_PAGE_ADMIN_MAIN = "path.page.admin.main";
    private static final String PATH_PAGE_ERROR = "path.page.error.rooms";

    @Override
    public String execute(HttpServletRequest request) {
        ConfigurationManager manager = new ConfigurationManager();
        IndexService service = new IndexService();
        HttpSession session = request.getSession();

        ClientType role = (ClientType) session.getAttribute(PARAM_ROLE);
        String page = manager.getProperty(PATH_PAGE_INDEX);

        try {
            if (ClientType.ADMIN.equals(role)) {
                request.setAttribute(PARAM_TODAY_BOOKINGS, service.collectProcessingTodayBookings());
                page = manager.getProperty(PATH_PAGE_ADMIN_MAIN);
            } else {
                request.setAttribute(PARAM_SHARED_ROOMS, service.collectRoomsByType(false));
                request.setAttribute(PARAM_PRIVATE_ROOMS, service.collectRoomsByType(true));
            }
        } catch (ServiceException e) {
            LOG.log(Level.ERROR, e);
            request.setAttribute(PARAM_ERROR, e);
            page = manager.getProperty(PATH_PAGE_ERROR);
        }

        return page;
    }
}
