package com.epam.hostel.command;

import com.epam.hostel.entity.Comment;
import com.epam.hostel.entity.User;
import com.epam.hostel.exception.ServiceException;
import com.epam.hostel.manager.ConfigurationManager;
import com.epam.hostel.service.CommentService;
import com.epam.hostel.type.ClientType;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Map;

public class ToCommentsCommand implements ActionCommand {
    private static final Logger LOG = LogManager.getLogger();

    private static final String PARAM_COMMENTS = "comments";
    private static final String PARAM_ERROR = "error";
    private static final String PARAM_USER = "user";
    private static final String PARAM_USER_COMMENTS = "userComments";
    private static final String PARAM_ROLE = "role";
    private static final String PARAM_INDEX = "pageIndex";
    private static final String PARAM_TYPE = "type";
    private static final String PARAM_CURR_PAGE = "currPage";
    private static final String PARAM_PAGES_COUNT = "pagesCount";
    private static final String PARAM_COMMENTS_TYPE = "commentsType";
    private static final String PARAM_BANED = "baned";

    private static final String PATH_PAGE_USER_COMMENTS = "path.page.comments";
    private static final String PATH_PAGE_ADMIN_COMMENTS = "path.page.admin.comments";
    private static final String PATH_PAGE_ERROR = "path.page.error.common";

    @Override
    public String execute(HttpServletRequest request) {
        HttpSession session = request.getSession();
        ConfigurationManager manager = new ConfigurationManager();
        CommentService commentService = new CommentService();
        User user = (User) session.getAttribute(PARAM_USER);
        ClientType role = (ClientType) session.getAttribute(PARAM_ROLE);

        String page = manager.getProperty(PATH_PAGE_USER_COMMENTS);

        try {
            if (ClientType.ADMIN.equals(role)) {
                int pageIndex = 0;
                String pageIndexParam = request.getParameter(PARAM_INDEX);

                if (pageIndexParam != null) {
                    pageIndex = Integer.parseInt(pageIndexParam);
                }

                String commentsType = request.getParameter(PARAM_TYPE);

                int pagesCount = commentService.defineCommentsPagesCountByType(commentsType);
                request.setAttribute(PARAM_CURR_PAGE, pageIndex);
                request.setAttribute(PARAM_PAGES_COUNT, pagesCount);
                request.setAttribute(PARAM_COMMENTS, commentService.defineCommentsByPageAndType(pageIndex, commentsType));
                request.setAttribute(PARAM_COMMENTS_TYPE, commentsType);
                page = manager.getProperty(PATH_PAGE_ADMIN_COMMENTS);
            } else {
                Map<Comment, String> comments = commentService.collectNotDeletedComments();
                boolean baned = commentService.isUserBaned(user.getId());

                request.setAttribute(PARAM_COMMENTS, comments);
                request.setAttribute(PARAM_BANED, baned);

                if (user != null) {
                    List<Comment> userComments = commentService.collectCommentsByUserId(user.getId());

                    if (userComments != null) {
                        request.setAttribute(PARAM_USER_COMMENTS, userComments);
                    }
                }
            }
        } catch (ServiceException e) {
            LOG.log(Level.ERROR, e);
            request.setAttribute(PARAM_ERROR, e);
            page = manager.getProperty(PATH_PAGE_ERROR);
        }

        return page;
    }
}
