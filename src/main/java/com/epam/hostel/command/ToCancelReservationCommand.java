package com.epam.hostel.command;

import com.epam.hostel.entity.BankAccount;
import com.epam.hostel.entity.Booking;
import com.epam.hostel.entity.User;
import com.epam.hostel.exception.ServiceException;
import com.epam.hostel.manager.ConfigurationManager;
import com.epam.hostel.service.BankAccountService;
import com.epam.hostel.service.BookService;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

public class ToCancelReservationCommand implements ActionCommand {
    private static final Logger LOG = LogManager.getLogger();

    private static final String PARAM_ID = "id";
    private static final String PARAM_BOOKING = "booking";
    private static final String PARAM_ACCOUNTS = "accounts";
    private static final String PARAM_USER = "user";
    private static final String PARAM_ERROR = "error";

    private static final String PATH_PAGE_CANCEL_RESERVATION = "path.page.user.cancel.reservation";
    private static final String PATH_PAGE_ERROR = "path.page.error.user.cancel.reservation";

    @Override
    public String execute(HttpServletRequest request) {
        HttpSession session = request.getSession();
        BookService bookService = new BookService();
        ConfigurationManager manager = new ConfigurationManager();

        long bookingId = Long.parseLong(request.getParameter(PARAM_ID));
        User user = (User) session.getAttribute(PARAM_USER);
        Booking booking = null;
        List<BankAccount> accounts = null;

        String page = manager.getProperty(PATH_PAGE_CANCEL_RESERVATION);

        try {
            booking = bookService.defineById(bookingId);
            accounts = bookService.collectAccountsByUserId(user.getId());

            request.setAttribute(PARAM_BOOKING, booking);
            request.setAttribute(PARAM_ACCOUNTS, accounts);
        } catch (ServiceException e) {
            LOG.log(Level.ERROR, e);
            request.setAttribute(PARAM_ERROR, e);
            page = manager.getProperty(PATH_PAGE_ERROR);
        }

        return page;
    }
}
