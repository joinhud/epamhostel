package com.epam.hostel.command;

import com.epam.hostel.exception.ServiceException;
import com.epam.hostel.manager.ConfigurationManager;
import com.epam.hostel.service.RegisterService;
import com.epam.hostel.validator.UserValidator;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

public class RegisterCommand implements ActionCommand {
    private static final Logger LOG = LogManager.getLogger();

    private static final String PARAM_EMAIL = "email";
    private static final String PARAM_PASSWORD = "password";
    private static final String PARAM_PASSWORD_CONFIRM = "passConfirm";
    private static final String PARAM_FIRST_NAME = "firstName";
    private static final String PARAM_LAST_NAME = "lastName";
    private static final String PARAM_ERROR_EMAIL_MSG = "errorEmailMsg";
    private static final String PARAM_ERROR_PASSWORD_MSG = "errorPasswordMsg";
    private static final String PARAM_ERROR_PASS_CONFIRM_MSG = "errorPassConfirmMsg";
    private static final String PARAM_ERROR_FIRST_NAME_MSG = "errorFirstNameMsg";
    private static final String PARAM_ERROR_LAST_NAME_MSG = "errorLastNameMsg";
    private static final String PARAM_ERROR = "error";
    private static final String PARAM_SUCCESS = "success";

    private static final String PATH_PAGE_REGISTER = "path.page.register";
    private static final String PATH_URL_SIGN_IN = "path.url.sign.in";
    private static final String PATH_PAGE_ERROR = "path.page.error.register";

    @Override
    public String execute(HttpServletRequest request) {
        boolean valid = true;
        ConfigurationManager manager = new ConfigurationManager();
        RegisterService service = new RegisterService();
        UserValidator validator = new UserValidator();
        String page = manager.getProperty(PATH_PAGE_REGISTER);

        String email = request.getParameter(PARAM_EMAIL);
        String password = request.getParameter(PARAM_PASSWORD);
        String passConfirm = request.getParameter(PARAM_PASSWORD_CONFIRM);
        String firstName = request.getParameter(PARAM_FIRST_NAME);
        String lastName = request.getParameter(PARAM_LAST_NAME);

        String errMsgEmail = "";
        request.setAttribute(PARAM_SUCCESS, false);

        try {
            errMsgEmail = service.isUserExistByEmail(email);
            if (errMsgEmail.isEmpty()) {
                errMsgEmail = validator.validEmail(email);
            }
        } catch (ServiceException e) {
            LOG.log(Level.ERROR, e);
            request.setAttribute(PARAM_ERROR, e);
            return manager.getProperty(PATH_PAGE_ERROR);
        }

        if (!errMsgEmail.isEmpty()) {
            valid = false;
            request.setAttribute(PARAM_ERROR_EMAIL_MSG, errMsgEmail);
        }

        String errMsgPassword = validator.validatePassword(password);

        if (!errMsgPassword.isEmpty()) {
            valid = false;
            request.setAttribute(PARAM_ERROR_PASSWORD_MSG, errMsgPassword);
        }

        String errMsgConfirmPassword = validator.checkPasswordConfirmation(password, passConfirm);

        if (!errMsgConfirmPassword.isEmpty()) {
            valid = false;
            request.setAttribute(PARAM_ERROR_PASS_CONFIRM_MSG, errMsgConfirmPassword);
        }

        String errMsgFirstName = validator.validateName(firstName);

        if (!errMsgFirstName.isEmpty()) {
            valid = false;
            request.setAttribute(PARAM_ERROR_FIRST_NAME_MSG, errMsgFirstName);
        }

        String errMsgLastName = validator.validateName(lastName);

        if (!errMsgLastName.isEmpty()) {
            valid = false;
            request.setAttribute(PARAM_ERROR_LAST_NAME_MSG, errMsgLastName);
        }

        if (valid) {
            try {
                service.createUser(email, password, firstName, lastName);
                request.setAttribute(PARAM_SUCCESS, true);
                page = manager.getProperty(PATH_URL_SIGN_IN);
            } catch (ServiceException e) {
                LOG.log(Level.ERROR, e);
                request.setAttribute(PARAM_ERROR, e);
                page = manager.getProperty(PATH_PAGE_ERROR);
            }
        }

        return page;
    }
}