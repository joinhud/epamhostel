package com.epam.hostel.command;

import com.epam.hostel.entity.User;
import com.epam.hostel.exception.ServiceException;
import com.epam.hostel.manager.ConfigurationManager;
import com.epam.hostel.service.BankAccountService;
import com.epam.hostel.service.BookService;
import com.epam.hostel.service.ProfileService;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class DeleteBankAccountCommand implements ActionCommand {
    private static final Logger LOG = LogManager.getLogger();

    private static final String PARAM_ACCOUNT = "account";
    private static final String PARAM_USER = "user";
    private static final String PARAM_ERROR = "error";
    private static final String PARAM_SUCCESS = "success";

    private static final String PATH_URL_PROFILE = "path.url.user.profile";
    private static final String PATH_PAGE_ERROR = "path.page.error.user.delete.account";

    private static final String ERROR_MSG = "Can not delete bank account.";

    @Override
    public String execute(HttpServletRequest request) {
        BankAccountService bankAccountService = new BankAccountService();
        ConfigurationManager manager = new ConfigurationManager();

        long accountNumber = Long.parseLong(request.getParameter(PARAM_ACCOUNT));
        String page = manager.getProperty(PATH_URL_PROFILE);
        request.setAttribute(PARAM_SUCCESS, false);

        try {
            if (!bankAccountService.removeByAccountNumber(accountNumber)) {
                page = manager.getProperty(PATH_PAGE_ERROR);
                request.setAttribute(PARAM_ERROR, ERROR_MSG);
            } else {
                request.setAttribute(PARAM_SUCCESS, true);
            }
        } catch (ServiceException e) {
            LOG.log(Level.ERROR, e);
            request.setAttribute(PARAM_ERROR, e);
            page = manager.getProperty(PATH_PAGE_ERROR);
        }

        return page;
    }
}
