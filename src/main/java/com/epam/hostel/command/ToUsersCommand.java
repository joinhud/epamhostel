package com.epam.hostel.command;

import com.epam.hostel.exception.ServiceException;
import com.epam.hostel.manager.ConfigurationManager;
import com.epam.hostel.service.UserService;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

public class ToUsersCommand implements ActionCommand {
    private static final Logger LOG = LogManager.getLogger();

    private static final String PARAM_ERROR = "error";
    private static final String PARAM_USERS = "users";
    private static final String PARAM_INDEX = "pageIndex";
    private static final String PARAM_TYPE = "type";
    private static final String PARAM_CURR_PAGE = "currPage";
    private static final String PARAM_PAGES_COUNT = "pagesCount";
    private static final String PARAM_USERS_TYPE = "usersType";

    private static final String PATH_PAGE_USERS = "path.page.admin.users";
    private static final String PATH_PAGE_ERROR = "path.page.error.common";

    @Override
    public String execute(HttpServletRequest request) {
        ConfigurationManager manager = new ConfigurationManager();
        UserService userService = new UserService();

        String page = manager.getProperty(PATH_PAGE_ERROR);

        try {
            int pageIndex = 0;
            String pageIndexParam = request.getParameter(PARAM_INDEX);

            if (pageIndexParam != null) {
                pageIndex = Integer.parseInt(pageIndexParam);
            }

            String usersType = request.getParameter(PARAM_TYPE);

            int pagesCount = userService.defineUsersPagesCountByType(usersType);
            request.setAttribute(PARAM_CURR_PAGE, pageIndex);
            request.setAttribute(PARAM_PAGES_COUNT, pagesCount);
            request.setAttribute(PARAM_USERS, userService.defineUsersByPageAndType(pageIndex, usersType));
            request.setAttribute(PARAM_USERS_TYPE, usersType);
            page = manager.getProperty(PATH_PAGE_USERS);
        } catch (ServiceException e) {
            LOG.log(Level.ERROR, e);
            request.setAttribute(PARAM_ERROR, e);
        }

        return page;
    }
}
