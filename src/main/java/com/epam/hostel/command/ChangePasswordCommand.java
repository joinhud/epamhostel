package com.epam.hostel.command;

import com.epam.hostel.entity.Ban;
import com.epam.hostel.entity.BankAccount;
import com.epam.hostel.entity.User;
import com.epam.hostel.exception.ServiceException;
import com.epam.hostel.manager.ConfigurationManager;
import com.epam.hostel.service.BookService;
import com.epam.hostel.service.ProfileService;
import com.epam.hostel.service.SignInService;
import com.epam.hostel.validator.UserValidator;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

public class ChangePasswordCommand implements ActionCommand {
    private static final Logger LOG = LogManager.getLogger();

    private static final String PARAM_OLD_PASS = "oldPassword";
    private static final String PARAM_NEW_PASS = "newPassword";
    private static final String PARAM_ERROR_OLD_PASS = "errorOldPass";
    private static final String PARAM_ERROR_NEW_PASS = "errorNewPass";
    private static final String PARAM_USER = "user";
    private static final String PARAM_ERROR = "error";
    private static final String PARAM_ACCOUNTS = "accounts";
    private static final String PARAM_BLOCKING = "blockings";
    private static final String PARAM_SUCCESS = "success";

    private static final String PATH_PAGE_ERROR = "path.page.error.user.change.profile";
    private static final String PATH_PAGE_PROFILE = "path.page.user.profile";
    private static final String PATH_URL_PROFILE = "path.url.user.profile";

    private static final String ERROR_UPDATING_MSG = "Can not update user password.";

    @Override
    public String execute(HttpServletRequest request) {
        ConfigurationManager manager = new ConfigurationManager();
        UserValidator validator = new UserValidator();
        SignInService signInService = new SignInService();
        BookService bookService = new BookService();
        ProfileService profileService = new ProfileService();
        HttpSession session = request.getSession();

        String oldPassword = request.getParameter(PARAM_OLD_PASS);
        String newPassword = request.getParameter(PARAM_NEW_PASS);
        User user = (User) session.getAttribute(PARAM_USER);
        boolean valid = true;
        String page = manager.getProperty(PATH_PAGE_PROFILE);
        request.setAttribute(PARAM_SUCCESS, false);

        String errorMsgOldPass = "";

        try {
            errorMsgOldPass = validator.validatePassword(oldPassword);

            if (errorMsgOldPass.isEmpty()) {
                errorMsgOldPass = signInService.isCorrectPassword(user.getEmail(), oldPassword);
            }
        } catch (ServiceException e) {
            LOG.log(Level.ERROR, e);
            request.setAttribute(PARAM_ERROR, e);
            return manager.getProperty(PATH_PAGE_ERROR);
        }

        if (!errorMsgOldPass.isEmpty()) {
            valid = false;
            request.setAttribute(PARAM_ERROR_OLD_PASS, errorMsgOldPass);
        }

        String errorMsgNewPass = "";

        errorMsgNewPass = validator.validatePassword(newPassword);

        if (!errorMsgNewPass.isEmpty()) {
            valid = false;
            request.setAttribute(PARAM_ERROR_NEW_PASS, errorMsgNewPass);
        }

        try {
            if (valid) {
                user.setPassword(DigestUtils.sha1Hex(newPassword));

                if (!profileService.updateUserData(user)) {
                    request.setAttribute(PARAM_ERROR, ERROR_UPDATING_MSG);
                    page = manager.getProperty(PATH_PAGE_ERROR);
                } else {
                    page = manager.getProperty(PATH_URL_PROFILE);
                    request.setAttribute(PARAM_SUCCESS, true);
                }
            } else {
                List<BankAccount> accounts = bookService.collectAccountsByUserId(user.getId());
                List<Ban> bans = profileService.defineBlockingByUserId(user.getId());

                if (accounts != null) {
                    request.setAttribute(PARAM_ACCOUNTS, accounts);
                }

                if (bans != null) {
                    request.setAttribute(PARAM_BLOCKING, bans);
                }
            }
        } catch (ServiceException e) {
            LOG.log(Level.ERROR, e);
            request.setAttribute(PARAM_ERROR, e);
            page = manager.getProperty(PATH_PAGE_ERROR);
        }

        return page;
    }
}
