package com.epam.hostel.command;

import com.epam.hostel.entity.BankAccount;
import com.epam.hostel.entity.User;
import com.epam.hostel.exception.ServiceException;
import com.epam.hostel.manager.ConfigurationManager;
import com.epam.hostel.service.BookService;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

public class ToBookCommand implements ActionCommand {
    private static final Logger LOG = LogManager.getLogger();

    private static final String PARAM_ROOM = "room";
    private static final String PARAM_ROOM_NUMBER = "roomNumber";
    private static final String PARAM_ACCOUNTS = "accounts";
    private static final String PARAM_USER = "user";
    private static final String PARAM_ERROR = "error";

    private static final String PATH_PAGE_BOOK = "path.page.user.book";
    private static final String PATH_PAGE_ERROR = "path.page.error.common";

    @Override
    public String execute(HttpServletRequest request) {
        BookService service = new BookService();
        ConfigurationManager manager = new ConfigurationManager();
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute(PARAM_USER);

        String room = request.getParameter(PARAM_ROOM);
        request.setAttribute(PARAM_ROOM_NUMBER, room);

        String page = manager.getProperty(PATH_PAGE_BOOK);

        try {
            List<BankAccount> accounts = service.collectAccountsByUserId(user.getId());

            if (accounts != null) {
                request.setAttribute(PARAM_ACCOUNTS, accounts);
            }
        } catch (ServiceException e) {
            LOG.log(Level.ERROR, e);
            request.setAttribute(PARAM_ERROR, e);
            page = manager.getProperty(PATH_PAGE_ERROR);
        }

        return page;
    }
}
