package com.epam.hostel.command;

import com.epam.hostel.entity.Comment;
import com.epam.hostel.entity.User;
import com.epam.hostel.exception.ServiceException;
import com.epam.hostel.manager.ConfigurationManager;
import com.epam.hostel.service.CommentService;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

public class AddCommentCommand implements ActionCommand {
    private static final Logger LOG = LogManager.getLogger();

    private static final String PARAM_COMMENT_TEXT = "commentText";
    private static final String PARAM_ERROR_MSG = "errorMsgAddComment";
    private static final String PARAM_ERROR = "error";
    private static final String PARAM_USER = "user";
    private static final String PARAM_COMMENTS = "comments";
    private static final String PARAM_USER_COMMENTS = "userComments";
    private static final String PARAM_SUCCESS = "success";

    private static final String PATH_PAGE_COMMENTS = "path.page.comments";
    private static final String PATH_PAGE_ERROR = "path.page.error.common";
    private static final String PATH_URL_COMMENTS = "path.url.comments";

    private static final String ERROR_ADD_COMMENT = "Can not add comment.";

    @Override
    public String execute(HttpServletRequest request) {
        ConfigurationManager manager = new ConfigurationManager();
        CommentService commentService = new CommentService();
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute(PARAM_USER);

        String commentText = request.getParameter(PARAM_COMMENT_TEXT);
        String page = manager.getProperty(PATH_PAGE_COMMENTS);
        request.setAttribute(PARAM_SUCCESS, false);


        try {
            boolean added = commentService.addNewComment(new Comment(user.getId(), commentText, LocalDateTime.now(), false));

            if (!added) {
                Map<Comment, String> comments = commentService.collectNotDeletedComments();
                request.setAttribute(PARAM_COMMENTS, comments);

                List<Comment> userComments = commentService.collectCommentsByUserId(user.getId());

                if (userComments != null) {
                    request.setAttribute(PARAM_USER_COMMENTS, userComments);
                }

                request.setAttribute(PARAM_ERROR_MSG, ERROR_ADD_COMMENT);
            } else {
                request.setAttribute(PARAM_SUCCESS, true);
                page = manager.getProperty(PATH_URL_COMMENTS);
            }
        } catch (ServiceException e) {
            LOG.log(Level.ERROR, e);
            request.setAttribute(PARAM_ERROR, e);
            page = manager.getProperty(PATH_PAGE_ERROR);
        }

        return page;
    }
}
