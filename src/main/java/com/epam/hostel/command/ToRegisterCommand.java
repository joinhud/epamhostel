package com.epam.hostel.command;

import com.epam.hostel.manager.ConfigurationManager;

import javax.servlet.http.HttpServletRequest;

public class ToRegisterCommand implements ActionCommand {
    private static final String PATH_PAGE_REGISTER = "path.page.register";

    @Override
    public String execute(HttpServletRequest request) {
        ConfigurationManager manager = new ConfigurationManager();
        return manager.getProperty(PATH_PAGE_REGISTER);
    }
}
