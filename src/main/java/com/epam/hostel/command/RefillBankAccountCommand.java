package com.epam.hostel.command;

import com.epam.hostel.entity.User;
import com.epam.hostel.exception.ServiceException;
import com.epam.hostel.manager.ConfigurationManager;
import com.epam.hostel.service.BankAccountService;
import com.epam.hostel.validator.BankAccountValidator;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.math.BigDecimal;

public class RefillBankAccountCommand implements ActionCommand {
    private static final Logger LOG = LogManager.getLogger();

    private static final String PARAM_REFILLING = "refilling";
    private static final String PARAM_ACCOUNT_NUMBER = "accountNumber";
    private static final String PARAM_ERROR_MSG = "errorMsg";
    private static final String PARAM_ACCOUNT = "account";
    private static final String PARAM_USER = "user";
    private static final String PARAM_ERROR = "error";
    private static final String PARAM_SUCCESS = "success";

    private static final String PATH_URL_PROFILE = "path.url.user.profile";
    private static final String PATH_PAGE_REFILL = "path.page.user.refill";
    private static final String PATH_PAGE_ERROR = "path.page.error.user.refill.account";

    @Override
    public String execute(HttpServletRequest request) {
        BankAccountValidator validator = new BankAccountValidator();
        ConfigurationManager manager = new ConfigurationManager();
        BankAccountService bankAccountService = new BankAccountService();
        HttpSession session = request.getSession();

        long accountNumber = Long.parseLong(request.getParameter(PARAM_ACCOUNT_NUMBER));
        String refilling = request.getParameter(PARAM_REFILLING);
        User user = (User) session.getAttribute(PARAM_USER);

        String page = manager.getProperty(PATH_PAGE_REFILL);
        request.setAttribute(PARAM_SUCCESS, false);

        String errorMsg = "";

        errorMsg = validator.validateMoneyInput(refilling);

        try {
            if (!errorMsg.isEmpty()) {
                request.setAttribute(PARAM_ERROR_MSG, errorMsg);
                request.setAttribute(PARAM_ACCOUNT, bankAccountService.defineAccountByNumber(accountNumber));
            } else {
                BigDecimal money = new BigDecimal(refilling);

                if (bankAccountService.refillAccount(accountNumber, money)) {
                    page = manager.getProperty(PATH_URL_PROFILE);
                    request.setAttribute(PARAM_SUCCESS, true);
                }
            }
        } catch (ServiceException e) {
            LOG.log(Level.ERROR, e);
            request.setAttribute(PARAM_ERROR, e);
            page = manager.getProperty(PATH_PAGE_ERROR);
        }

        return page;
    }
}
