package com.epam.hostel.command;

import com.epam.hostel.entity.Booking;
import com.epam.hostel.entity.User;
import com.epam.hostel.exception.ServiceException;
import com.epam.hostel.manager.ConfigurationManager;
import com.epam.hostel.service.BookService;
import com.epam.hostel.type.ClientType;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.time.LocalDate;
import java.util.List;

public class ToBookingsCommand implements ActionCommand {
    private static final Logger LOG = LogManager.getLogger();

    private static final String PARAM_BOOKINGS = "bookings";
    private static final String PARAM_NOW = "now";
    private static final String PARAM_USER = "user";
    private static final String PARAM_ROLE = "role";
    private static final String PARAM_INDEX = "pageIndex";
    private static final String PARAM_CURR_PAGE = "currPage";
    private static final String PARAM_PAGES_COUNT = "pagesCount";
    private static final String PARAM_TYPE = "type";
    private static final String PARAM_BOOKINGS_TYPE = "bookingsType";
    private static final String PARAM_ERROR = "error";

    private static final String PATH_PAGE_USER_BOOKINGS = "path.page.user.bookings";
    private static final String PATH_PAGE_ADMIN_BOOKINGS = "path.page.admin.bookings";
    private static final String PATH_PAGE_ERROR = "path.page.error.user.bookings";

    @Override
    public String execute(HttpServletRequest request) {
        HttpSession session = request.getSession();
        BookService service = new BookService();
        ConfigurationManager manager = new ConfigurationManager();
        User user = (User) session.getAttribute(PARAM_USER);
        ClientType role = (ClientType) session.getAttribute(PARAM_ROLE);
        String page = manager.getProperty(PATH_PAGE_ERROR);

        try {
            if (ClientType.USER.equals(role)) {
                List<Booking> bookings = service.defineBookingsByUserId(user.getId());

                if (bookings != null) {
                    request.setAttribute(PARAM_BOOKINGS, bookings);
                    request.setAttribute(PARAM_NOW, LocalDate.now());
                    page = manager.getProperty(PATH_PAGE_USER_BOOKINGS);
                }
            } else if (ClientType.ADMIN.equals(role)){
                int pageIndex = 0;
                String pageIndexParam = request.getParameter(PARAM_INDEX);
                String stateValue = request.getParameter(PARAM_TYPE);

                if (pageIndexParam != null) {
                    pageIndex = Integer.parseInt(pageIndexParam);
                }

                int pagesCount = service.defineBookingsPagesCountByState(stateValue);
                request.setAttribute(PARAM_CURR_PAGE, pageIndex);
                request.setAttribute(PARAM_PAGES_COUNT, pagesCount);
                request.setAttribute(PARAM_BOOKINGS, service.defineBookingsByPageAndState(pageIndex, stateValue));
                request.setAttribute(PARAM_BOOKINGS_TYPE, stateValue);
                page = manager.getProperty(PATH_PAGE_ADMIN_BOOKINGS);
            }
        } catch (ServiceException e) {
            LOG.log(Level.ERROR, e);
            request.setAttribute(PARAM_ERROR, e);
        }

        return page;
    }
}
