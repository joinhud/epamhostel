package com.epam.hostel.command;

import com.epam.hostel.exception.ServiceException;
import com.epam.hostel.manager.ConfigurationManager;
import com.epam.hostel.service.BookService;
import com.epam.hostel.type.BookingState;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

public class ChangeBookingStateCommand implements ActionCommand {
    private static final Logger LOG = LogManager.getLogger();

    private static final String PARAM_ID = "bookingId";
    private static final String PARAM_STATE = "state";
    private static final String PARAM_BOOKINGS_TYPE = "bookingsType";
    private static final String PARAM_CURR_PAGE = "currPage";
    private static final String PARAM_ERROR = "error";
    private static final String PARAM_SUCCESS = "success";

    private static final String QUERY_PAGE_INDEX = "&pageIndex=";
    private static final String QUERY_TYPE = "&type=";

    private static final String PATH_PAGE_ERROR = "path.page.error.common";
    private static final String PATH_URL_BOOKINGS = "path.url.user.bookings";

    @Override
    public String execute(HttpServletRequest request) {
        ConfigurationManager manager = new ConfigurationManager();
        BookService bookService = new BookService();

        long bookingId = Long.parseLong(request.getParameter(PARAM_ID));
        BookingState state = BookingState.valueOf(request.getParameter(PARAM_STATE));
        String bookingsType = request.getParameter(PARAM_BOOKINGS_TYPE);
        String currPage = request.getParameter(PARAM_CURR_PAGE);

        String page = manager.getProperty(PATH_PAGE_ERROR);
        request.setAttribute(PARAM_SUCCESS, false);

        try {
            if (bookService.updateBookingState(bookingId, state)) {
                request.setAttribute(PARAM_SUCCESS, true);
                page = manager.getProperty(PATH_URL_BOOKINGS) +
                        QUERY_PAGE_INDEX  + currPage +
                        QUERY_TYPE + bookingsType;
            }
        } catch (ServiceException e) {
            LOG.log(Level.ERROR, e);
            request.setAttribute(PARAM_ERROR, e);
        }

        return page;
    }
}
