package com.epam.hostel.command;

import com.epam.hostel.entity.Room;
import com.epam.hostel.exception.ServiceException;
import com.epam.hostel.manager.ConfigurationManager;
import com.epam.hostel.service.RoomService;
import com.epam.hostel.validator.RoomValidator;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;

public class EditRoomCommand implements ActionCommand {
    private static final Logger LOG = LogManager.getLogger();

    private static final String PARAM_ID = "id";
    private static final String PARAM_MAX_LIVINGS = "maxLivings";
    private static final String PARAM_TYPE = "type";
    private static final String PARAM_COST = "cost";
    private static final String PARAM_ON_REPAIR = "onRepair";
    private static final String PARAM_GRADE = "grade";
    private static final String PARAM_DESCRIPTION = "description";
    private static final String PARAM_ERROR_MAX_LIVINGS = "errorMaxLivingsMsg";
    private static final String PARAM_ERROR_COST = "errorCostMsg";
    private static final String PARAM_ERROR = "error";
    private static final String PARAM_SUCCESS = "success";
    private static final String PARAM_ROOM = "room";

    private static final String PATH_PAGE_EDIT_ROOM = "path.page.admin.edit.room";
    private static final String PATH_PAGE_ERROR = "path.page.error.common";
    private static final String PATH_URL_ROOM = "path.url.admin.room";

    @Override
    public String execute(HttpServletRequest request) {
        ConfigurationManager manager = new ConfigurationManager();
        RoomService roomService = new RoomService();
        RoomValidator validator = new RoomValidator();

        long id = Long.parseLong(request.getParameter(PARAM_ID));
        String maxLivingsName = request.getParameter(PARAM_MAX_LIVINGS);
        String typeName = request.getParameter(PARAM_TYPE);
        String costName = request.getParameter(PARAM_COST);
        String onRepairName = request.getParameter(PARAM_ON_REPAIR);
        String grade = request.getParameter(PARAM_GRADE);
        String description = request.getParameter(PARAM_DESCRIPTION);
        String page = manager.getProperty(PATH_PAGE_EDIT_ROOM);
        request.setAttribute(PARAM_SUCCESS, false);
        boolean valid = true;

        String errorMaxLivingsMsg = validator.validMaxLivings(maxLivingsName);

        if (!errorMaxLivingsMsg.isEmpty()) {
            valid = false;
            request.setAttribute(PARAM_ERROR_MAX_LIVINGS, errorMaxLivingsMsg);
        }

        String errorCostMsg = validator.validateCost(costName);

        if (!errorCostMsg.isEmpty()) {
            valid = false;
            request.setAttribute(PARAM_ERROR_COST, errorCostMsg);
        }

        try {
            Room room = roomService.defineRoomById(id);

            if (valid) {
                short maxLivings = Short.parseShort(maxLivingsName);
                boolean type = Boolean.parseBoolean(typeName);
                BigDecimal cost = new BigDecimal(costName);
                boolean onRepair = Boolean.parseBoolean(onRepairName);

                room.setMaxLivings(maxLivings);
                room.setType(type);
                room.setCost(cost);
                room.setOnRepair(onRepair);
                room.setGrade(grade);
                room.setDescription(description);

                if (roomService.updateRoomData(room)) {
                    page = manager.getProperty(PATH_URL_ROOM) + id;
                    request.setAttribute(PARAM_SUCCESS, true);
                }
            } else {
                request.setAttribute(PARAM_ROOM, room);
            }
        } catch (ServiceException e) {
            LOG.log(Level.ERROR, e);
            request.setAttribute(PARAM_ERROR, e);
            page = manager.getProperty(PATH_PAGE_ERROR);
        }

        return page;
    }
}
