package com.epam.hostel.command;

import com.epam.hostel.entity.BankAccount;
import com.epam.hostel.entity.Booking;
import com.epam.hostel.entity.User;
import com.epam.hostel.exception.ServiceException;
import com.epam.hostel.manager.ConfigurationManager;
import com.epam.hostel.service.BookService;
import com.epam.hostel.type.BookingState;
import com.epam.hostel.validator.BookingValidator;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

public class BookCommand implements ActionCommand {
    private static final Logger LOG = LogManager.getLogger();

    private static final String PARAM_NUMBER = "number";
    private static final String PARAM_ROOM_NUMBER = "roomNumber";
    private static final String PARAM_NUMBER_OF_LIVINGS = "numberOfLivings";
    private static final String PARAM_FROM_DATE = "fromDate";
    private static final String PARAM_TO_DATE = "toDate";
    private static final String PARAM_PAID = "paid";
    private static final String PARAM_ERROR_MSG = "errorMsg";
    private static final String PARAM_BANK_ACCOUNT = "bankAccounts";
    private static final String PARAM_USER = "user";
    private static final String PARAM_BOOKING = "booking";
    private static final String PARAM_ACCOUNTS = "accounts";
    private static final String PARAM_ERROR = "error";
    private static final String PARAM_SUCCESS = "success";

    private static final String PATH_PAGE_PAYMENT_BOOK = "path.page.user.payment.book";
    private static final String PATH_PAGE_BOOK = "path.page.user.book";
    private static final String PATH_PAGE_ERROR = "path.page.error.rooms";
    private static final String PATH_URL_BOOKINGS = "path.url.user.bookings";

    @Override
    public String execute(HttpServletRequest request) {
        BookService service = new BookService();
        BookingValidator validator = new BookingValidator();
        HttpSession session = request.getSession();
        ConfigurationManager manager = new ConfigurationManager();

        long number = Long.parseLong(request.getParameter(PARAM_NUMBER));
        int numberOfLiving = Integer.parseInt(request.getParameter(PARAM_NUMBER_OF_LIVINGS));
        LocalDate fromDate = LocalDate.parse(request.getParameter(PARAM_FROM_DATE));
        LocalDate toDate = LocalDate.parse(request.getParameter(PARAM_TO_DATE));
        boolean paid = request.getParameter(PARAM_PAID) != null;
        String bankAcc = request.getParameter(PARAM_BANK_ACCOUNT);
        User user = (User) session.getAttribute(PARAM_USER);
        BigDecimal totalCost = BigDecimal.ZERO;
        request.setAttribute(PARAM_SUCCESS, false);

        try {
            totalCost = service.calculateTotalSum(number, user.getDiscount(), fromDate, toDate);
        } catch (ServiceException e) {
            LOG.log(Level.ERROR, e);
            request.setAttribute(PARAM_ERROR, e);
            return manager.getProperty(PATH_PAGE_ERROR);
        }

        String errMsg = "";

        errMsg += validator.validCountOfLivings(numberOfLiving);
        errMsg += validator.validateDate(fromDate);
        errMsg += validator.validateInterval(fromDate, toDate);

        if (errMsg.isEmpty()) {
            try {
                errMsg += service.checkAvailability(number, numberOfLiving, fromDate, toDate);

                if (paid) {
                    errMsg += service.checkBankAccount(bankAcc);
                }
            } catch (ServiceException e) {
                LOG.log(Level.ERROR, e);
                request.setAttribute(PARAM_ERROR, e);
                return manager.getProperty(PATH_PAGE_ERROR);
            }
        }

        String page = manager.getProperty(PATH_PAGE_BOOK);

        try {
            if (!errMsg.isEmpty()) {
                request.setAttribute(PARAM_ERROR_MSG, errMsg);
                request.setAttribute(PARAM_ROOM_NUMBER, number);
                List<BankAccount> accounts = service.collectAccountsByUserId(user.getId());

                if (accounts != null) {
                    request.setAttribute(PARAM_ACCOUNTS, accounts);
                }
            } else if (paid){
                Booking booking = new Booking(number, user.getId(), LocalDateTime.now(),
                        numberOfLiving, fromDate, toDate, paid, totalCost, BookingState.PROCESSING);
                request.setAttribute(PARAM_BOOKING, booking);
                request.setAttribute(PARAM_BANK_ACCOUNT, bankAcc);
                page = manager.getProperty(PATH_PAGE_PAYMENT_BOOK);
            } else {
                service.createBooking(number, user.getId(), numberOfLiving, fromDate, toDate, paid, totalCost);
                request.setAttribute(PARAM_SUCCESS, true);
                page = manager.getProperty(PATH_URL_BOOKINGS);
            }
        } catch (ServiceException e) {
            LOG.log(Level.ERROR, e);
            request.setAttribute(PARAM_ERROR, e);
            page = manager.getProperty(PATH_PAGE_ERROR);
        }

        return page;
    }
}
