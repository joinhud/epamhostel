package com.epam.hostel.command;

import com.epam.hostel.entity.Room;
import com.epam.hostel.exception.ServiceException;
import com.epam.hostel.manager.ConfigurationManager;
import com.epam.hostel.service.RoomService;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public class ToRoomsCommand implements ActionCommand {
    private static final Logger LOG = LogManager.getLogger();

    private static final String PARAM_ROOMS = "rooms";
    private static final String PARAM_ERROR = "error";

    private static final String PATH_PAGE_ROOMS = "path.page.admin.rooms";
    private static final String PATH_PAGE_ERROR = "path.page.error.common";

    @Override
    public String execute(HttpServletRequest request) {
        ConfigurationManager manager = new ConfigurationManager();
        RoomService roomService = new RoomService();
        String page = manager.getProperty(PATH_PAGE_ERROR);

        try {
            List<Room> rooms = roomService.defineAllRooms();

            if (rooms != null) {
                request.setAttribute(PARAM_ROOMS, rooms);
                page = manager.getProperty(PATH_PAGE_ROOMS);
            }
        } catch (ServiceException e) {
            LOG.log(Level.ERROR, e);
            request.setAttribute(PARAM_ERROR, e);
        }

        return page;
    }
}
