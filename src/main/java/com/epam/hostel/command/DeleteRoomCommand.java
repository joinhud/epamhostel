package com.epam.hostel.command;

import com.epam.hostel.exception.ServiceException;
import com.epam.hostel.manager.ConfigurationManager;
import com.epam.hostel.service.RoomService;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

public class DeleteRoomCommand implements ActionCommand {
    private static final Logger LOG = LogManager.getLogger();

    private static final String PARAM_ID = "id";
    private static final String PARAM_ERROR = "error";
    private static final String PARAM_SUCCESS = "success";

    private static final String PATH_PAGE_ERROR = "path.page.error.common";
    private static final String PATH_URL_ROOM = "path.url.admin.rooms";

    @Override
    public String execute(HttpServletRequest request) {
        ConfigurationManager manager = new ConfigurationManager();
        RoomService roomService = new RoomService();

        long id = Long.parseLong(request.getParameter(PARAM_ID));
        String page = manager.getProperty(PATH_PAGE_ERROR);

        try {
            if (roomService.deleteRoom(id)) {
                request.setAttribute(PARAM_SUCCESS, true);
                page = manager.getProperty(PATH_URL_ROOM);
            }
        } catch (ServiceException e) {
            LOG.log(Level.ERROR, e);
            request.setAttribute(PARAM_ERROR, e);
        }

        return page;
    }
}
