package com.epam.hostel.command;

import com.epam.hostel.entity.Ban;
import com.epam.hostel.entity.Booking;
import com.epam.hostel.entity.User;
import com.epam.hostel.exception.ServiceException;
import com.epam.hostel.manager.ConfigurationManager;
import com.epam.hostel.service.UserService;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public class ToUserDetailsCommand implements ActionCommand {
    private static final Logger LOG = LogManager.getLogger();

    private static final String PARAM_ID = "id";
    private static final String PARAM_ERROR = "error";
    private static final String PARAM_USER = "user";
    private static final String PARAM_USER_BLOCKINGS = "userBlockings";
    private static final String PARAM_USER_BOOKINGS = "userBookings";

    private static final String PATH_PAGE_USER = "path.page.admin.user";
    private static final String PATH_PAGE_ERROR = "path.page.error.common";

    @Override
    public String execute(HttpServletRequest request) {
        ConfigurationManager manager = new ConfigurationManager();
        UserService service = new UserService();

        long id = Long.parseLong(request.getParameter(PARAM_ID));
        String page = manager.getProperty(PATH_PAGE_ERROR);

        try {
            User user = service.defineUserById(id);
            List<Ban> bans = service.defineBlockingsByUserId(id);
            List<Booking> bookings = service.defineBookingsByUserId(id);

            if (user != null) {
                request.setAttribute(PARAM_USER, user);
                request.setAttribute(PARAM_USER_BLOCKINGS, bans);
                request.setAttribute(PARAM_USER_BOOKINGS, bookings);
                page = manager.getProperty(PATH_PAGE_USER);
            }
        } catch (ServiceException e) {
            LOG.log(Level.ERROR, e);
            request.setAttribute(PARAM_ERROR, e);
        }

        return page;
    }
}
