package com.epam.hostel.command;

import com.epam.hostel.entity.Ban;
import com.epam.hostel.entity.BankAccount;
import com.epam.hostel.entity.User;
import com.epam.hostel.exception.ServiceException;
import com.epam.hostel.manager.ConfigurationManager;
import com.epam.hostel.service.BookService;
import com.epam.hostel.service.ProfileService;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

public class ToProfileCommand implements ActionCommand {
    private static final Logger LOG = LogManager.getLogger();

    private static final String PARAM_USER = "user";
    private static final String PARAM_ACCOUNTS = "accounts";
    private static final String PARAM_BLOCKING = "blockings";
    private static final String PARAM_ERROR = "error";

    private static final String PATH_PAGE_PROFILE = "path.page.user.profile";
    private static final String PATH_PAGE_ERROR = "path.page.error.common";

    @Override
    public String execute(HttpServletRequest request) {
        HttpSession session = request.getSession();
        ConfigurationManager manager = new ConfigurationManager();
        BookService bookService = new BookService();
        ProfileService profileService = new ProfileService();

        User user = (User) session.getAttribute(PARAM_USER);
        String page = manager.getProperty(PATH_PAGE_PROFILE);

        try {
            List<BankAccount> accounts = bookService.collectAccountsByUserId(user.getId());
            List<Ban> bans = profileService.defineBlockingByUserId(user.getId());

            if (accounts != null) {
                request.setAttribute(PARAM_ACCOUNTS, accounts);
            }

            if (bans != null) {
                request.setAttribute(PARAM_BLOCKING, bans);
            }
        } catch (ServiceException e) {
            LOG.log(Level.ERROR, e);
            request.setAttribute(PARAM_ERROR, e);
            page = manager.getProperty(PATH_PAGE_ERROR);
        }

        return page;
    }
}
