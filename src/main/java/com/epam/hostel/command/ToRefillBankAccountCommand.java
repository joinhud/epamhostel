package com.epam.hostel.command;

import com.epam.hostel.entity.BankAccount;
import com.epam.hostel.exception.ServiceException;
import com.epam.hostel.manager.ConfigurationManager;
import com.epam.hostel.service.BankAccountService;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

public class ToRefillBankAccountCommand implements ActionCommand {
    private static final Logger LOG = LogManager.getLogger();

    private static final String PARAM_ID = "id";
    private static final String PARAM_ACCOUNT = "account";
    private static final String PARAM_ERROR = "error";

    private static final String PATH_PAGE_REFILL = "path.page.user.refill";
    private static final String PATH_PAGE_ERROR = "path.page.error.common";

    @Override
    public String execute(HttpServletRequest request) {
        BankAccountService service = new BankAccountService();
        ConfigurationManager manager = new ConfigurationManager();

        long accountNumber = Long.parseLong(request.getParameter(PARAM_ID));
        BankAccount account;

        String page = manager.getProperty(PATH_PAGE_ERROR);

        try {
            account = service.defineAccountByNumber(accountNumber);

            if (account != null) {
                request.setAttribute(PARAM_ACCOUNT, account);
                page = manager.getProperty(PATH_PAGE_REFILL);
            }
        } catch (ServiceException e) {
            LOG.log(Level.ERROR, e);
            request.setAttribute(PARAM_ERROR, e);
        }

        return page;
    }
}
