package com.epam.hostel.command;

import com.epam.hostel.exception.ServiceException;
import com.epam.hostel.manager.ConfigurationManager;
import com.epam.hostel.service.CommentService;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

public class DeleteCommentCommand implements ActionCommand {
    private static final Logger LOG = LogManager.getLogger();

    private static final String PARAM_COMMENT_ID = "id";
    private static final String PARAM_ERROR = "error";
    private static final String PARAM_SUCCESS = "success";

    private static final String PATH_PAGE_ERROR = "path.page.error.common";
    private static final String PATH_URL_COMMENTS = "path.url.comments";

    private static final String ERROR_DELETE_COMMENT = "Can not delete comment.";

    @Override
    public String execute(HttpServletRequest request) {
        ConfigurationManager manager = new ConfigurationManager();
        CommentService commentService = new CommentService();

        long id = Long.parseLong(request.getParameter(PARAM_COMMENT_ID));
        String page = manager.getProperty(PATH_PAGE_ERROR);

        request.setAttribute(PARAM_SUCCESS, false);

        try {
            boolean removed = commentService.deleteCommentById(id);

            if (!removed) {
                request.setAttribute(PARAM_ERROR, ERROR_DELETE_COMMENT);
            } else {
                request.setAttribute(PARAM_SUCCESS, true);
                page = manager.getProperty(PATH_URL_COMMENTS);
            }
        } catch (ServiceException e) {
            LOG.log(Level.ERROR, e);
            request.setAttribute(PARAM_ERROR, e);
            page = manager.getProperty(PATH_PAGE_ERROR);
        }

        return page;
    }
}
