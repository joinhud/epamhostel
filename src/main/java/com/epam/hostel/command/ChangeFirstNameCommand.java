package com.epam.hostel.command;

import com.epam.hostel.entity.Ban;
import com.epam.hostel.entity.BankAccount;
import com.epam.hostel.entity.User;
import com.epam.hostel.exception.ServiceException;
import com.epam.hostel.manager.ConfigurationManager;
import com.epam.hostel.service.BookService;
import com.epam.hostel.service.ProfileService;
import com.epam.hostel.validator.UserValidator;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

public class ChangeFirstNameCommand implements ActionCommand {
    private static final Logger LOG = LogManager.getLogger();

    private static final String PARAM_FIRST_NAME = "firstName";
    private static final String PARAM_ERROR_FIRST_NAME = "errorFirstName";
    private static final String PARAM_USER = "user";
    private static final String PARAM_ERROR = "error";
    private static final String PARAM_ACCOUNTS = "accounts";
    private static final String PARAM_BLOCKING = "blockings";
    private static final String PARAM_SUCCESS = "success";

    private static final String PATH_PAGE_ERROR = "path.page.error.user.change.profile";
    private static final String PATH_PAGE_PROFILE = "path.page.user.profile";
    private static final String PATH_URL_PROFILE = "path.url.user.profile";

    private static final String ERROR_UPDATING_MSG = "Can not update user First Name.";

    @Override
    public String execute(HttpServletRequest request) {
        HttpSession session = request.getSession();
        UserValidator validator = new UserValidator();
        ProfileService profileService = new ProfileService();
        BookService bookService = new BookService();
        ConfigurationManager manager = new ConfigurationManager();

        String firstName = request.getParameter(PARAM_FIRST_NAME);
        User user = (User) session.getAttribute(PARAM_USER);
        String page = manager.getProperty(PATH_PAGE_PROFILE);
        request.setAttribute(PARAM_SUCCESS, false);

        String errorMsg = "";

        errorMsg = validator.validateName(firstName);

        try {
            if (errorMsg.isEmpty()) {
                user.setFirstName(firstName);

                if (!profileService.updateUserData(user)) {
                    request.setAttribute(PARAM_ERROR, ERROR_UPDATING_MSG);
                    page = manager.getProperty(PATH_PAGE_ERROR);
                } else {
                    request.setAttribute(PARAM_SUCCESS, true);
                    page = manager.getProperty(PATH_URL_PROFILE);
                }
            } else {
                List<BankAccount> accounts = bookService.collectAccountsByUserId(user.getId());
                List<Ban> bans = profileService.defineBlockingByUserId(user.getId());

                if (accounts != null) {
                    request.setAttribute(PARAM_ACCOUNTS, accounts);
                }

                if (bans != null) {
                    request.setAttribute(PARAM_BLOCKING, bans);
                }

                request.setAttribute(PARAM_ERROR_FIRST_NAME, errorMsg);
            }
        } catch (ServiceException e) {
            LOG.log(Level.ERROR, e);
            request.setAttribute(PARAM_ERROR, e);
            page = manager.getProperty(PATH_PAGE_ERROR);
        }

        return page;
    }
}
