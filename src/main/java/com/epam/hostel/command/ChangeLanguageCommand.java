package com.epam.hostel.command;

import com.epam.hostel.manager.TextManager;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Locale;

public class ChangeLanguageCommand implements ActionCommand {
    private static final int PARAM_FIRST = 0;
    private static final int PARAM_SECOND = 1;

    private static final String PARAM_LOCALE = "locale";
    private static final String PARAM_LANGUAGE = "language";
    private static final String PARAM_CURR_LANGUAGE = "currLanguage";
    private static final String PARAM_QUERY = "query";
    private static final String PARAM_SUCCESS = "success";

    private static final String DEFAULT_LOCALE_VALUE = "en_US";
    private static final String DEFAULT_LANGUAGE_NAME = "English";

    private static final String LOCALE_SPLITTER = "_";
    private static final String REQUEST_PARAM_SPLITTER = "?";

    @Override
    public String execute(HttpServletRequest request) {
        String pageLocale = request.getParameter(PARAM_LOCALE);
        String language = request.getParameter(PARAM_LANGUAGE);
        String query = request.getParameter(PARAM_QUERY);
        String resultUri = request.getRequestURI();

        HttpSession session = request.getSession();

        if(!pageLocale.isEmpty()) {
            session.setAttribute(PARAM_LOCALE, pageLocale);

            String[] localeValues = pageLocale.split(LOCALE_SPLITTER);
            Locale locale = new Locale(localeValues[PARAM_FIRST], localeValues[PARAM_SECOND]);
            TextManager manager = new TextManager(locale);

            session.setAttribute(PARAM_CURR_LANGUAGE, manager.getProperty(language));
        } else {
            session.setAttribute(PARAM_LOCALE, DEFAULT_LOCALE_VALUE);
            session.setAttribute(PARAM_CURR_LANGUAGE, DEFAULT_LANGUAGE_NAME);
        }

        if (!query.isEmpty()) {
            resultUri += REQUEST_PARAM_SPLITTER + query;
        }

        request.setAttribute(PARAM_SUCCESS, true);

        return resultUri;
    }
}
