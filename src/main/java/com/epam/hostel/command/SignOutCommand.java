package com.epam.hostel.command;

import com.epam.hostel.manager.ConfigurationManager;
import com.epam.hostel.type.ClientType;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class SignOutCommand implements ActionCommand {

    private static final String PARAM_ROLE = "role";
    private static final String PARAM_USER = "user";
    private static final String PARAM_SUCCESS = "success";

    private static final String PATH_URL_INDEX = "path.url.index";

    @Override
    public String execute(HttpServletRequest request) {
        ConfigurationManager manager = new ConfigurationManager();
        HttpSession session = request.getSession();
        String page = manager.getProperty(PATH_URL_INDEX);

        session.setAttribute(PARAM_ROLE, ClientType.GUEST);
        session.removeAttribute(PARAM_USER);
        request.setAttribute(PARAM_SUCCESS, true);

        return page;
    }
}
