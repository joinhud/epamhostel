package com.epam.hostel.command;

import com.epam.hostel.entity.Booking;
import com.epam.hostel.entity.User;
import com.epam.hostel.exception.ServiceException;
import com.epam.hostel.manager.ConfigurationManager;
import com.epam.hostel.service.BookService;
import com.epam.hostel.validator.BankAccountValidator;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class CancelReservationCommand implements ActionCommand {
    private static final Logger LOG = LogManager.getLogger();

    private static final String PARAM_ID = "bookingId";
    private static final String PARAM_ACCOUNT = "bankAccounts";
    private static final String PARAM_ERROR_MSG = "errorMsg";
    private static final String PARAM_ERROR = "error";
    private static final String PARAM_USER = "user";
    private static final String PARAM_SUCCESS = "success";

    private static final String PATH_PAGE_CANCEL_RESERVATION = "path.page.error.user.cancel.reservation";
    private static final String PATH_PAGE_ERROR = "path.page.error.user.bookings";
    private static final String PATH_URL_BOOKINGS = "path.url.user.bookings";

    private static final String ERROR_MSG_RETURNED_MONEY = "Money wasn't returned!";

    @Override
    public String execute(HttpServletRequest request) {
        ConfigurationManager manager = new ConfigurationManager();
        BankAccountValidator validator = new BankAccountValidator();
        BookService service = new BookService();
        long bookingId = Long.parseLong(request.getParameter(PARAM_ID));

        String page = manager.getProperty(PATH_PAGE_CANCEL_RESERVATION);
        request.setAttribute(PARAM_SUCCESS, false);

        Booking booking = null;

        try {
            booking = service.defineById(bookingId);
            String errorMsg = "";

            if (booking.isPaid()) {
                String bankAccountName = request.getParameter(PARAM_ACCOUNT);

                errorMsg = validator.validateAccountNumber(bankAccountName);

                if (!errorMsg.isEmpty()) {
                    request.setAttribute(PARAM_ERROR_MSG, errorMsg);
                } else {
                    long bankAccount = Long.parseLong(bankAccountName);

                    if (!service.returnMoney(bankAccount, booking.getTotalCost())) {
                        errorMsg = ERROR_MSG_RETURNED_MONEY;
                        request.setAttribute(PARAM_ERROR_MSG, errorMsg);
                    }
                }
            }

            if (errorMsg.isEmpty()) {
                if (service.removeByBookingId(bookingId)) {
                    request.setAttribute(PARAM_SUCCESS, true);
                    page = manager.getProperty(PATH_URL_BOOKINGS);
                }
            }
        } catch (ServiceException e) {
            LOG.log(Level.ERROR, e);
            request.setAttribute(PARAM_ERROR, e);
            page = manager.getProperty(PATH_PAGE_ERROR);
        }

        return page;
    }
}
