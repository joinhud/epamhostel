package com.epam.hostel.command;

import com.epam.hostel.entity.Ban;
import com.epam.hostel.entity.Booking;
import com.epam.hostel.entity.User;
import com.epam.hostel.exception.ServiceException;
import com.epam.hostel.manager.ConfigurationManager;
import com.epam.hostel.service.UserService;
import com.epam.hostel.validator.UserValidator;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public class ChangeUserDiscountCommand implements ActionCommand {
    private static final Logger LOG = LogManager.getLogger();

    private static final String PARAM_ID = "userId";
    private static final String PARAM_ERROR = "error";
    private static final String PARAM_ERROR_DISCOUNT_MSG = "errorDiscountMsg";
    private static final String PARAM_NEW_DISCOUNT = "newDiscount";
    private static final String PARAM_USER = "user";
    private static final String PARAM_USER_BLOCKINGS = "userBlockings";
    private static final String PARAM_USER_BOOKINGS = "userBookings";
    private static final String PARAM_SUCCESS = "success";

    private static final String PATH_PAGE_ERROR = "path.page.error.common";
    private static final String PATH_PAGE_USER = "path.page.admin.user";
    private static final String PATH_URL_USER = "path.url.admin.user";

    @Override
    public String execute(HttpServletRequest request) {
        ConfigurationManager manager = new ConfigurationManager();
        UserService service = new UserService();
        UserValidator validator = new UserValidator();

        long id = Long.parseLong(request.getParameter(PARAM_ID));
        String discountName = request.getParameter(PARAM_NEW_DISCOUNT);
        String page = manager.getProperty(PATH_PAGE_USER);
        request.setAttribute(PARAM_SUCCESS, false);

        String errorMsgDiscount = validator.validateDiscount(discountName);

        try {
            if (!errorMsgDiscount.isEmpty()) {
                User user = service.defineUserById(id);

                if (user != null) {
                    List<Booking> bookings = service.defineBookingsByUserId(id);
                    List<Ban> bans = service.defineBlockingsByUserId(id);
                    request.setAttribute(PARAM_USER, user);
                    request.setAttribute(PARAM_USER_BLOCKINGS, bans);
                    request.setAttribute(PARAM_USER_BOOKINGS, bookings);
                    request.setAttribute(PARAM_ERROR_DISCOUNT_MSG, errorMsgDiscount);
                }
            } else {
                float discount = Float.parseFloat(discountName);

                if (service.changeUserDiscount(id, discount)) {
                    request.setAttribute(PARAM_SUCCESS, true);
                    page = manager.getProperty(PATH_URL_USER) + id;
                }
            }
        } catch (ServiceException e) {
            LOG.log(Level.ERROR, e);
            request.setAttribute(PARAM_ERROR, e);
            page = manager.getProperty(PATH_PAGE_ERROR);
        }


        return page;
    }
}
