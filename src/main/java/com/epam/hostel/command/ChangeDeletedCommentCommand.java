package com.epam.hostel.command;

import com.epam.hostel.exception.ServiceException;
import com.epam.hostel.manager.ConfigurationManager;
import com.epam.hostel.service.CommentService;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

public class ChangeDeletedCommentCommand implements ActionCommand {
    private static final Logger LOG = LogManager.getLogger();

    private static final String PARAM_ID = "commentId";
    private static final String PARAM_DELETED = "deleted";
    private static final String PARAM_COMMENTS_TYPE = "commentsType";
    private static final String PARAM_CURR_PAGE = "currPage";
    private static final String PARAM_ERROR = "error";
    private static final String PARAM_SUCCESS = "success";

    private static final String QUERY_PAGE_INDEX = "&pageIndex=";
    private static final String QUERY_TYPE = "&type=";

    private static final String PATH_PAGE_ERROR = "path.page.error.common";
    private static final String PATH_URL_COMMENTS = "path.url.comments";

    @Override
    public String execute(HttpServletRequest request) {
        ConfigurationManager manager = new ConfigurationManager();
        CommentService commentService = new CommentService();

        long commentId = Long.parseLong(request.getParameter(PARAM_ID));
        boolean deleted = Boolean.parseBoolean(request.getParameter(PARAM_DELETED));
        String commentsType = request.getParameter(PARAM_COMMENTS_TYPE);
        String currPage = request.getParameter(PARAM_CURR_PAGE);

        String page = manager.getProperty(PATH_PAGE_ERROR);
        request.setAttribute(PARAM_SUCCESS, false);

        try {
            if (commentService.changeDeletedValue(commentId, deleted)) {
                request.setAttribute(PARAM_SUCCESS, true);
                page = manager.getProperty(PATH_URL_COMMENTS) +
                        QUERY_PAGE_INDEX + currPage +
                        QUERY_TYPE + commentsType;
            }
        } catch (ServiceException e) {
            LOG.log(Level.ERROR, e);
            request.setAttribute(PARAM_ERROR, e);
        }

        return page;
    }
}
