package com.epam.hostel.command;

import com.epam.hostel.entity.Room;
import com.epam.hostel.exception.ServiceException;
import com.epam.hostel.manager.ConfigurationManager;
import com.epam.hostel.service.RoomService;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

public class ToUploadImageCommand implements ActionCommand {
    private static final Logger LOG = LogManager.getLogger();

    private static final String PARAM_ID = "id";
    private static final String PARAM_ROOM = "room";
    private static final String PARAM_ERROR = "error";

    private static final String PATH_PAGE_UPLOAD = "path.page.admin.upload.image";
    private static final String PATH_PAGE_ERROR = "path.page.error.common";

    private static final String ERROR_DEFINE_ROOM = "Can not define room by number.";

    @Override
    public String execute(HttpServletRequest request) {
        ConfigurationManager manager = new ConfigurationManager();
        RoomService roomService = new RoomService();

        String roomNumberName = request.getParameter(PARAM_ID);
        String page = manager.getProperty(PATH_PAGE_ERROR);

        try {
            Room room = null;

            if (roomNumberName != null) {
                long roomNumber = Long.parseLong(roomNumberName);
                room = roomService.defineRoomById(roomNumber);
            }

            if (room != null) {
                request.setAttribute(PARAM_ROOM, room);
                page = manager.getProperty(PATH_PAGE_UPLOAD);
            } else {
                request.setAttribute(PARAM_ERROR, ERROR_DEFINE_ROOM);
            }
        } catch (ServiceException e) {
            LOG.log(Level.ERROR, e);
            request.setAttribute(PARAM_ERROR, e);
        }

        return page;
    }
}
