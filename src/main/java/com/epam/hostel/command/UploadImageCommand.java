package com.epam.hostel.command;

import com.epam.hostel.entity.Room;
import com.epam.hostel.exception.ServiceException;
import com.epam.hostel.manager.ConfigurationManager;
import com.epam.hostel.service.RoomService;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.util.List;

public class UploadImageCommand implements ActionCommand {
    private static final Logger LOG = LogManager.getLogger();

    private static final String UPLOAD_DIRECTORY = "/img/";
    private static final String TEMP_DIR_ATTR = "javax.servlet.context.tempdir";

    private static final String PARAM_IMAGE = "image";
    private static final String PARAM_ROOM_ID = "roomId";
    private static final String PARAM_ROOM = "room";
    private static final String PARAM_ERROR_UPLOAD_MSG = "errorUploadMsg";
    private static final String PARAM_ERROR = "error";
    private static final String PARAM_SUCCESS = "success";

    private static final String PATH_PAGE_UPLOAD = "path.page.admin.upload.image";
    private static final String PATH_PAGE_ERROR = "path.page.error.common";
    private static final String PATH_URL_ROOM = "path.url.admin.room";

    private static final String ERROR_UPDATE_FILE = "Can not update room image. Try again later.";
    private static final String ERROR_UPLOAD_FILE = "Can not upload room image. Try again later.";

    @Override
    public String execute(HttpServletRequest request) {
        RoomService roomService = new RoomService();
        ConfigurationManager manager = new ConfigurationManager();

        File tempDir = (File) request.getServletContext().getAttribute(TEMP_DIR_ATTR);
        ServletFileUpload upload = roomService.initFileUploader(tempDir);
        String uploadPath = request.getServletContext().getRealPath(UPLOAD_DIRECTORY);
        String page = manager.getProperty(PATH_PAGE_UPLOAD);
        request.setAttribute(PARAM_SUCCESS, false);

        try {
            List<FileItem> items = upload.parseRequest(request);

            String roomIdName = roomService.getParameter(PARAM_ROOM_ID, items);
            String uploadFileName = roomService.uploadFile(PARAM_IMAGE, items, uploadPath);
            Room room = null;

            if (roomIdName != null && uploadFileName != null) {
                long roomId = Long.parseLong(roomIdName);

                room = roomService.defineRoomById(roomId);
                room.setImageUrl(UPLOAD_DIRECTORY + uploadFileName);
            } else {
                request.setAttribute(PARAM_ERROR_UPLOAD_MSG, ERROR_UPLOAD_FILE);
                return page;
            }

            if (roomService.updateRoomData(room)) {
                page = manager.getProperty(PATH_URL_ROOM) + room.getId();
                request.setAttribute(PARAM_SUCCESS, true);
            } else {
                request.setAttribute(PARAM_ERROR_UPLOAD_MSG, ERROR_UPDATE_FILE);
                request.setAttribute(PARAM_ROOM, room);
            }
        } catch (FileUploadException | ServiceException e) {
            LOG.log(Level.ERROR, e);
            request.setAttribute(PARAM_ERROR, e);
            page = manager.getProperty(PATH_PAGE_ERROR);
        }

        return page;
    }
}
