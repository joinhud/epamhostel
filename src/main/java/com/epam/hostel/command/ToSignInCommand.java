package com.epam.hostel.command;

import com.epam.hostel.manager.ConfigurationManager;

import javax.servlet.http.HttpServletRequest;

public class ToSignInCommand implements ActionCommand {
    private static final String PATH_PAGE_SIGN_IN = "path.page.sign.in";

    @Override
    public String execute(HttpServletRequest request) {
        ConfigurationManager manager = new ConfigurationManager();
        return manager.getProperty(PATH_PAGE_SIGN_IN);
    }
}
