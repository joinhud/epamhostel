package com.epam.hostel.command;

import com.epam.hostel.entity.Booking;
import com.epam.hostel.entity.User;
import com.epam.hostel.exception.ServiceException;
import com.epam.hostel.manager.ConfigurationManager;
import com.epam.hostel.service.BankAccountService;
import com.epam.hostel.service.BookService;
import com.epam.hostel.type.BookingState;
import com.epam.hostel.validator.BankAccountValidator;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

public class PayBookingCommand implements ActionCommand {
    private static final Logger LOG = LogManager.getLogger();

    private static final String PARAM_ROOM = "number";
    private static final String PARAM_NUM_OF_LIVING = "numberOfLiving";
    private static final String PARAM_FROM_DATE = "fromDate";
    private static final String PARAM_TO_DATE = "toDate";
    private static final String PARAM_TOTAL_COST = "totalCost";
    private static final String PARAM_PASSWORD = "password";
    private static final String PARAM_ACCOUNT = "bankAccount";
    private static final String PARAM_PASSWORD_ERROR = "errorPassword";
    private static final String PARAM_BOOKING = "booking";
    private static final String PARAM_BANK_ACC = "bankAccounts";
    private static final String PARAM_USER = "user";
    private static final String PARAM_ERROR = "error";
    private static final String PARAM_SUCCESS = "success";

    private static final String PATH_PAGE_PAYMENT_BOOK = "path.page.user.payment.book";
    private static final String PATH_PAGE_ERROR = "path.page.error.pay.booking";
    private static final String PATH_URL_BOOKINGS = "path.url.user.bookings";

    @Override
    public String execute(HttpServletRequest request) {
        ConfigurationManager manager = new ConfigurationManager();
        BankAccountValidator validator = new BankAccountValidator();
        BankAccountService accountService = new BankAccountService();
        BookService bookService = new BookService();
        HttpSession session = request.getSession();

        long room = Long.parseLong(request.getParameter(PARAM_ROOM));
        int numberOfLiving = Integer.parseInt(request.getParameter(PARAM_NUM_OF_LIVING));
        LocalDate fromDate = LocalDate.parse(request.getParameter(PARAM_FROM_DATE));
        LocalDate toDate = LocalDate.parse(request.getParameter(PARAM_TO_DATE));
        BigDecimal totalCost = new BigDecimal(request.getParameter(PARAM_TOTAL_COST));
        long accountNumber = Long.parseLong(request.getParameter(PARAM_ACCOUNT));
        String password = request.getParameter(PARAM_PASSWORD);

        String errorMsg = validator.validatePassword(password);

        if (errorMsg.isEmpty()) {
            try {
                errorMsg = accountService.isCorrectPassword(accountNumber, password);
            } catch (ServiceException e) {
                LOG.log(Level.ERROR, e);
                request.setAttribute(PARAM_ERROR, e);
                return manager.getProperty(PATH_PAGE_ERROR);
            }
        }

        if (errorMsg.isEmpty()) {
            try {
                errorMsg = accountService.payBooking(accountNumber, totalCost);
            } catch (ServiceException e) {
                LOG.log(Level.ERROR, e);
                request.setAttribute(PARAM_ERROR, e);
                return manager.getProperty(PATH_PAGE_ERROR);
            }
        }

        String page = manager.getProperty(PATH_PAGE_PAYMENT_BOOK);
        User user = (User) session.getAttribute(PARAM_USER);
        request.setAttribute(PARAM_SUCCESS, false);

        if (!errorMsg.isEmpty()) {
            Booking booking = new Booking(room, user.getId(), LocalDateTime.now(), numberOfLiving,
                    fromDate, toDate, true, totalCost, BookingState.PROCESSING);
            request.setAttribute(PARAM_BOOKING, booking);
            request.setAttribute(PARAM_PASSWORD_ERROR, errorMsg);
            request.setAttribute(PARAM_BANK_ACC, accountNumber);
        } else {
            try {
                bookService.createBooking(room, user.getId(), numberOfLiving, fromDate, toDate, true, totalCost);
                request.setAttribute(PARAM_SUCCESS, true);
                page = manager.getProperty(PATH_URL_BOOKINGS);
            } catch (ServiceException e) {
                LOG.log(Level.ERROR, e);
                request.setAttribute(PARAM_ERROR, e);
                page = manager.getProperty(PATH_PAGE_ERROR);
            }
        }

        return page;
    }
}
