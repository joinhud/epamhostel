package com.epam.hostel.command;

import com.epam.hostel.entity.User;
import com.epam.hostel.exception.ServiceException;
import com.epam.hostel.manager.ConfigurationManager;
import com.epam.hostel.service.SignInService;
import com.epam.hostel.type.ClientType;
import com.epam.hostel.validator.UserValidator;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class SignInCommand implements ActionCommand {
    private static final Logger LOG = LogManager.getLogger();

    private static final String PARAM_EMAIL = "email";
    private static final String PARAM_PASSWORD = "password";
    private static final String PARAM_ERROR_EMAIL_MSG = "errorEmailMsg";
    private static final String PARAM_ERROR_PASS_MSG = "errorPasswordMsg";
    private static final String PARAM_ROLE = "role";
    private static final String PARAM_USER = "user";
    private static final String PARAM_ERROR = "error";
    private static final String PARAM_SUCCESS = "success";

    private static final String PATH_PAGE_SIGN_IN = "path.page.sign.in";
    private static final String PATH_PAGE_ERROR = "path.page.error.sign.in";
    private static final String PATH_URL_INDEX = "path.url.index";

    @Override
    public String execute(HttpServletRequest request) {
        boolean valid = true;
        SignInService signInService = new SignInService();
        UserValidator validator = new UserValidator();
        HttpSession session = request.getSession();
        ConfigurationManager manager = new ConfigurationManager();
        String page = manager.getProperty(PATH_PAGE_SIGN_IN);

        String email = request.getParameter(PARAM_EMAIL);
        String password = request.getParameter(PARAM_PASSWORD);
        request.setAttribute(PARAM_SUCCESS, false);

        String errMsgEmail = "";

        //validating email address
        try {
            errMsgEmail = signInService.isEmailExist(email);

            if (errMsgEmail.isEmpty()) {
                errMsgEmail = validator.validEmail(email);
            }
        } catch (ServiceException e) {
            LOG.log(Level.ERROR, e);
            request.setAttribute(PARAM_ERROR, e);
            return manager.getProperty(PATH_PAGE_ERROR);
        }

        if (!errMsgEmail.isEmpty()) {
            valid = false;
            request.setAttribute(PARAM_ERROR_EMAIL_MSG, errMsgEmail);
        }

        String errMsgPassword = "";

        //validating password
        try {
            errMsgPassword = validator.validatePassword(password);

            if (errMsgPassword.isEmpty()) {
                errMsgPassword = signInService.isCorrectPassword(email, password);
            }
        } catch (ServiceException e) {
            LOG.log(Level.ERROR, e);
            request.setAttribute(PARAM_ERROR, e);
            return manager.getProperty(PATH_PAGE_ERROR);
        }

        if (!errMsgPassword.isEmpty()) {
            valid = false;
            request.setAttribute(PARAM_ERROR_PASS_MSG, errMsgPassword);
        }

        if (valid) {
            try {
                User user = signInService.defineUserByEmail(email);

                if (signInService.isAdmin(email)) {
                    session.setAttribute(PARAM_ROLE, ClientType.ADMIN);
                } else {
                    session.setAttribute(PARAM_ROLE, ClientType.USER);
                }

                session.setAttribute(PARAM_USER, user);
                request.setAttribute(PARAM_SUCCESS, true);
                page = manager.getProperty(PATH_URL_INDEX);
            } catch (ServiceException e) {
                LOG.log(Level.ERROR, e);
                request.setAttribute(PARAM_ERROR, e);
                page = manager.getProperty(PATH_PAGE_ERROR);
            }
        } else {
            request.setAttribute(PARAM_ERROR_EMAIL_MSG, errMsgEmail);
            request.setAttribute(PARAM_ERROR_PASS_MSG, errMsgPassword);
        }

        return page;
    }
}
