package com.epam.hostel.command;

import com.epam.hostel.entity.User;
import com.epam.hostel.exception.ServiceException;
import com.epam.hostel.manager.ConfigurationManager;
import com.epam.hostel.service.BankAccountService;
import com.epam.hostel.validator.BankAccountValidator;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.math.BigDecimal;

public class AddAccountCommand implements ActionCommand {
    private static final Logger LOG = LogManager.getLogger();

    private static final String PARAM_ACCOUNT = "account";
    private static final String PARAM_MONEY = "money";
    private static final String PARAM_PASSWORD = "password";
    private static final String PARAM_CONFIRMATION = "passConfirm";
    private static final String PARAM_ERROR_ACCOUNT = "errorAccount";
    private static final String PARAM_ERROR_PASSWORD = "errorPassword";
    private static final String PARAM_ERROR_CONFIRMATION = "errorPassConfirm";
    private static final String PARAM_ERROR_MONEY = "errorMoney";
    private static final String PARAM_USER = "user";
    private static final String PARAM_ERROR = "error";
    private static final String PARAM_SUCCESS = "success";

    private static final String PATH_PAGE_ADD_ACCOUNT = "path.page.user.add.account";
    private static final String PATH_URL_INDEX = "path.url.index";
    private static final String PATH_PAGE_ERROR = "path.page.error.add.account";

    @Override
    public String execute(HttpServletRequest request) {
        BankAccountValidator validator = new BankAccountValidator();
        BankAccountService bankAccountService = new BankAccountService();
        HttpSession session = request.getSession();
        ConfigurationManager manager = new ConfigurationManager();

        long accountNumber = Long.parseLong(request.getParameter(PARAM_ACCOUNT));
        String moneyInput = request.getParameter(PARAM_MONEY);
        String password = request.getParameter(PARAM_PASSWORD);
        String confirmation = request.getParameter(PARAM_CONFIRMATION);

        User user = (User) session.getAttribute(PARAM_USER);
        boolean valid = true;
        String page = manager.getProperty(PATH_PAGE_ADD_ACCOUNT);
        request.setAttribute(PARAM_SUCCESS, false);

        String errorMsgNumber = "";

        try {
            errorMsgNumber = bankAccountService.isAccountExist(accountNumber);

            if (errorMsgNumber.isEmpty()) {
                errorMsgNumber = validator.validateAccountNumber(String.valueOf(accountNumber));
            }
        } catch (ServiceException e) {
            LOG.log(Level.ERROR, e);
            request.setAttribute(PARAM_ERROR, e);
            return manager.getProperty(PATH_PAGE_ERROR);
        }

        if (!errorMsgNumber.isEmpty()) {
            valid = false;
            request.setAttribute(PARAM_ERROR_ACCOUNT, errorMsgNumber);
        }

        String errorMsgPassword = validator.validatePassword(password);

        if (!errorMsgPassword.isEmpty()) {
            valid = false;
            request.setAttribute(PARAM_ERROR_PASSWORD, errorMsgPassword);
        }

        String errorMsgConfirmation = validator.checkPasswordConfirmation(password, confirmation);

        if (!errorMsgConfirmation.isEmpty()) {
            valid = false;
            request.setAttribute(PARAM_ERROR_CONFIRMATION, errorMsgConfirmation);
        }

        String errorMsgMoney = validator.validateMoneyInput(moneyInput);

        if (!errorMsgMoney.isEmpty()) {
            valid = false;
            request.setAttribute(PARAM_ERROR_MONEY, errorMsgMoney);
        }

        if (valid) {
            try {
                BigDecimal money = new BigDecimal(moneyInput);
                bankAccountService.createAccount(accountNumber, user.getId(), money, password);
                page = manager.getProperty(PATH_URL_INDEX);
                request.setAttribute(PARAM_SUCCESS, true);
            } catch (ServiceException e) {
                LOG.log(Level.ERROR, e);
                request.setAttribute(PARAM_ERROR, e);
                page = manager.getProperty(PATH_PAGE_ERROR);
            }
        }

        return page;
    }
}
