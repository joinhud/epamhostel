package com.epam.hostel.filter;

import com.epam.hostel.manager.ConfigurationManager;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebFilter(filterName = "pageRedirectSecurityFilter")
public class PageRedirectSecurityFilter implements Filter {
    private static final String PATH_URL_INDEX = "path.url.index";

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        ConfigurationManager manager = new ConfigurationManager();

        response.sendRedirect(manager.getProperty(PATH_URL_INDEX));
    }

    @Override
    public void destroy() {

    }
}
