package com.epam.hostel.filter;

import com.epam.hostel.type.ClientType;
import com.epam.hostel.type.CommandEnum;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.EnumSet;

@WebFilter(filterName = "servletRoleSecurityFilter", servletNames = {"HostelController"},
        initParams = {@WebInitParam(name = "SIGN_IN_PATH", value = "/jsp/signin.jsp")})
public class ServletRoleSecurityFilter implements Filter {
    private static final String PARAM_COMMAND = "command";
    private static final String PARAM_ROLE = "role";
    private static final String PARAM_PATH = "SIGN_IN_PATH";

    private String signInPath;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        signInPath = filterConfig.getInitParameter(PARAM_PATH);
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        HttpSession session = request.getSession(true);

        ClientType role = (ClientType) session.getAttribute(PARAM_ROLE);
        String commandName = request.getParameter(PARAM_COMMAND);
        EnumSet<CommandEnum> userCommands = EnumSet.range(CommandEnum.SIGNOUT, CommandEnum.CHANGE_DELETED_COMMENT);

        if (ClientType.GUEST.equals(role) && commandName != null) {
            CommandEnum command = CommandEnum.valueOf(commandName.toUpperCase());

            if (userCommands.contains(command)) {
                RequestDispatcher rq = request.getServletContext().getRequestDispatcher(signInPath);
                rq.forward(request, response);

                return;
            }
        }

        filterChain.doFilter(servletRequest, servletResponse);
    }

    @Override
    public void destroy() {
    }
}
