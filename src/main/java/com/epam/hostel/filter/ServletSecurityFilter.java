package com.epam.hostel.filter;


import com.epam.hostel.type.ClientType;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebFilter(filterName = "servletSecurityFilter", servletNames = {"HostelController"})
public class ServletSecurityFilter implements Filter {
    private static final String PARAM_ROLE = "role";
    private static final String PARAM_LOCALE = "locale";
    private static final String PARAM_CURR_LANGUAGE = "currLanguage";

    private static final String DEFAULT_LOCALE = "en_US";
    private static final String DEFAULT_LANGUAGE = "English";

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
            throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpSession session = request.getSession();

        ClientType role = (ClientType) session.getAttribute(PARAM_ROLE);
        String locale = (String) session.getAttribute(PARAM_LOCALE);

        if (role == null) {
            session.setAttribute(PARAM_ROLE, ClientType.GUEST);
        }

        if (locale == null) {
            session.setAttribute(PARAM_LOCALE, DEFAULT_LOCALE);
            session.setAttribute(PARAM_CURR_LANGUAGE, DEFAULT_LANGUAGE);
        }

        filterChain.doFilter(servletRequest, servletResponse);
    }

    @Override
    public void destroy() {
    }
}
