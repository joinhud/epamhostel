package com.epam.hostel.dao;

import com.epam.hostel.entity.Room;
import com.epam.hostel.exception.DAOException;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.Connection;
import java.sql.Statement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class RoomDAO extends AbstractDAO<Room> {
    //columns names
    private static final String COLUMN_NUMBER = "number";
    private static final String COLUMN_IMG_URL = "img_url";
    private static final String COLUMN_MAX_LIVINGS = "max_livings";
    private static final String COLUMN_TYPE = "type";
    private static final String COLUMN_COST = "cost";
    private static final String COLUMN_ON_REPAIR = "on_repair";
    private static final String COLUMN_GRADE = "grade";
    private static final String COLUMN_DESCRIPTION = "description";
    //indexes
    private static final int FIRST_INDEX = 1;
    private static final int SECOND_INDEX = 2;
    private static final int THIRD_INDEX = 3;
    private static final int FOURTH_INDEX = 4;
    private static final int FIFTH_INDEX = 5;
    private static final int SIXTH_INDEX = 6;
    private static final int SEVENTH_INDEX = 7;
    private static final int EIGHTH_INDEX = 8;
    //SQL scripts
    private static final String SQL_SELECT_ALL = "SELECT `number`, `img_url`, `max_livings`, `type`, `cost`, " +
            "`on_repair`, `grade`, `description` FROM `room`";
    private static final String SQL_SELECT_BY_ID = "SELECT number, img_url, max_livings, type, cost, " +
            "on_repair, grade, description FROM room WHERE number = ?";
    private static final String SQL_DELETE_BY_ID = "DELETE FROM room WHERE number = ?";
    private static final String SQL_SELECT_BY_TYPE = "SELECT `number`, `img_url`, `max_livings`, `type`, `cost`, " +
            "`on_repair`, `grade`, `description` FROM `room` WHERE `type` = ? AND `on_repair` = FALSE";
    private static final String SQL_UPDATE_ROOM = "UPDATE `room` SET `img_url`=?, `max_livings`=?, `type`=?, " +
            "`cost`=?, `on_repair`=?, `grade`=?, `description`=? WHERE `number`=?";
    private static final String SQL_INSERT_ROOM = "INSERT INTO `room` (`number`, `img_url`, `max_livings`, `type`, " +
            "`cost`, `on_repair`, `grade`, `description`) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
    private static final String SQL_DELETE_BY_ENTITY = "DELETE FROM `room` WHERE `number` = ? AND `type` = ?";

    public RoomDAO(Connection connection) {
        super(connection);
    }

    @Override
    public List<Room> findAll() throws DAOException {
        List<Room> rooms = new ArrayList<>();
        Statement stmt = null;

        try {
            stmt = connection.createStatement();
            ResultSet resultSet = stmt.executeQuery(SQL_SELECT_ALL);

            while (resultSet.next()) {
                long number = resultSet.getLong(COLUMN_NUMBER);
                String imageUrl = resultSet.getString(COLUMN_IMG_URL);
                short maxLivings = resultSet.getShort(COLUMN_MAX_LIVINGS);
                boolean type = resultSet.getBoolean(COLUMN_TYPE);
                BigDecimal cost = resultSet.getBigDecimal(COLUMN_COST);
                boolean onRepair = resultSet.getBoolean(COLUMN_ON_REPAIR);
                String grade = resultSet.getString(COLUMN_GRADE);
                String description = resultSet.getString(COLUMN_DESCRIPTION);
                rooms.add(new Room(number, imageUrl, maxLivings, type, cost, onRepair, grade, description));
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            this.close(stmt);
        }

        return rooms;
    }

    @Override
    public Room findEntityById(long id) throws DAOException {
        Room room = null;
        PreparedStatement stmt = null;

        try {
            stmt = connection.prepareStatement(SQL_SELECT_BY_ID);
            stmt.setLong(FIRST_INDEX, id);

            ResultSet resultSet = stmt.executeQuery();

            if (resultSet.next()) {
                long number = resultSet.getLong(COLUMN_NUMBER);
                String imageUrl = resultSet.getString(COLUMN_IMG_URL);
                short maxLivings = resultSet.getShort(COLUMN_MAX_LIVINGS);
                boolean type = resultSet.getBoolean(COLUMN_TYPE);
                BigDecimal cost = resultSet.getBigDecimal(COLUMN_COST);
                boolean onRepair = resultSet.getBoolean(COLUMN_ON_REPAIR);
                String grade = resultSet.getString(COLUMN_GRADE);
                String description = resultSet.getString(COLUMN_DESCRIPTION);

                room = new Room(number, imageUrl, maxLivings, type, cost, onRepair, grade, description);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            this.close(stmt);
        }

        return room;
    }

    @Override
    public boolean remove(long id) throws DAOException {
        boolean result = false;
        PreparedStatement stmt = null;

        try {
            stmt = connection.prepareStatement(SQL_DELETE_BY_ID);
            stmt.setLong(1, id);
            stmt.executeUpdate();
            result = true;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            this.close(stmt);
        }

        return result;
    }

    @Override
    public boolean remove(Room entity) throws DAOException {
        boolean result = false;
        PreparedStatement stmt = null;

        try {
            stmt = connection.prepareStatement(SQL_DELETE_BY_ENTITY);
            stmt.setLong(FIRST_INDEX, entity.getId());
            stmt.setBoolean(SECOND_INDEX, entity.getType());
            stmt.executeUpdate();

            result = true;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            this.close(stmt);
        }

        return result;
    }

    @Override
    public boolean create(Room entity) throws DAOException {
        boolean created = false;
        PreparedStatement stmt = null;

        try {
            stmt = connection.prepareStatement(SQL_INSERT_ROOM);
            stmt.setLong(FIRST_INDEX, entity.getId());
            stmt.setString(SECOND_INDEX, entity.getImageUrl());
            stmt.setShort(THIRD_INDEX, entity.getMaxLivings());
            stmt.setBoolean(FOURTH_INDEX, entity.getType());
            stmt.setBigDecimal(FIFTH_INDEX, entity.getCost());
            stmt.setBoolean(SIXTH_INDEX, entity.isOnRepair());
            stmt.setString(SEVENTH_INDEX, entity.getGrade());
            stmt.setString(EIGHTH_INDEX, entity.getDescription());
            stmt.executeUpdate();

            created = true;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            this.close(stmt);
        }

        return created;
    }

    @Override
    public Room update(Room entity) throws DAOException {
        Room updated = null;
        PreparedStatement stmt = null;

        try {
            stmt = connection.prepareStatement(SQL_UPDATE_ROOM);
            stmt.setString(FIRST_INDEX, entity.getImageUrl());
            stmt.setShort(SECOND_INDEX, entity.getMaxLivings());
            stmt.setBoolean(THIRD_INDEX, entity.getType());
            stmt.setBigDecimal(FOURTH_INDEX, entity.getCost());
            stmt.setBoolean(FIFTH_INDEX, entity.isOnRepair());
            stmt.setString(SIXTH_INDEX, entity.getGrade());
            stmt.setString(SEVENTH_INDEX, entity.getDescription());
            stmt.setLong(EIGHTH_INDEX, entity.getId());
            stmt.executeUpdate();

            updated = entity;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            this.close(stmt);
        }

        return updated;
    }

    public List<Room> findAllByType(boolean type) throws DAOException {
        List<Room> rooms = new ArrayList<>();
        PreparedStatement stmt = null;

        try {
            stmt = connection.prepareStatement(SQL_SELECT_BY_TYPE);
            stmt.setBoolean(FIRST_INDEX, type);
            ResultSet resultSet = stmt.executeQuery();

            while (resultSet.next()) {
                long number = resultSet.getLong(COLUMN_NUMBER);
                String imageUrl = resultSet.getString(COLUMN_IMG_URL);
                short maxLivings = resultSet.getShort(COLUMN_MAX_LIVINGS);
                BigDecimal cost = resultSet.getBigDecimal(COLUMN_COST);
                boolean onRepair = resultSet.getBoolean(COLUMN_ON_REPAIR);
                String grade = resultSet.getString(COLUMN_GRADE);
                String description = resultSet.getString(COLUMN_DESCRIPTION);
                rooms.add(new Room(number, imageUrl, maxLivings, type, cost, onRepair, grade, description));
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            this.close(stmt);
        }

        return rooms;
    }
}
