package com.epam.hostel.dao;

import com.epam.hostel.entity.Booking;
import com.epam.hostel.exception.DAOException;
import com.epam.hostel.type.BookingState;

import java.math.BigDecimal;
import java.sql.*;
import java.sql.Date;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;

public class BookingDAO extends AbstractDAO<Booking> {

    private static final int FIRST_INDEX = 1;
    private static final int SECOND_INDEX = 2;
    private static final int THIRD_INDEX = 3;
    private static final int FOURTH_INDEX = 4;
    private static final int FIFTH_INDEX = 5;
    private static final int SIXTH_INDEX = 6;
    private static final int SEVENTH_INDEX = 7;
    private static final int EIGHTH_INDEX = 8;
    private static final int NINTH_INDEX = 9;

    private static final String COLUMN_ID = "booking_id";
    private static final String COLUMN_ROOM_NUMBER = "rooms_number";
    private static final String COLUMN_USER_ID = "users_id";
    private static final String COLUMN_BOOK_DATE = "book_date";
    private static final String COLUMN_NUMBER_OF_LIVING = "number_of_living";
    private static final String COLUMN_FROM_DATE = "from_date";
    private static final String COLUMN_TO_DATE = "to_date";
    private static final String COLUMN_PAID = "paid";
    private static final String COLUMN_TOTAL_COST = "total_cost";
    private static final String COLUMN_STATE = "state";
    private static final String COLUMN_EMAIL = "email";

    private static final String SQL_INSERT_BOOKING = "INSERT INTO booking (`booking_id`, `rooms_number`, `users_id`, " +
            "`book_date`, `number_of_living`, `from_date`, `to_date`, `paid`, `total_cost`, `state`) " +
            "VALUES (NULL, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
    private static final String SQL_SELECT_FREE_SLEEPS = "SELECT max_livings - " +
            "IFNULL((SELECT sum(number_of_living) " +
            "FROM booking WHERE rooms_number = ? AND from_date <= ? AND to_date >= ?),0) " +
            "FROM room WHERE number = ?";
    private static final String SQL_SELECT_ALL_BY_USER_ID = "SELECT `booking_id`, `rooms_number`, `users_id`, " +
            "`book_date`, `number_of_living`, `from_date`, `to_date`, `paid`, `total_cost`, `state` FROM `booking` " +
            "WHERE users_id = ? " +
            "ORDER BY book_date DESC";
    private static final String SQL_DELETE_BY_ID = "DELETE FROM `booking` WHERE booking_id = ?";
    private static final String SQL_SELECT_BY_ID = "SELECT `booking_id`, `rooms_number`, `users_id`, " +
            "`book_date`, `number_of_living`, `from_date`, `to_date`, `paid`, `total_cost`, `state` FROM `booking` " +
            "WHERE booking_id = ? ";
    private static final String SQL_SELECT_TODAY_BY_STATE = "SELECT `booking_id`, `rooms_number`, `users_id`, `book_date`, " +
            "`number_of_living`, `from_date`, `to_date`, `paid`, `total_cost`, `state`, `email` " +
            "FROM `booking` INNER JOIN `user` ON `booking`.`users_id` = `user`.`id` " +
            "WHERE YEAR(book_date) = YEAR(NOW()) " +
            "AND MONTH(book_date) = MONTH(NOW()) " +
            "AND DAY(book_date) = DAY(NOW()) " +
            "AND state = ? " +
            "ORDER BY book_date DESC";
    private static final String SQL_SELECT_ALL_LIMIT = "SELECT `booking_id`, `rooms_number`, `users_id`, `book_date`, " +
            "`number_of_living`, `from_date`, `to_date`, `paid`, `total_cost`, `state`, `email` " +
            "FROM `booking` INNER JOIN `user` ON `users_id` = `id` " +
            "ORDER BY `book_date` DESC " +
            "LIMIT ? OFFSET ?";
    private static final String SQL_SELECT_LIMIT_BY_STATE = "SELECT `booking_id`, `rooms_number`, `users_id`, " +
            "`book_date`, `number_of_living`, `from_date`, `to_date`, `paid`, `total_cost`, `state`, `email` " +
            "FROM `booking` INNER JOIN `user` ON `users_id` = `id` " +
            "WHERE `state` = ? " +
            "ORDER BY `book_date` DESC " +
            "LIMIT ? OFFSET ?";
    private static final String SQL_SELECT_COUNT_BOOKINGS = "SELECT COUNT(`booking_id`) FROM `booking`";
    private static final String SQL_SELECT_COUNT_BY_STATE = "SELECT COUNT(`booking_id`) FROM `booking` WHERE `state` = ?";
    private static final String SQL_UPDATE_STATE = "UPDATE `booking` SET `state` = ? WHERE `booking_id` = ?";
    private static final String SQL_SELECT_ALL = "SELECT `booking_id`, `rooms_number`, `users_id`, `book_date`, " +
            "`number_of_living`, `from_date`, `to_date`, `paid`, `total_cost`, `state` FROM `booking`";
    private static final String SQL_DELETE_BY_ENTITY = "DELETE FROM `booking` WHERE `booking_id` = ? " +
            "AND `rooms_number` = ? AND `users_id`=? AND `book_date` = ?";

    public BookingDAO(Connection connection) {
        super(connection);
    }

    @Override
    public List<Booking> findAll() throws DAOException {
        List<Booking> bookings = new ArrayList<>();
        Statement stmt = null;

        try {
            stmt = connection.createStatement();
            ResultSet resultSet = stmt.executeQuery(SQL_SELECT_ALL);

            while (resultSet.next()) {
                long id = resultSet.getLong(COLUMN_ID);
                long roomNumber = resultSet.getLong(COLUMN_ROOM_NUMBER);
                long userId = resultSet.getLong(COLUMN_USER_ID);
                LocalDateTime bookingDate = resultSet.getTimestamp(COLUMN_BOOK_DATE).toLocalDateTime();
                int numberOfLiving = resultSet.getInt(COLUMN_NUMBER_OF_LIVING);
                LocalDate fromDate = resultSet.getDate(COLUMN_FROM_DATE).toLocalDate();
                LocalDate toDate = resultSet.getDate(COLUMN_TO_DATE).toLocalDate();
                boolean paid = resultSet.getBoolean(COLUMN_PAID);
                BigDecimal totalCost = resultSet.getBigDecimal(COLUMN_TOTAL_COST);
                BookingState state = BookingState.valueOf(resultSet.getString(COLUMN_STATE).toUpperCase());

                Booking booking = new Booking(id, roomNumber, userId, bookingDate,
                        numberOfLiving, fromDate, toDate, paid, totalCost, state);
                bookings.add(booking);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            this.close(stmt);
        }

        return bookings;
    }

    @Override
    public Booking findEntityById(long id) throws DAOException {
        Booking booking = null;
        PreparedStatement stmt = null;

        try {
            stmt = connection.prepareStatement(SQL_SELECT_BY_ID);
            stmt.setLong(FIRST_INDEX, id);
            ResultSet resultSet = stmt.executeQuery();

            if (resultSet.next()) {
                long roomNumber = resultSet.getLong(COLUMN_ROOM_NUMBER);
                long userId = resultSet.getLong(COLUMN_USER_ID);
                LocalDateTime bookingDate = resultSet.getTimestamp(COLUMN_BOOK_DATE).toLocalDateTime();
                int numberOfLiving = resultSet.getInt(COLUMN_NUMBER_OF_LIVING);
                LocalDate fromDate = resultSet.getDate(COLUMN_FROM_DATE).toLocalDate();
                LocalDate toDate = resultSet.getDate(COLUMN_TO_DATE).toLocalDate();
                boolean paid = resultSet.getBoolean(COLUMN_PAID);
                BigDecimal totalCost = resultSet.getBigDecimal(COLUMN_TOTAL_COST);
                BookingState state = BookingState.valueOf(resultSet.getString(COLUMN_STATE).toUpperCase());

                booking = new Booking(id, roomNumber, userId, bookingDate, numberOfLiving, fromDate, toDate, paid, totalCost, state);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            close(stmt);
        }

        return booking;
    }

    @Override
    public boolean remove(long id) throws DAOException {
        boolean removed = false;
        PreparedStatement stmt = null;

        try {
            stmt = connection.prepareStatement(SQL_DELETE_BY_ID);
            stmt.setLong(FIRST_INDEX, id);
            stmt.executeUpdate();

            removed = true;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            this.close(stmt);
        }

        return removed;
    }

    @Override
    public boolean remove(Booking entity) throws DAOException {
        boolean result = false;
        PreparedStatement stmt = null;

        try {
            stmt = connection.prepareStatement(SQL_DELETE_BY_ENTITY);
            stmt.setLong(FIRST_INDEX, entity.getId());
            stmt.setLong(SECOND_INDEX, entity.getRoomNumber());
            stmt.setLong(THIRD_INDEX, entity.getUserId());
            stmt.setTimestamp(FOURTH_INDEX, Timestamp.valueOf(entity.getBookingDate()));
            stmt.executeUpdate();

            result = true;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            this.close(stmt);
        }

        return result;
    }

    @Override
    public boolean create(Booking entity) throws DAOException {
        PreparedStatement stmt = null;
        boolean result = false;

        try {
            stmt = connection.prepareStatement(SQL_INSERT_BOOKING);
            stmt.setLong(FIRST_INDEX, entity.getRoomNumber());
            stmt.setLong(SECOND_INDEX, entity.getUserId());
            stmt.setTimestamp(THIRD_INDEX, Timestamp.valueOf(entity.getBookingDate()));
            stmt.setInt(FOURTH_INDEX, entity.getNumberOfLiving());
            stmt.setDate(FIFTH_INDEX, Date.valueOf(entity.getFromDate()));
            stmt.setDate(SIXTH_INDEX, Date.valueOf(entity.getToDate()));
            stmt.setBoolean(SEVENTH_INDEX, entity.isPaid());
            stmt.setBigDecimal(EIGHTH_INDEX, entity.getTotalCost());
            stmt.setString(NINTH_INDEX, entity.getState().toString().toLowerCase());
            stmt.executeUpdate();

            result = true;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            this.close(stmt);
        }

        return result;
    }

    @Override
    public Booking update(Booking entity) throws DAOException {
        Booking updated = null;
        PreparedStatement stmt = null;

        try {
            stmt = connection.prepareStatement(SQL_UPDATE_STATE);
            stmt.setString(FIRST_INDEX, entity.getState().toString().toLowerCase());
            stmt.setLong(SECOND_INDEX, entity.getId());
            stmt.executeUpdate();

            updated = entity;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            this.close(stmt);
        }

        return updated;
    }

    public int findFreeSleeps(long roomNumber, LocalDate fromDate, LocalDate toDate) throws DAOException {
        int freeSleeps = 0;
        PreparedStatement stmt = null;

        try {
            stmt = connection.prepareStatement(SQL_SELECT_FREE_SLEEPS);
            stmt.setLong(FIRST_INDEX, roomNumber);
            stmt.setDate(SECOND_INDEX, Date.valueOf(toDate));
            stmt.setDate(THIRD_INDEX, Date.valueOf(fromDate));
            stmt.setLong(FOURTH_INDEX, roomNumber);

            ResultSet resultSet = stmt.executeQuery();

            if (resultSet.next()) {
                freeSleeps = resultSet.getInt(FIRST_INDEX);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            this.close(stmt);
        }

        return freeSleeps;
    }

    public List<Booking> findAllByUserId(long userId) throws DAOException {
        List<Booking> bookings = new ArrayList<>();
        PreparedStatement stmt = null;

        try {
            stmt = connection.prepareStatement(SQL_SELECT_ALL_BY_USER_ID);
            stmt.setLong(FIRST_INDEX, userId);
            ResultSet resultSet = stmt.executeQuery();

            while (resultSet.next()) {
                long id = resultSet.getLong(COLUMN_ID);
                long roomNumber = resultSet.getLong(COLUMN_ROOM_NUMBER);
                LocalDateTime bookingDate = resultSet.getTimestamp(COLUMN_BOOK_DATE).toLocalDateTime();
                int numberOfLiving = resultSet.getInt(COLUMN_NUMBER_OF_LIVING);
                LocalDate fromDate = resultSet.getDate(COLUMN_FROM_DATE).toLocalDate();
                LocalDate toDate = resultSet.getDate(COLUMN_TO_DATE).toLocalDate();
                boolean paid = resultSet.getBoolean(COLUMN_PAID);
                BigDecimal totalCost = resultSet.getBigDecimal(COLUMN_TOTAL_COST);
                BookingState state = BookingState.valueOf(resultSet.getString(COLUMN_STATE).toUpperCase());

                bookings.add(new Booking(id, roomNumber, userId, bookingDate, numberOfLiving, fromDate, toDate, paid, totalCost, state));
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            this.close(stmt);
        }

        return bookings;
    }

    public Map<Booking, String> findAllTodayBookingsByState(BookingState state) throws DAOException {
        Map<Booking, String> bookings = new LinkedHashMap<>();
        PreparedStatement stmt = null;

        try {
            stmt = connection.prepareStatement(SQL_SELECT_TODAY_BY_STATE);
            stmt.setString(FIRST_INDEX, state.toString().toLowerCase());
            ResultSet resultSet = stmt.executeQuery();

            while (resultSet.next()) {
                long bookingId = resultSet.getLong(COLUMN_ID);
                long roomNumber = resultSet.getLong(COLUMN_ROOM_NUMBER);
                long userId = resultSet.getLong(COLUMN_USER_ID);
                LocalDateTime bookingDate = resultSet.getTimestamp(COLUMN_BOOK_DATE).toLocalDateTime();
                int numberOfLiving = resultSet.getInt(COLUMN_NUMBER_OF_LIVING);
                LocalDate fromDate = resultSet.getDate(COLUMN_FROM_DATE).toLocalDate();
                LocalDate toDate = resultSet.getDate(COLUMN_TO_DATE).toLocalDate();
                boolean paid = resultSet.getBoolean(COLUMN_PAID);
                BigDecimal totalCost = resultSet.getBigDecimal(COLUMN_TOTAL_COST);
                String email = resultSet.getString(COLUMN_EMAIL);

                Booking booking = new Booking(bookingId, roomNumber, userId, bookingDate,
                        numberOfLiving, fromDate, toDate, paid, totalCost, state);
                bookings.put(booking, email);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            this.close(stmt);
        }

        return bookings;
    }

    public Map<Booking, String> findAllLimitBookings(int limit, int offset) throws DAOException {
        Map<Booking, String> bookings = new LinkedHashMap<>();
        PreparedStatement stmt = null;

        try {
            stmt = connection.prepareStatement(SQL_SELECT_ALL_LIMIT);
            stmt.setInt(FIRST_INDEX, limit);
            stmt.setInt(SECOND_INDEX, offset);
            ResultSet resultSet = stmt.executeQuery();

            while (resultSet.next()) {
                long bookingId = resultSet.getLong(COLUMN_ID);
                long roomNumber = resultSet.getLong(COLUMN_ROOM_NUMBER);
                long userId = resultSet.getLong(COLUMN_USER_ID);
                LocalDateTime bookingDate = resultSet.getTimestamp(COLUMN_BOOK_DATE).toLocalDateTime();
                int numberOfLiving = resultSet.getInt(COLUMN_NUMBER_OF_LIVING);
                LocalDate fromDate = resultSet.getDate(COLUMN_FROM_DATE).toLocalDate();
                LocalDate toDate = resultSet.getDate(COLUMN_TO_DATE).toLocalDate();
                boolean paid = resultSet.getBoolean(COLUMN_PAID);
                BigDecimal totalCost = resultSet.getBigDecimal(COLUMN_TOTAL_COST);
                BookingState state = BookingState.valueOf(resultSet.getString(COLUMN_STATE).toUpperCase());

                String email = resultSet.getString(COLUMN_EMAIL);
                Booking booking = new Booking(bookingId, roomNumber, userId, bookingDate,
                        numberOfLiving, fromDate, toDate, paid, totalCost, state);

                bookings.put(booking, email);
            }

        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            this.close(stmt);
        }

        return bookings;
    }

    public Map<Booking, String> findLimitBookingsByState(int limit, int offset, BookingState state) throws DAOException {
        Map<Booking, String> bookings = new LinkedHashMap<>();
        PreparedStatement stmt = null;

        try {
            stmt = connection.prepareStatement(SQL_SELECT_LIMIT_BY_STATE);
            stmt.setString(FIRST_INDEX, state.toString().toLowerCase());
            stmt.setInt(SECOND_INDEX, limit);
            stmt.setInt(THIRD_INDEX, offset);
            ResultSet resultSet = stmt.executeQuery();

            while (resultSet.next()) {
                String email = resultSet.getString(COLUMN_EMAIL);

                long bookingId = resultSet.getLong(COLUMN_ID);
                long roomNumber = resultSet.getLong(COLUMN_ROOM_NUMBER);
                long userId = resultSet.getLong(COLUMN_USER_ID);
                LocalDateTime bookingDate = resultSet.getTimestamp(COLUMN_BOOK_DATE).toLocalDateTime();
                int numberOfLiving = resultSet.getInt(COLUMN_NUMBER_OF_LIVING);
                LocalDate fromDate = resultSet.getDate(COLUMN_FROM_DATE).toLocalDate();
                LocalDate toDate = resultSet.getDate(COLUMN_TO_DATE).toLocalDate();
                boolean paid = resultSet.getBoolean(COLUMN_PAID);
                BigDecimal totalCost = resultSet.getBigDecimal(COLUMN_TOTAL_COST);

                Booking booking = new Booking(bookingId, roomNumber, userId, bookingDate,
                        numberOfLiving, fromDate, toDate, paid, totalCost, state);

                bookings.put(booking, email);
            }

        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            this.close(stmt);
        }

        return bookings;
    }

    public int findCountOfAllBookings() throws DAOException {
        int count = 0;
        Statement stmt = null;

        try {
            stmt = connection.createStatement();
            ResultSet resultSet = stmt.executeQuery(SQL_SELECT_COUNT_BOOKINGS);

            if (resultSet.next()) {
                count = resultSet.getInt(FIRST_INDEX);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            this.close(stmt);
        }

        return count;
    }

    public int findCountBookingsByState(BookingState state) throws DAOException {
        int count = 0;
        PreparedStatement stmt = null;

        try {
            stmt = connection.prepareStatement(SQL_SELECT_COUNT_BY_STATE);
            stmt.setString(FIRST_INDEX, state.toString().toLowerCase());
            ResultSet resultSet = stmt.executeQuery();

            if (resultSet.next()) {
                count = resultSet.getInt(FIRST_INDEX);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            this.close(stmt);
        }

        return count;
    }
}
