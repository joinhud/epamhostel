package com.epam.hostel.dao;

import com.epam.hostel.entity.User;
import com.epam.hostel.exception.DAOException;

import java.sql.*;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class UserDAO extends AbstractDAO<User> {
    //column names
    private static final String COLUMN_ID = "id";
    private static final String COLUMN_EMAIL = "email";
    private static final String COLUMN_PASSWORD = "password";
    private static final String COLUMN_FIRST_NAME = "f_name";
    private static final String COLUMN_LAST_NAME = "l_name";
    private static final String COLUMN_DISCOUNT = "discount";
    //parameters indexes
    private static final int FIRST_INDEX = 1;
    private static final int SECOND_INDEX = 2;
    private static final int THIRD_INDEX = 3;
    private static final int FOURTH_INDEX = 4;
    private static final int FIFTH_INDEX = 5;
    //SQL scripts
    private static final String SQL_SELECT_BY_EMAIL = "SELECT id, email, password, f_name, " +
            "l_name, discount FROM user WHERE email = ?";
    private static final String SQL_INSERT_USER = "INSERT INTO user (`id`, `email`, `password`, " +
            "`f_name`, `l_name`, `discount`) VALUES (NULL, ?, ?, ?, ?, ?)";
    private static final String SQL_UPDATE_USER = "UPDATE `user` SET `password` = ?, `f_name` = ?, " +
            "`l_name` = ?, `discount` = ? WHERE `id` = ?";
    private static final String SQL_SELECT_BY_ID = "SELECT `id`, `email`, `password`, `f_name`, `l_name`, " +
            "`discount` FROM `user` WHERE id = ?";
    private static final String SQL_SELECT_COUNT_USERS = "SELECT COUNT(`id`) FROM `user`";
    private static final String SQL_SELECT_COUNT_BANED_USERS = "SELECT COUNT(`id`) " +
            "FROM `user` INNER JOIN `ban_list` ON `id` = `users_id` " +
            "WHERE NOW() < `to_date`";
    private static final String SQL_SELECT_ALL_LIMIT_USERS = "SELECT `id`, `email`, `password`, `f_name`, `l_name`, " +
            "`discount` FROM `user` WHERE `id` <> 1 LIMIT ? OFFSET ?";
    private static final String SQL_SELECT_BANED_LIMIT_USERS = "SELECT `id`, `email`, `password`, `f_name`, `l_name`, " +
            "`discount` " +
            "FROM `user` INNER JOIN `ban_list` ON `id` = `users_id` " +
            "WHERE NOW() < `to_date` " +
            "LIMIT ? OFFSET ?";
    private static final String SQL_SELECT_ALL = "SELECT `id`, `email`, `password`, " +
            "`f_name`, `l_name`, `discount` FROM `user`";
    private static final String SQL_DELETE_BY_ID = "DELETE FROM `user` WHERE `id` = ?";
    private static final String SQL_DELETE_BY_ENTITY = "DELETE FROM `user` WHERE `id` = ? " +
            "AND `email` = ? AND `password` = ?";

    public UserDAO(Connection connection) {
        super(connection);
    }

    @Override
    public List<User> findAll() throws DAOException {
        List<User> users = new ArrayList<>();
        Statement stmt = null;

        try {
            stmt = connection.createStatement();
            ResultSet resultSet = stmt.executeQuery(SQL_SELECT_ALL);

            while (resultSet.next()) {
                long id = resultSet.getLong(COLUMN_ID);
                String email = resultSet.getString(COLUMN_EMAIL);
                String password = resultSet.getString(COLUMN_PASSWORD);
                String firstName = resultSet.getString(COLUMN_FIRST_NAME);
                String lastName = resultSet.getString(COLUMN_LAST_NAME);
                float discount = resultSet.getFloat(COLUMN_DISCOUNT);

                users.add(new User(id, email, password, firstName, lastName, discount));
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            this.close(stmt);
        }

        return users;
    }

    @Override
    public User findEntityById(long id) throws DAOException {
        User user = null;
        PreparedStatement stmt = null;

        try {
            stmt = connection.prepareStatement(SQL_SELECT_BY_ID);
            stmt.setLong(FIRST_INDEX, id);
            ResultSet resultSet = stmt.executeQuery();

            if (resultSet.next()) {
                String email = resultSet.getString(COLUMN_EMAIL);
                String password = resultSet.getString(COLUMN_PASSWORD);
                String firstName = resultSet.getString(COLUMN_FIRST_NAME);
                String lastName = resultSet.getString(COLUMN_LAST_NAME);
                float discount = resultSet.getFloat(COLUMN_DISCOUNT);

                user = new User(id, email, password, firstName, lastName, discount);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            this.close(stmt);
        }

        return user;
    }

    @Override
    public boolean remove(long id) throws DAOException {
        boolean result = false;
        PreparedStatement stmt = null;

        try {
            stmt = connection.prepareStatement(SQL_DELETE_BY_ID);
            stmt.setLong(FIRST_INDEX, id);
            stmt.executeUpdate();

            result = true;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            this.close(stmt);
        }

        return result;
    }

    @Override
    public boolean remove(User entity) throws DAOException {
        boolean result = false;
        PreparedStatement stmt = null;

        try {
            stmt = connection.prepareStatement(SQL_DELETE_BY_ENTITY);
            stmt.setLong(FIRST_INDEX, entity.getId());
            stmt.setString(SECOND_INDEX, entity.getEmail());
            stmt.setString(THIRD_INDEX, entity.getPassword());
            stmt.executeUpdate();

            result = true;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            this.close(stmt);
        }

        return result;
    }

    @Override
    public boolean create(User entity) throws DAOException {
        PreparedStatement stmt = null;
        boolean result = false;

        try {
            stmt = connection.prepareStatement(SQL_INSERT_USER);
            stmt.setString(FIRST_INDEX, entity.getEmail());
            stmt.setString(SECOND_INDEX, entity.getPassword());
            stmt.setString(THIRD_INDEX, entity.getFirstName());
            stmt.setString(FOURTH_INDEX, entity.getLastName());
            stmt.setFloat(FIFTH_INDEX, entity.getDiscount());
            stmt.executeUpdate();

            result = true;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            this.close(stmt);
        }

        return result;
    }

    @Override
    public User update(User entity) throws DAOException {
        User user = null;
        PreparedStatement stmt = null;

        try {
            stmt = connection.prepareStatement(SQL_UPDATE_USER);
            stmt.setString(FIRST_INDEX, entity.getPassword());
            stmt.setString(SECOND_INDEX, entity.getFirstName());
            stmt.setString(THIRD_INDEX, entity.getLastName());
            stmt.setFloat(FOURTH_INDEX, entity.getDiscount());
            stmt.setLong(FIFTH_INDEX, entity.getId());
            stmt.executeUpdate();

            user = entity;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            this.close(stmt);
        }

        return user;
    }

    public User findEntityByEmail(String email) throws DAOException {
        User user = null;
        PreparedStatement stmt = null;

        try {
            stmt = connection.prepareStatement(SQL_SELECT_BY_EMAIL);
            stmt.setString(FIRST_INDEX, email);
            ResultSet resultSet = stmt.executeQuery();

            if(resultSet.next()) {
                long id = resultSet.getLong(COLUMN_ID);
                String password = resultSet.getString(COLUMN_PASSWORD);
                String firstName = resultSet.getString(COLUMN_FIRST_NAME);
                String lastName = resultSet.getString(COLUMN_LAST_NAME);
                float discount = resultSet.getFloat(COLUMN_DISCOUNT);

                user = new User(id, email, password, firstName, lastName, discount);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            this.close(stmt);
        }

        return user;
    }

    public int findCountOfAllUsers() throws DAOException {
        int count = 0;
        Statement stmt = null;

        try {
            stmt = connection.createStatement();
            ResultSet resultSet = stmt.executeQuery(SQL_SELECT_COUNT_USERS);

            if (resultSet.next()) {
                count = resultSet.getInt(FIRST_INDEX);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            this.close(stmt);
        }

        return count;
    }

    public int findCountOfBanedUsers() throws DAOException {
        int count = 0;
        Statement stmt = null;

        try {
            stmt = connection.createStatement();
            ResultSet resultSet = stmt.executeQuery(SQL_SELECT_COUNT_BANED_USERS);

            if (resultSet.next()) {
                count = resultSet.getInt(FIRST_INDEX);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            this.close(stmt);
        }

        return count;
    }

    public List<User> findAllLimitUsers(int limit, int offset) throws DAOException {
        List<User> users = new LinkedList<>();
        PreparedStatement stmt = null;

        try {
            stmt = connection.prepareStatement(SQL_SELECT_ALL_LIMIT_USERS);
            stmt.setInt(FIRST_INDEX, limit);
            stmt.setInt(SECOND_INDEX, offset);
            ResultSet resultSet = stmt.executeQuery();

            while (resultSet.next()) {
                long id = resultSet.getLong(COLUMN_ID);
                String email = resultSet.getString(COLUMN_EMAIL);
                String password = resultSet.getString(COLUMN_PASSWORD);
                String firstName = resultSet.getString(COLUMN_FIRST_NAME);
                String lastName = resultSet.getString(COLUMN_LAST_NAME);
                float discount = resultSet.getFloat(COLUMN_DISCOUNT);

                users.add(new User(id, email, password, firstName, lastName, discount));
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            this.close(stmt);
        }

        return users;
    }

    public List<User> findBanedLimitUsers(int limit, int offset) throws DAOException {
        List<User> users = new LinkedList<>();
        PreparedStatement stmt = null;

        try {
            stmt = connection.prepareStatement(SQL_SELECT_BANED_LIMIT_USERS);
            stmt.setInt(FIRST_INDEX, limit);
            stmt.setInt(SECOND_INDEX, offset);
            ResultSet resultSet = stmt.executeQuery();

            while (resultSet.next()) {
                long id = resultSet.getLong(COLUMN_ID);
                String email = resultSet.getString(COLUMN_EMAIL);
                String password = resultSet.getString(COLUMN_PASSWORD);
                String firstName = resultSet.getString(COLUMN_FIRST_NAME);
                String lastName = resultSet.getString(COLUMN_LAST_NAME);
                float discount = resultSet.getFloat(COLUMN_DISCOUNT);

                User user = new User(id, email, password, firstName, lastName, discount);

                users.add(user);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            this.close(stmt);
        }

        return users;
    }
}
