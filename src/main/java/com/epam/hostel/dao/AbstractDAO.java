package com.epam.hostel.dao;

import com.epam.hostel.entity.Entity;
import com.epam.hostel.exception.DAOException;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

public abstract class AbstractDAO <T extends Entity> {
    protected Connection connection;

    public AbstractDAO(Connection connection) {
        this.connection = connection;
    }

    public abstract List<T> findAll() throws DAOException;
    public abstract T findEntityById(long id) throws DAOException;
    public abstract boolean remove(long id) throws DAOException;
    public abstract boolean remove(T entity) throws DAOException;
    public abstract boolean create(T entity) throws DAOException;
    public abstract T update(T entity) throws DAOException;

    public void close(Statement statement) throws DAOException {
        try {
            if (statement != null) {
                statement.close();
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }
}
