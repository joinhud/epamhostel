package com.epam.hostel.dao;

import com.epam.hostel.entity.Ban;
import com.epam.hostel.exception.DAOException;

import java.sql.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class BanDAO extends AbstractDAO<Ban> {
    private static final String COLUMN_ID = "ban_id";
    private static final String COLUMN_USER_ID = "users_id";
    private static final String COLUMN_FROM_DATE = "from_date";
    private static final String COLUMN_TO_DATE = "to_date";

    private static final int FIRST_INDEX = 1;
    private static final int SECOND_INDEX = 2;
    private static final int THIRD_INDEX = 3;
    private static final int FOURTH_INDEX = 4;

    private static final String SQL_SELECT_BY_USER_ID = "SELECT `ban_id`, `users_id`, `from_date`, " +
            "`to_date` FROM `ban_list` WHERE users_id = ?";
    private static final String SQL_SELECT_LAST_BAN_BY_USER_ID = "SELECT `ban_id`, `users_id`, `from_date`, `to_date` " +
            "FROM `ban_list` WHERE `users_id` = ? ORDER BY `ban_id` DESC LIMIT 1";
    private static final String SQL_INSERT_BAN = "INSERT INTO `ban_list` (`ban_id`, `users_id`, `from_date`, `to_date`) " +
            "VALUES (NULL, ?, ?, ?)";
    private static final String SQL_SELECT_ALL = "SELECT `ban_id`, `users_id`, `from_date`, `to_date` FROM `ban_list`";
    private static final String SQL_SELECT_BY_ID = "SELECT `ban_id`, `users_id`, `from_date`, `to_date` FROM `ban_list` WHERE `ban_id` = ?";
    private static final String SQL_DELETE_BY_ID = "DELETE FROM `ban_list` WHERE `ban_id` = ?";
    private static final String SQL_DELETE_BY_ENTITY = "DELETE FROM `ban_list` WHERE `ban_id` = ? AND `users_id` = ? " +
            "AND `from_date` = ? AND `to_date` = ?";
    private static final String SQL_UPDATE_BAN = "UPDATE `ban_list` SET `users_id` = ?, `from_date` = ?, `to_date` = ?";

    public BanDAO(Connection connection) {
        super(connection);
    }

    @Override
    public List<Ban> findAll() throws DAOException {
        List<Ban> bans = new ArrayList<>();
        Statement stmt = null;

        try {
            stmt = connection.createStatement();
            ResultSet resultSet = stmt.executeQuery(SQL_SELECT_ALL);

            while (resultSet.next()) {
                long id = resultSet.getLong(COLUMN_ID);
                long userId = resultSet.getLong(COLUMN_USER_ID);
                LocalDateTime fromDate = resultSet.getTimestamp(COLUMN_FROM_DATE).toLocalDateTime();
                LocalDateTime toDate = resultSet.getTimestamp(COLUMN_TO_DATE).toLocalDateTime();

                bans.add(new Ban(id, userId, fromDate, toDate));
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            this.close(stmt);
        }

        return bans;
    }

    @Override
    public Ban findEntityById(long id) throws DAOException {
        Ban ban = null;
        PreparedStatement stmt = null;

        try {
            stmt = connection.prepareStatement(SQL_SELECT_BY_ID);
            stmt.setLong(FIRST_INDEX, id);
            ResultSet resultSet = stmt.executeQuery();

            if(resultSet.next()) {
                long userId = resultSet.getLong(COLUMN_USER_ID);
                LocalDateTime fromDate = resultSet.getTimestamp(COLUMN_FROM_DATE).toLocalDateTime();
                LocalDateTime toDate = resultSet.getTimestamp(COLUMN_TO_DATE).toLocalDateTime();

                ban = new Ban(id, userId, fromDate, toDate);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            this.close(stmt);
        }

        return ban;
    }

    @Override
    public boolean remove(long id) throws DAOException {
        boolean result = false;
        PreparedStatement stmt = null;

        try {
            stmt = connection.prepareStatement(SQL_DELETE_BY_ID);
            stmt.setLong(FIRST_INDEX, id);
            stmt.executeUpdate();

            result = true;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            this.close(stmt);
        }

        return result;
    }

    @Override
    public boolean remove(Ban entity) throws DAOException {
        boolean result = false;
        PreparedStatement stmt = null;

        try {
            stmt = connection.prepareStatement(SQL_DELETE_BY_ENTITY);
            stmt.setLong(FIRST_INDEX, entity.getId());
            stmt.setLong(SECOND_INDEX, entity.getUserId());
            stmt.setTimestamp(THIRD_INDEX, Timestamp.valueOf(entity.getFromDate()));
            stmt.setTimestamp(FOURTH_INDEX, Timestamp.valueOf(entity.getToDate()));

            result = true;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            this.close(stmt);
        }

        return result;
    }

    @Override
    public boolean create(Ban entity) throws DAOException {
        boolean created = false;
        PreparedStatement stmt = null;

        try {
            stmt = connection.prepareStatement(SQL_INSERT_BAN);
            stmt.setLong(FIRST_INDEX, entity.getUserId());
            stmt.setTimestamp(SECOND_INDEX, Timestamp.valueOf(entity.getFromDate()));
            stmt.setTimestamp(THIRD_INDEX, Timestamp.valueOf(entity.getToDate()));
            stmt.executeUpdate();

            created = true;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            this.close(stmt);
        }

        return created;
    }

    @Override
    public Ban update(Ban entity) throws DAOException {
        Ban updated = null;
        PreparedStatement stmt = null;

        try {
            stmt = connection.prepareStatement(SQL_UPDATE_BAN);
            stmt.setTimestamp(SECOND_INDEX, Timestamp.valueOf(entity.getFromDate()));
            stmt.setTimestamp(THIRD_INDEX, Timestamp.valueOf(entity.getToDate()));
            stmt.executeUpdate();

            updated = entity;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            this.close(stmt);
        }

        return updated;
    }

    public List<Ban> findBansByUserId(long userId) throws DAOException {
        List<Ban> bans = new ArrayList<>();
        PreparedStatement stmt = null;

        try {
            stmt = connection.prepareStatement(SQL_SELECT_BY_USER_ID);
            stmt.setLong(FIRST_INDEX, userId);
            ResultSet resultSet = stmt.executeQuery();

            while (resultSet.next()) {
                long id = resultSet.getLong(COLUMN_ID);
                LocalDateTime fromDate = resultSet.getTimestamp(COLUMN_FROM_DATE).toLocalDateTime();
                LocalDateTime toDate = resultSet.getTimestamp(COLUMN_TO_DATE).toLocalDateTime();

                bans.add(new Ban(id, userId, fromDate, toDate));
            }

        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            this.close(stmt);
        }

        return bans;
    }

    public Ban findLastBanByUserId(long id) throws DAOException {
        Ban lastBan = null;
        PreparedStatement stmt = null;

        try {
            stmt = connection.prepareStatement(SQL_SELECT_LAST_BAN_BY_USER_ID);
            stmt.setLong(FIRST_INDEX, id);
            ResultSet resultSet = stmt.executeQuery();

            if(resultSet.next()) {
                long banId = resultSet.getLong(COLUMN_ID);
                LocalDateTime fromDate = resultSet.getTimestamp(COLUMN_FROM_DATE).toLocalDateTime();
                LocalDateTime toDate = resultSet.getTimestamp(COLUMN_TO_DATE).toLocalDateTime();

                lastBan = new Ban(banId, id, fromDate, toDate);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            this.close(stmt);
        }

        return lastBan;
    }
}
