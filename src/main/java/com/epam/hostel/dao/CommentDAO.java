package com.epam.hostel.dao;

import com.epam.hostel.entity.Comment;
import com.epam.hostel.exception.DAOException;

import java.sql.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class CommentDAO extends AbstractDAO<Comment> {
    private static final int FIRST_INDEX = 1;
    private static final int SECOND_INDEX = 2;
    private static final int THIRD_INDEX = 3;
    private static final int FOURTH_INDEX = 4;
    private static final int FIFTH_INDEX = 5;

    private static final String COLUMN_ID = "comment_id";
    private static final String COLUMN_USER_ID = "users_id";
    private static final String COLUMN_COMMENT = "comment";
    private static final String COLUMN_DATE = "date";
    private static final String COLUMN_DELETED = "is_deleted";
    private static final String COLUMN_EMAIL = "email";

    private static final String SQL_SELECT_ALL = "SELECT `comment_id`, `users_id`, `comment`, `date`, " +
            "`is_deleted` FROM `comment`";
    private static final String SQL_DELETE_BY_ENTITY = "DELETE FROM `comment` WHERE `comment_id` = ? " +
            "AND `users_id` = ? AND `comment` = ? AND `date` = ?";
    private static final String SQL_SELECT_ALL_NOT_DELETED = "SELECT `comment_id`, `users_id`, `comment`, `date`, " +
            "`is_deleted` FROM `comment` WHERE is_deleted = FALSE ORDER BY `date` DESC";
    private static final String SQL_INSERT_COMMENT = "INSERT INTO `comment` (`comment_id`, `users_id`, " +
            "`comment`, `date`, `is_deleted`) VALUES (NULL, ?, ?, ?, ?)";
    private static final String SQL_DELETE_BY_ID = "DELETE FROM `comment` WHERE comment_id = ?";
    private static final String SQL_SELECT_ALL_BY_USER_ID = "SELECT `comment_id`, `users_id`, `comment`, `date`, " +
            "`is_deleted` FROM `comment` WHERE users_id = ? ORDER BY `date` DESC";
    private static final String SQL_UPDATE_COMMENT = "UPDATE `comment` SET `users_id` = ?, `date` = ?, " +
            "`is_deleted` = ? WHERE `comment_id` = ?";
    private static final String SQL_SELECT_BY_ID = "SELECT `comment_id`, `users_id`, `comment`, `date`, " +
            "`is_deleted` FROM `comment` WHERE comment_id = ?";
    private static final String SQL_SELECT_COUNT_COMMENTS = "SELECT COUNT(`comment_id`) FROM `comment`";
    private static final String SQL_SELECT_COUNT_COMMENTS_BY_DELETED = "SELECT COUNT(`comment_id`) " +
            "FROM `comment` WHERE `is_deleted` = ?";
    private static final String SQL_SELECT_COUNT_TODAY_COMMENTS = "SELECT COUNT(`comment_id`) FROM `comment` " +
            "WHERE `is_deleted` = FALSE " +
            "AND YEAR(`date`) = YEAR(NOW()) " +
            "AND MONTH(`date`) = MONTH(NOW()) " +
            "AND DAY(`date`) = DAY(NOW())";
    private static final String SQL_SELECT_ALL_LIMIT_COMMENTS = "SELECT `comment_id`, `users_id`, `comment`, `date`, " +
            "`is_deleted`, `email` FROM `comment` INNER JOIN `user` " +
            "ON `comment_id` = `id` " +
            "ORDER BY `date` DESC " +
            "LIMIT ? OFFSET ?";
    private static final String SQL_SELECT_LIMIT_COMMENTS_BY_DELETED = "SELECT `comment_id`, `users_id`, `comment`, " +
            "`date`, `is_deleted`, `email` " +
            "FROM `comment` INNER JOIN `user` ON `comment_id` = `id` " +
            "WHERE `is_deleted` = ? " +
            "ORDER BY `date` DESC " +
            "LIMIT ? OFFSET ?";
    private static final String SQL_SELECT_LIMIT_TODAY_COMMENTS = "SELECT `comment_id`, `users_id`, `comment`, `date`, " +
            "`is_deleted`, `email` " +
            "FROM `comment` INNER JOIN `user` ON `comment_id` = `id` " +
            "WHERE `is_deleted` = FALSE " +
            "AND YEAR(`date`) = YEAR(NOW()) " +
            "AND MONTH(`date`) = MONTH(NOW()) " +
            "AND DAY(`date`) = DAY(NOW()) " +
            "ORDER BY `date` DESC " +
            "LIMIT ? OFFSET ?";

    public CommentDAO(Connection connection) {
        super(connection);
    }

    @Override
    public List<Comment> findAll() throws DAOException {
        List<Comment> comments = new ArrayList<>();
        Statement stmt = null;

        try {
            stmt = connection.createStatement();
            ResultSet resultSet = stmt.executeQuery(SQL_SELECT_ALL);

            while (resultSet.next()) {
                long id = resultSet.getLong(COLUMN_ID);
                long userId = resultSet.getLong(COLUMN_USER_ID);
                String commentText = resultSet.getString(COLUMN_COMMENT);
                LocalDateTime date = resultSet.getTimestamp(COLUMN_DATE).toLocalDateTime();
                boolean deleted = resultSet.getBoolean(COLUMN_DELETED);

                comments.add(new Comment(id, userId, commentText, date, deleted));
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            this.close(stmt);
        }

        return comments;
    }

    @Override
    public Comment findEntityById(long id) throws DAOException {
        Comment comment = null;
        PreparedStatement stmt = null;

        try {
            stmt = connection.prepareStatement(SQL_SELECT_BY_ID);
            stmt.setLong(FIRST_INDEX, id);
            ResultSet resultSet = stmt.executeQuery();

            if (resultSet.next()) {
                long userId = resultSet.getLong(COLUMN_USER_ID);
                String commentText = resultSet.getString(COLUMN_COMMENT);
                LocalDateTime date = resultSet.getTimestamp(COLUMN_DATE).toLocalDateTime();
                boolean deleted = resultSet.getBoolean(COLUMN_DELETED);

                comment = new Comment(id, userId, commentText, date, deleted);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            this.close(stmt);
        }

        return comment;
    }

    @Override
    public boolean remove(long id) throws DAOException {
        boolean removed = false;
        PreparedStatement stmt = null;

        try {
            stmt = connection.prepareStatement(SQL_DELETE_BY_ID);
            stmt.setLong(FIRST_INDEX, id);
            stmt.executeUpdate();

            removed = true;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            this.close(stmt);
        }

        return removed;
    }

    @Override
    public boolean remove(Comment entity) throws DAOException {
        boolean result = false;
        PreparedStatement stmt = null;

        try {
            stmt = connection.prepareStatement(SQL_DELETE_BY_ENTITY);
            stmt.setLong(FIRST_INDEX, entity.getId());
            stmt.setLong(SECOND_INDEX, entity.getUserId());
            stmt.setString(THIRD_INDEX, entity.getComment());
            stmt.setTimestamp(FOURTH_INDEX, Timestamp.valueOf(entity.getDate()));
            stmt.executeUpdate();

            result = true;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            this.close(stmt);
        }

        return true;
    }

    @Override
    public boolean create(Comment entity) throws DAOException {
        boolean created = false;
        PreparedStatement stmt = null;

        try {
            stmt = connection.prepareStatement(SQL_INSERT_COMMENT);
            stmt.setLong(FIRST_INDEX, entity.getUserId());
            stmt.setString(SECOND_INDEX, entity.getComment());
            stmt.setTimestamp(THIRD_INDEX, Timestamp.valueOf(entity.getDate()));
            stmt.setBoolean(FOURTH_INDEX, entity.isDeleted());
            stmt.executeUpdate();

            created = true;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            this.close(stmt);
        }

        return created;
    }

    @Override
    public Comment update(Comment entity) throws DAOException {
        Comment updated = null;
        PreparedStatement stmt = null;

        try {
            stmt = connection.prepareStatement(SQL_UPDATE_COMMENT);
            stmt.setLong(FIRST_INDEX, entity.getUserId());
            stmt.setTimestamp(SECOND_INDEX, Timestamp.valueOf(entity.getDate()));
            stmt.setBoolean(THIRD_INDEX, entity.isDeleted());
            stmt.setLong(FOURTH_INDEX, entity.getId());
            stmt.executeUpdate();

            updated = entity;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            this.close(stmt);
        }

        return updated;
    }

    public List<Comment> findAllNotDeleted() throws DAOException {
        List<Comment> comments = new ArrayList<>();
        Statement stmt = null;

        try {
            stmt = connection.createStatement();
            ResultSet resultSet = stmt.executeQuery(SQL_SELECT_ALL_NOT_DELETED);

            while (resultSet.next()) {
                long id = resultSet.getLong(COLUMN_ID);
                long userId = resultSet.getLong(COLUMN_USER_ID);
                String comment = resultSet.getString(COLUMN_COMMENT);
                LocalDateTime date = resultSet.getTimestamp(COLUMN_DATE).toLocalDateTime();
                boolean deleted = resultSet.getBoolean(COLUMN_DELETED);

                comments.add(new Comment(id, userId, comment, date, deleted));
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            this.close(stmt);
        }

        return comments;
    }

    public List<Comment> findCommentsByUserId(long userId) throws DAOException {
        List<Comment> comments = new ArrayList<>();
        PreparedStatement stmt = null;

        try {
            stmt = connection.prepareStatement(SQL_SELECT_ALL_BY_USER_ID);
            stmt.setLong(FIRST_INDEX, userId);
            ResultSet resultSet = stmt.executeQuery();

            while (resultSet.next()) {
                long id = resultSet.getLong(COLUMN_ID);
                String comment = resultSet.getString(COLUMN_COMMENT);
                LocalDateTime date = resultSet.getTimestamp(COLUMN_DATE).toLocalDateTime();
                boolean deleted = resultSet.getBoolean(COLUMN_DELETED);

                comments.add(new Comment(id, userId, comment, date, deleted));
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            this.close(stmt);
        }

        return comments;
    }

    public int findCountOfAllComments() throws DAOException {
        int count = 0;
        Statement stmt = null;

        try {
            stmt = connection.createStatement();
            ResultSet resultSet = stmt.executeQuery(SQL_SELECT_COUNT_COMMENTS);

            if (resultSet.next()) {
                count = resultSet.getInt(FIRST_INDEX);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            this.close(stmt);
        }

        return count;
    }

    public int findCountCommentsByDeleted(boolean deleted) throws DAOException {
        int count = 0;
        PreparedStatement stmt = null;

        try {
            stmt = connection.prepareStatement(SQL_SELECT_COUNT_COMMENTS_BY_DELETED);
            stmt.setBoolean(FIRST_INDEX, deleted);
            ResultSet resultSet = stmt.executeQuery();

            if (resultSet.next()) {
                count = resultSet.getInt(FIRST_INDEX);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            this.close(stmt);
        }

        return count;
    }

    public int findCountTodayComments() throws DAOException {
        int count = 0;
        Statement stmt = null;

        try {
            stmt = connection.createStatement();
            ResultSet resultSet = stmt.executeQuery(SQL_SELECT_COUNT_TODAY_COMMENTS);

            if (resultSet.next()) {
                count = resultSet.getInt(FIRST_INDEX);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            this.close(stmt);
        }

        return count;
    }

    public Map<Comment, String> findAllLimitComments(int limit, int offset) throws DAOException {
        Map<Comment, String> comments = new LinkedHashMap<>();
        PreparedStatement stmt = null;

        try {
            stmt = connection.prepareStatement(SQL_SELECT_ALL_LIMIT_COMMENTS);
            stmt.setInt(FIRST_INDEX, limit);
            stmt.setInt(SECOND_INDEX, offset);
            ResultSet resultSet = stmt.executeQuery();

            while (resultSet.next()) {
                long commentId = resultSet.getLong(COLUMN_ID);
                long userId = resultSet.getLong(COLUMN_USER_ID);
                String commentText = resultSet.getString(COLUMN_COMMENT);
                LocalDateTime date = resultSet.getTimestamp(COLUMN_DATE).toLocalDateTime();
                boolean deleted = resultSet.getBoolean(COLUMN_DELETED);
                String email = resultSet.getString(COLUMN_EMAIL);

                Comment comment = new Comment(commentId, userId, commentText, date, deleted);

                comments.put(comment, email);
            }

        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            this.close(stmt);
        }

        return comments;
    }

    public Map<Comment, String> findAllLimitCommentsByDeletedValue(int limit, int offset, boolean deleted)
            throws DAOException {
        Map<Comment, String> comments = new LinkedHashMap<>();
        PreparedStatement stmt = null;

        try {
            stmt = connection.prepareStatement(SQL_SELECT_LIMIT_COMMENTS_BY_DELETED);
            stmt.setBoolean(FIRST_INDEX, deleted);
            stmt.setInt(SECOND_INDEX, limit);
            stmt.setInt(THIRD_INDEX, offset);
            ResultSet resultSet = stmt.executeQuery();

            while (resultSet.next()) {
                long commentId = resultSet.getLong(COLUMN_ID);
                long userId = resultSet.getLong(COLUMN_USER_ID);
                String commentText = resultSet.getString(COLUMN_COMMENT);
                LocalDateTime date = resultSet.getTimestamp(COLUMN_DATE).toLocalDateTime();
                String email = resultSet.getString(COLUMN_EMAIL);

                Comment comment = new Comment(commentId, userId, commentText, date, deleted);

                comments.put(comment, email);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            this.close(stmt);
        }

        return comments;
    }

    public Map<Comment, String> findLimitTodayComments(int limit, int offset) throws DAOException {
        Map<Comment, String> comments = new LinkedHashMap<>();
        PreparedStatement stmt = null;

        try {
            stmt = connection.prepareStatement(SQL_SELECT_LIMIT_TODAY_COMMENTS);
            stmt.setInt(FIRST_INDEX, limit);
            stmt.setInt(SECOND_INDEX, offset);
            ResultSet resultSet = stmt.executeQuery();

            while (resultSet.next()) {
                String email = resultSet.getString(COLUMN_EMAIL);
                long commentId = resultSet.getLong(COLUMN_ID);
                long userId = resultSet.getLong(COLUMN_USER_ID);
                String commentText = resultSet.getString(COLUMN_COMMENT);
                LocalDateTime date = resultSet.getTimestamp(COLUMN_DATE).toLocalDateTime();
                boolean deleted = resultSet.getBoolean(COLUMN_DELETED);

                Comment comment = new Comment(commentId, userId, commentText, date, deleted);

                comments.put(comment, email);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            this.close(stmt);
        }

        return comments;
    }
}
