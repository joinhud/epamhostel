package com.epam.hostel.dao;

import com.epam.hostel.entity.BankAccount;
import com.epam.hostel.exception.DAOException;

import java.math.BigDecimal;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class BankAccountDAO extends AbstractDAO<BankAccount> {

    private static final int FIRST_INDEX = 1;
    private static final int SECOND_INDEX = 2;
    private static final int THIRD_INDEX = 3;
    private static final int FOURTH_INDEX = 4;

    private static final String COLUMN_ID = "bank_account";
    private static final String COLUMN_USER_ID = "users_id";
    private static final String COLUMN_MONEY = "money";
    private static final String COLUMN_PASSWORD = "account_password";

    private static final String SQL_SELECT_BY_USER_ID = "SELECT bank_account, users_id, " +
            "money, account_password FROM bank WHERE users_id = ?";
    private static final String SQL_SELECT_BY_ID = "SELECT bank_account, users_id, money, " +
            "account_password FROM bank WHERE bank_account = ?";
    private static final String SQL_INSERT_ACCOUNT = "INSERT INTO bank (`bank_account`, `users_id`, " +
            "`money`, `account_password`) VALUES (?, ?, ?, ?)";
    private static final String SQL_UPDATE_ACCOUNT = "UPDATE `bank` SET `users_id` = ?, " +
            "`money` = ?, `account_password` = ? WHERE `bank_account` = ?";
    private static final String SQL_DELETE_BY_ID = "DELETE FROM `bank` WHERE bank_account = ?";
    private static final String SQL_DELETE_BY_ENTITY = "DELETE FROM `bank` WHERE `bank_account` = ? AND `users_id` = ? " +
            "AND `money` = ? AND `account_password` = ?";
    private static final String SQL_SELECT_ALL = "SELECT `bank_account`, `users_id`, `money`, `account_password` " +
            "FROM `bank`";


    public BankAccountDAO(Connection connection) {
        super(connection);
    }

    @Override
    public List<BankAccount> findAll() throws DAOException {
        List<BankAccount> accounts = new ArrayList<>();
        Statement stmt = null;

        try {
            stmt = connection.createStatement();
            ResultSet resultSet = stmt.executeQuery(SQL_SELECT_ALL);

            while (resultSet.next()) {
                long id = resultSet.getLong(COLUMN_ID);
                long userId = resultSet.getLong(COLUMN_USER_ID);
                BigDecimal money = resultSet.getBigDecimal(COLUMN_MONEY);
                String password = resultSet.getString(COLUMN_PASSWORD);

                accounts.add(new BankAccount(id, userId, money, password));
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            this.close(stmt);
        }

        return accounts;
    }

    @Override
    public BankAccount findEntityById(long id) throws DAOException {
        BankAccount bankAccount = null;
        PreparedStatement stmt = null;

        try {
            stmt = connection.prepareStatement(SQL_SELECT_BY_ID);
            stmt.setLong(FIRST_INDEX, id);
            ResultSet resultSet = stmt.executeQuery();

            if (resultSet.next()) {
                long userId = resultSet.getLong(COLUMN_USER_ID);
                BigDecimal money = resultSet.getBigDecimal(COLUMN_MONEY);
                String password = resultSet.getString(COLUMN_PASSWORD);

                bankAccount = new BankAccount(id, userId, money, password);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            this.close(stmt);
        }

        return bankAccount;
    }

    @Override
    public boolean remove(long id) throws DAOException {
        boolean result = false;
        PreparedStatement stmt = null;

        try {
            stmt = connection.prepareStatement(SQL_DELETE_BY_ID);
            stmt.setLong(FIRST_INDEX, id);
            stmt.executeUpdate();

            result = true;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            this.close(stmt);
        }

        return result;
    }

    @Override
    public boolean remove(BankAccount entity) throws DAOException {
        boolean result = false;
        PreparedStatement stmt = null;

        try {
            stmt = connection.prepareStatement(SQL_DELETE_BY_ENTITY);
            stmt.setLong(FIRST_INDEX, entity.getId());
            stmt.setLong(SECOND_INDEX, entity.getUserId());
            stmt.setBigDecimal(THIRD_INDEX, entity.getMoney());
            stmt.setString(FOURTH_INDEX, entity.getPassword());
            stmt.executeUpdate();

            result = true;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            this.close(stmt);
        }

        return result;
    }

    @Override
    public boolean create(BankAccount entity) throws DAOException {
        boolean result = false;
        PreparedStatement stmt = null;

        try {
            stmt = connection.prepareStatement(SQL_INSERT_ACCOUNT);
            stmt.setLong(FIRST_INDEX, entity.getId());
            stmt.setLong(SECOND_INDEX, entity.getUserId());
            stmt.setBigDecimal(THIRD_INDEX, entity.getMoney());
            stmt.setString(FOURTH_INDEX, entity.getPassword());
            stmt.executeUpdate();

            result = true;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            this.close(stmt);
        }

        return result;
    }

    @Override
    public BankAccount update(BankAccount entity) throws DAOException {
        BankAccount result = null;
        PreparedStatement stmt = null;

        try {
            stmt = connection.prepareStatement(SQL_UPDATE_ACCOUNT);
            stmt.setLong(FIRST_INDEX, entity.getUserId());
            stmt.setBigDecimal(SECOND_INDEX, entity.getMoney());
            stmt.setString(THIRD_INDEX, entity.getPassword());
            stmt.setLong(FOURTH_INDEX, entity.getId());
            stmt.executeUpdate();

            result = entity;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            this.close(stmt);
        }

        return result;
    }

    public List<BankAccount> findAccountsByUserId(long userId) throws DAOException {
        List<BankAccount> accounts = new ArrayList<>();
        PreparedStatement stmt = null;

        try {
            stmt = connection.prepareStatement(SQL_SELECT_BY_USER_ID);
            stmt.setLong(FIRST_INDEX, userId);
            ResultSet resultSet = stmt.executeQuery();

            while (resultSet.next()) {
                long id = resultSet.getLong(COLUMN_ID);
                BigDecimal money = resultSet.getBigDecimal(COLUMN_MONEY);
                String password = resultSet.getString(COLUMN_PASSWORD);

                accounts.add(new BankAccount(id, userId, money, password));
            }
        } catch (SQLException e) {
            throw new DAOException();
        } finally {
            this.close(stmt);
        }

        return accounts;
    }
}
