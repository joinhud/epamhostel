package com.epam.hostel.entity;

import java.math.BigDecimal;

public class BankAccount extends Entity {
    private long userId;
    private BigDecimal money;
    private String password;

    public BankAccount() {
    }

    public BankAccount(long id, long userId, BigDecimal money, String password) {
        super(id);
        this.userId = userId;
        this.money = money;
        this.password = password;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public BigDecimal getMoney() {
        return money;
    }

    public void setMoney(BigDecimal money) {
        this.money = money;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }

        BankAccount account = (BankAccount) o;

        if (userId != account.userId) {
            return false;
        }
        if (money != null ? !money.equals(account.money) : account.money != null) {
            return false;
        }

        return password != null ? password.equals(account.password) : account.password == null;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (int) (userId ^ (userId >>> 32));
        result = 31 * result + (money != null ? money.hashCode() : 0);
        result = 31 * result + (password != null ? password.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "BankAccount{" +
                "accountId=" + getId() +
                ", userId=" + userId +
                ", money=" + money.doubleValue() +
                ", password='" + password + '\'' +
                '}';
    }
}
