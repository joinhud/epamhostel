package com.epam.hostel.entity;

import java.math.BigDecimal;

public class Room extends Entity {
    private String imageUrl;
    private short maxLivings;
    private boolean type;
    private BigDecimal cost;
    private boolean onRepair;
    private String grade;
    private String description;

    public Room() {
    }

    public Room(long id, String imageUrl, short maxLivings, boolean type, BigDecimal cost, boolean onRepair) {
        super(id);
        this.imageUrl = imageUrl;
        this.maxLivings = maxLivings;
        this.type = type;
        this.cost = cost;
        this.onRepair = onRepair;
    }

    public Room(long id, String imageUrl, short maxLivings, boolean type, BigDecimal cost, boolean onRepair, String grade, String description) {
        super(id);
        this.imageUrl = imageUrl;
        this.maxLivings = maxLivings;
        this.type = type;
        this.cost = cost;
        this.onRepair = onRepair;
        this.grade = grade;
        this.description = description;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public short getMaxLivings() {
        return maxLivings;
    }

    public void setMaxLivings(short maxLivings) {
        this.maxLivings = maxLivings;
    }

    public boolean getType() {
        return type;
    }

    public void setType(boolean type) {
        this.type = type;
    }

    public BigDecimal getCost() {
        return cost;
    }

    public void setCost(BigDecimal cost) {
        this.cost = cost;
    }

    public boolean isOnRepair() {
        return onRepair;
    }

    public void setOnRepair(boolean onRepair) {
        this.onRepair = onRepair;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }

        Room room = (Room) o;

        if (maxLivings != room.maxLivings) {
            return false;
        }
        if (type != room.type) {
            return false;
        }
        if (onRepair != room.onRepair) {
            return false;
        }
        if (imageUrl != null ? !imageUrl.equals(room.imageUrl) : room.imageUrl != null) {
            return false;
        }
        if (cost != null ? !cost.equals(room.cost) : room.cost != null) {
            return false;
        }
        if (grade != null ? !grade.equals(room.grade) : room.grade != null) {
            return false;
        }

        return description != null ? description.equals(room.description) : room.description == null;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (imageUrl != null ? imageUrl.hashCode() : 0);
        result = 31 * result + (int) maxLivings;
        result = 31 * result + (type ? 1 : 0);
        result = 31 * result + (cost != null ? cost.hashCode() : 0);
        result = 31 * result + (onRepair ? 1 : 0);
        result = 31 * result + (grade != null ? grade.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Room{" +
                "number=" + getId() +
                ", imageUrl='" + imageUrl + '\'' +
                ", maxLivings=" + maxLivings +
                ", type=" + type +
                ", cost=" + cost.doubleValue() +
                ", onRepair=" + onRepair +
                ", grade='" + grade + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
