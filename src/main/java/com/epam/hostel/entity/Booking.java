package com.epam.hostel.entity;

import com.epam.hostel.type.BookingState;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

public class Booking extends Entity {
    private long roomNumber;
    private long userId;
    private LocalDateTime bookingDate;
    private int numberOfLiving;
    private LocalDate fromDate;
    private LocalDate toDate;
    private boolean paid;
    private BigDecimal totalCost;
    private BookingState state;

    public Booking() {
    }

    public Booking(long roomNumber, long userId, LocalDateTime bookingDate, int numberOfLiving, LocalDate fromDate, LocalDate toDate, boolean paid, BigDecimal totalCost, BookingState state) {
        this.roomNumber = roomNumber;
        this.userId = userId;
        this.bookingDate = bookingDate;
        this.numberOfLiving = numberOfLiving;
        this.fromDate = fromDate;
        this.toDate = toDate;
        this.paid = paid;
        this.totalCost = totalCost;
        this.state = state;
    }

    public Booking(long id, long roomNumber, long userId, LocalDateTime bookingDate, int numberOfLiving, LocalDate fromDate, LocalDate toDate, boolean paid, BigDecimal totalCost, BookingState state) {
        super(id);
        this.roomNumber = roomNumber;
        this.userId = userId;
        this.bookingDate = bookingDate;
        this.numberOfLiving = numberOfLiving;
        this.fromDate = fromDate;
        this.toDate = toDate;
        this.paid = paid;
        this.totalCost = totalCost;
        this.state = state;
    }

    public long getRoomNumber() {
        return roomNumber;
    }

    public void setRoomNumber(long roomNumber) {
        this.roomNumber = roomNumber;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public LocalDateTime getBookingDate() {
        return bookingDate;
    }

    public void setBookingDate(LocalDateTime bookingDate) {
        this.bookingDate = bookingDate;
    }

    public int getNumberOfLiving() {
        return numberOfLiving;
    }

    public void setNumberOfLiving(int numberOfLiving) {
        this.numberOfLiving = numberOfLiving;
    }

    public LocalDate getFromDate() {
        return fromDate;
    }

    public void setFromDate(LocalDate fromDate) {
        this.fromDate = fromDate;
    }

    public LocalDate getToDate() {
        return toDate;
    }

    public void setToDate(LocalDate toDate) {
        this.toDate = toDate;
    }

    public boolean isPaid() {
        return paid;
    }

    public void setPaid(boolean paid) {
        this.paid = paid;
    }

    public BigDecimal getTotalCost() {
        return totalCost;
    }

    public void setTotalCost(BigDecimal totalCost) {
        this.totalCost = totalCost;
    }

    public BookingState getState() {
        return state;
    }

    public void setState(BookingState state) {
        this.state = state;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }

        Booking booking = (Booking) o;

        if (roomNumber != booking.roomNumber) {
            return false;
        }
        if (userId != booking.userId) {
            return false;
        }
        if (numberOfLiving != booking.numberOfLiving) {
            return false;
        }
        if (paid != booking.paid) {
            return false;
        }
        if (bookingDate != null ? !bookingDate.equals(booking.bookingDate) : booking.bookingDate != null) {
            return false;
        }
        if (fromDate != null ? !fromDate.equals(booking.fromDate) : booking.fromDate != null) {
            return false;
        }
        if (toDate != null ? !toDate.equals(booking.toDate) : booking.toDate != null) {
            return false;
        }
        if (totalCost != null ? !totalCost.equals(booking.totalCost) : booking.totalCost != null) {
            return false;
        }

        return state == booking.state;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (int) (roomNumber ^ (roomNumber >>> 32));
        result = 31 * result + (int) (userId ^ (userId >>> 32));
        result = 31 * result + (bookingDate != null ? bookingDate.hashCode() : 0);
        result = 31 * result + numberOfLiving;
        result = 31 * result + (fromDate != null ? fromDate.hashCode() : 0);
        result = 31 * result + (toDate != null ? toDate.hashCode() : 0);
        result = 31 * result + (paid ? 1 : 0);
        result = 31 * result + (totalCost != null ? totalCost.hashCode() : 0);
        result = 31 * result + (state != null ? state.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Booking{" +
                "id=" + getId() +
                ", roomNumber=" + roomNumber +
                ", userId=" + userId +
                ", bookingDate=" + bookingDate +
                ", numberOfLiving=" + numberOfLiving +
                ", fromDate=" + fromDate +
                ", toDate=" + toDate +
                ", paid=" + paid +
                ", totalCost" + totalCost.doubleValue() +
                ", state=" + state +
                '}';
    }
}
