package com.epam.hostel.entity;

import java.time.LocalDateTime;

public class Ban extends Entity {
    private long userId;
    private LocalDateTime fromDate;
    private LocalDateTime toDate;

    public Ban() {
    }

    public Ban(long userId, LocalDateTime fromDate, LocalDateTime toDate) {
        this.userId = userId;
        this.fromDate = fromDate;
        this.toDate = toDate;
    }

    public Ban(long id, long userId, LocalDateTime fromDate, LocalDateTime toDate) {
        super(id);
        this.userId = userId;
        this.fromDate = fromDate;
        this.toDate = toDate;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public LocalDateTime getFromDate() {
        return fromDate;
    }

    public void setFromDate(LocalDateTime fromDate) {
        this.fromDate = fromDate;
    }

    public LocalDateTime getToDate() {
        return toDate;
    }

    public void setToDate(LocalDateTime toDate) {
        this.toDate = toDate;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }

        Ban ban = (Ban) o;

        if (userId != ban.userId) {
            return false;
        }
        if (fromDate != null ? !fromDate.equals(ban.fromDate) : ban.fromDate != null) {
            return false;
        }

        return toDate != null ? toDate.equals(ban.toDate) : ban.toDate == null;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (int) (userId ^ (userId >>> 32));
        result = 31 * result + (fromDate != null ? fromDate.hashCode() : 0);
        result = 31 * result + (toDate != null ? toDate.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Ban{" +
                "id=" + getId() +
                ", userId=" + userId +
                ", fromDate=" + fromDate +
                ", toDate=" + toDate +
                '}';
    }
}
