package com.epam.hostel.entity;

import java.time.LocalDateTime;

public class Comment extends Entity {
    private long userId;
    private String comment;
    private LocalDateTime date;
    private boolean isDeleted;

    public Comment() {
    }

    public Comment(long userId, String comment, LocalDateTime date, boolean isDeleted) {
        this.userId = userId;
        this.comment = comment;
        this.date = date;
        this.isDeleted = isDeleted;
    }

    public Comment(long id, long userId, String comment, LocalDateTime date, boolean isDeleted) {
        super(id);
        this.userId = userId;
        this.comment = comment;
        this.date = date;
        this.isDeleted = isDeleted;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public boolean isDeleted() {
        return isDeleted;
    }

    public void setDeleted(boolean deleted) {
        isDeleted = deleted;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }

        Comment comment1 = (Comment) o;

        if (userId != comment1.userId) {
            return false;
        }
        if (isDeleted != comment1.isDeleted) {
            return false;
        }
        if (comment != null ? !comment.equals(comment1.comment) : comment1.comment != null) {
            return false;
        }

        return date != null ? date.equals(comment1.date) : comment1.date == null;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (int) (userId ^ (userId >>> 32));
        result = 31 * result + (comment != null ? comment.hashCode() : 0);
        result = 31 * result + (date != null ? date.hashCode() : 0);
        result = 31 * result + (isDeleted ? 1 : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Comment{" +
                "id=" + getId() +
                ", userId=" + userId +
                ", comment='" + comment + '\'' +
                ", date=" + date +
                ", isDeleted=" + isDeleted +
                '}';
    }
}
