package com.epam.hostel.validator;

/**
 * The {@code RoomValidator} class validate room entities
 * values.
 *
 * @author Eugene Dubouskiy
 * @version 1.0
 */
public class RoomValidator {
    /** Regular expression constant for checking digit consisting {@code String}.*/
    private static final String DIGIT_CONSIST_REGEX = "^\\d+$";

    /** Regular expression constant for checking real digits consisting {@code String}.*/
    private static final String COST_REGEX = "^\\d+.?\\d{0,2}$";

    /** Error message which used when max livings value is incorrect*/
    private static final String ERROR_MAX_LIVINGS = "Number of max livings must contains only digits!";

    /** Error message which used when cost value is incorrect*/
    private static final String ERROR_COST = "Incorrect value for cost!";

    /** Error message which used when room number value is incorrect*/
    private static final String ERROR_ROOM_NUMBER = "Incorrect value for room number!";

    /**
     * Validate room number {@code String}.
     *
     * Return error message if roomNumber {@code String} isn't
     * number value or roomNumber is {@code null}
     *
     * @param roomNumber {@code String} room number value
     *
     * @return error message if {@code roomNumber} incorrect
     *         or {@code null}
     */
    public String validRoomNumber(String roomNumber) {
        String errorMsg = "";

        if (roomNumber == null || !roomNumber.matches(DIGIT_CONSIST_REGEX)) {
            errorMsg = ERROR_ROOM_NUMBER;
        }

        return errorMsg;
    }

    /**
     * Validate max livings {@code String} value.
     *
     * Return error message if {@code maxLivings} is not consist
     * digit symbols or is {@code null}.
     *
     * @param  maxLivings {@code String} max livings value
     *
     * @return error message if {@code maxLivings} incorrect
     *         or {@code null}
     */
    public String validMaxLivings(String maxLivings) {
        String errorMsg = "";

        if (maxLivings == null || !maxLivings.matches(DIGIT_CONSIST_REGEX)) {
            errorMsg = ERROR_MAX_LIVINGS;
        }

        return errorMsg;
    }

    /**
     * Validate cost {@code String} value
     *
     * Return error message if {@code cost} is not
     * number value or is {@code null}.
     *
     * @param cost {@code String} cost value
     *
     * @return error message if {@code cost} incorrect
     * or {@code null}*/
    public String validateCost(String cost) {
        String errorMsg = "";

        if (cost == null || !cost.matches(COST_REGEX)) {
            errorMsg = ERROR_COST;
        }

        return errorMsg;
    }
}
