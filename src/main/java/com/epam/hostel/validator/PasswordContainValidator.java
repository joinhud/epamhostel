package com.epam.hostel.validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * The abstract class {@code PasswordContainValidator} is the superclass of
 * classes for validating entities values which contains password field.
 *
 * @author Eugene Dubouskiy
 * @version 1.0
 */
public abstract class PasswordContainValidator {
    /** Constant that define min password length*/
    private static final int REQUIRED_PASS_LENGTH = 6;

    //Regular expressions for checking values
    /** Regular expression constant for checking character in lower case consisting*/
    private static final String LOWER_SYMBOL_REGEX = "[\\p{Lower}\\u00f1\\u0430-\\u044f]";

    /** Regular expression constant for checking character in upper case consisting*/
    private static final String UPPER_SYMBOL_REGEX = "[\\p{Upper}\\u0410-\\u042f\\u00d1]";

    /** Regular expression constant for checking digit character consisting*/
    private static final String DIGIT_SYMBOL_REGEX = "\\d";

        /** Error message which used when password length is incorrect*/
    private static final String PASS_ERROR_LENGTH_MSG = "Password must be consist from 6 symbols at least!<br/>";

    /** Error message which used when password is not contain character in lower case*/
    private static final String PASS_ERROR_LOWER_SYMBOL_MSG = "Password must be contains one alphabetic symbol in lower case at least!<br/>";

    /** Error message which used when password in not contain character in upper case*/
    private static final String PASS_ERROR_UPPER_SYMBOL_MSG = "Password must be contains one alphabetic symbol in upper case at least!<br/>";

    /** Error message which used when password is no contain digit symbol*/
    private static final String PASS_ERROR_DIGIT_SYMBOL_MSG = "Password must be contains one digit symbol at least!<br/>";

    /** Error message which used when password confirmation isn't match password*/
    private static final String CONFIRMATION_ERROR_MSG = "Confirmation doesn't match the password!";

    /* Check containing regex param in value param*/
    private boolean containsRegex(String value, String regex) {
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(value);

        return matcher.find();
    }

    /**
     * Validate password value.
     *
     * Return error message if password incorrect
     *
     * @param password the password value to be validate
     * @return error message if password is not valid
     */
    public String validatePassword(String password) {
        String errorMsg = "";

        if (password.length() < REQUIRED_PASS_LENGTH) {
            errorMsg += PASS_ERROR_LENGTH_MSG;
        }

        if (!containsRegex(password, LOWER_SYMBOL_REGEX)) {
            errorMsg += PASS_ERROR_LOWER_SYMBOL_MSG;
        }

        if (!containsRegex(password, UPPER_SYMBOL_REGEX)) {
            errorMsg += PASS_ERROR_UPPER_SYMBOL_MSG;
        }

        if (!containsRegex(password, DIGIT_SYMBOL_REGEX)) {
            errorMsg += PASS_ERROR_DIGIT_SYMBOL_MSG;
        }

        return errorMsg;
    }

    /**
     * Checking password confirmation.
     *
     * Return error message if confirmation is not matched password
     *
     * @param password value of which is compared
     * @param confirmation value of which can be verified
     * @return error message if confirmation isn't match password
     */
    public String checkPasswordConfirmation(String password, String confirmation) {
        String errorMsg = "";

        if (!password.equals(confirmation)) {
            errorMsg = CONFIRMATION_ERROR_MSG;
        }

        return errorMsg;
    }
}
