package com.epam.hostel.validator;

import java.time.LocalDate;

/**
 * The {@code BookingValidator} class validate booking entity
 * values.
 *
 * @author Eugene Dubouskiy
 * @version 1.0
 */
public class BookingValidator {
    /* Minimal count fo livings*/
    private static final int MIN_COUNT = 1;

    /* Error messages*/
    private static final String COUNT_LIVINGS_ERROR_MSG = "Count of livings must be greater than 0. <br/>";
    private static final String INTERVAL_ERROR_MSG = "From date must be earlier than To date! <br/>";
    private static final String DATE_ERROR_MSG = "Incorrect date! <br/>";

    /**
     * Validate count of livings {@code String}.
     *
     * Return error message if count of livings {@code String} isn't
     * correct or count of livings is {@code null}.
     *
     * @param countOfLivings {@code String} count of livings value
     *
     * @return error message if {@code countOfLivings} incorrect
     *         or {@code null}
     */
    public String validCountOfLivings(int countOfLivings) {
        String errorMsg = "";

        if (countOfLivings < MIN_COUNT) {
            errorMsg = COUNT_LIVINGS_ERROR_MSG;
        }

        return errorMsg;
    }

    /**
     * Validate date interval.
     *
     * Return error message if from date is after
     * to date or from date is to date.
     *
     * @param fromDate {@code LocalDate} from date value
     * @param toDate {@code LocalDate} to date value
     *
     * @return error message if {@code fromDate} is after
     *         {@code toDate} or {@code fromDate} is
     *         {@code toDate}
     */
    public String validateInterval(LocalDate fromDate, LocalDate toDate) {
        String errorMsg = "";

        if (fromDate.isAfter(toDate) || fromDate.isEqual(toDate)) {
            errorMsg = INTERVAL_ERROR_MSG;
        }

        return errorMsg;
    }

    /**
     * Validate date value.
     *
     * Return error message if date isn't after today date.
     *
     * @param date {@code LocalDate} date value
     *
     * @return error message if {@code date} before today date
     */
    public String validateDate(LocalDate date) {
        String errorMsg = "";

        if (date.isBefore(LocalDate.now())) {
            errorMsg = DATE_ERROR_MSG;
        }

        return errorMsg;
    }
}