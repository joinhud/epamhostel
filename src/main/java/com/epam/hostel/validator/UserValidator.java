package com.epam.hostel.validator;

/**
 * The {@code UserValidator} class validate user entities
 * values.
 *
 * @author Eugene Dubouskiy
 * @version 1.0
 *
 * @see PasswordContainValidator
 */
public class UserValidator extends PasswordContainValidator {
    /** Regular expression for checking email value*/
    private static final String EMAIL_REGEX = "^(([^<>()\\[\\]\\\\.,;:\\s@\"]+(\\.[^<>()\\[\\]\\\\.,;:\\s@\"]+)*)|(\".+\"))@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$"; //"^[\\\\w!#$%&’*+/=?`{|}~^-]+(?:\\\\.[\\\\w!#$%&’*+/=?`{|}~^-]+)*@(?:[a-zA-Z0-9-]+\\\\.)+[a-zA-Z]{2,6}$";

    /** Regular expression for checking name value*/
    private static final String NAME_REGEX = "^[A-Z\\u0410-\\u042f\\u00d1][a-zA-Z\\u0410-\\u044f\\u00d1\\u00f1]+$";

    /** Regular expression for checking discount value*/
    private static final String DISCOUNT_REGEX = "^\\d+.?\\d{0,2}$";

    /** Error message which used when email value is incorrect*/
    private static final String EMAIL_ERROR_MSG = "Invalid e-mail!";

    /** Error message which used when name value is incorrect*/
    private static final String NAME_ERROR_MSG = "Incorrect name!";

    /** Error message which used when discount value is incorrect*/
    private static final String DISCOUNT_ERROR_MSG = "Incorrect discount value!";

    /**
     * Validate email {@code String}.
     *
     * Return error message if email {@code String} isn't
     * correct or email is {@code null}.
     *
     * @param email {@code String} email value
     *
     * @return error message if {@code email} incorrect
     *         or {@code null}
     */
    public String validEmail(String email) {
        String errorMsg = "";

        if(email == null || !email.matches(EMAIL_REGEX)) {
            errorMsg = EMAIL_ERROR_MSG;
        }

        return errorMsg;
    }

    /**
     * Validate name {@code String}.
     *
     * Return error message if name {@code String} isn't
     * correct or name is {@code null}.
     *
     * @param name {@code String} name value
     *
     * @return error message if {@code name} incorrect
     *         or {@code null}
     */
    public String validateName(String name) {
        String errorMsg = "";

        if (name == null || !name.matches(NAME_REGEX)) {
            errorMsg = NAME_ERROR_MSG;
        }

        return errorMsg;
    }

    /**
     * Validate discount {@code String}.
     *
     * Return error message if discount {@code String} isn't
     * correct or discount is {@code null}.
     *
     * @param discountName {@code String} discount value
     *
     * @return error message if {@code discountName} incorrect
     *         or {@code null}
     */
    public String validateDiscount(String discountName) {
        String errorMsg = "";

        if (discountName == null || !discountName.matches(DISCOUNT_REGEX)) {
            errorMsg = DISCOUNT_ERROR_MSG;
        } else {
            float discount = Float.parseFloat(discountName);

            if (discount < 1 || discount > 100) {
                errorMsg = DISCOUNT_ERROR_MSG;
            }
        }

        return errorMsg;
    }
}
