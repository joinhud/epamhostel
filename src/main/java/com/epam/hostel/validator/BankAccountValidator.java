package com.epam.hostel.validator;

/**
 * The {@code BankAccountValidator} class validate bank account entity
 * values.
 *
 * @author Eugene Dubouskiy
 * @version 1.0
 *
 * @see PasswordContainValidator
 */
public class BankAccountValidator extends PasswordContainValidator {
    /* Error messages*/
    private static final String ACCOUNT_ERROR_MSG = "Bank account must contains only 8 digits!";
    private static final String MONEY_ERROR_MSG = "Incorrect value";
    /* Regular expressions*/
    private static final String ACCOUNT_REGEX = "^\\d{8}$";
    private static final String MONEY_REGEX = "^\\d+.?\\d{0,2}$";

    /**
     * Validate {@code String} number value.
     *
     * Return error message if number isn't
     * correct or number is {@code null}.
     *
     * @param number {@code String} number value
     *
     * @return error message if {@code number} incorrect
     *         or {@code null}
     */
    public String validateAccountNumber(String number) {
        String errorMsg = "";

        if (number == null || !number.matches(ACCOUNT_REGEX)) {
            errorMsg = ACCOUNT_ERROR_MSG;
        }

        return errorMsg;
    }

    /**
     * Validate {@code String} money value.
     *
     * Return error message if money isn't
     * correct or money is {@code null}.
     *
     * @param money {@code String} money value
     *
     * @return error message if {@code money} incorrect
     *         or {@code null}
     */
    public String validateMoneyInput(String money) {
        String errorMsg = "";

        if (money == null || !money.matches(MONEY_REGEX)) {
            errorMsg = MONEY_ERROR_MSG;
        }

        return errorMsg;
    }
}
