package com.epam.hostel.manager;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.MissingResourceException;
import java.util.ResourceBundle;

public class ConfigurationManager {
    private static final Logger LOG = LogManager.getLogger();
    private static final String PATH_CONFIG = "properties.config";

    private ResourceBundle resourceBundle;

    public ConfigurationManager() {
        try {
            resourceBundle = ResourceBundle.getBundle(PATH_CONFIG);
        } catch (MissingResourceException e) {
            LOG.log(Level.FATAL, e);
            throw new RuntimeException();
        }
    }

    public String getProperty(String key) {
        String property;

        try {
            property = resourceBundle.getString(key);
        } catch (MissingResourceException e) {
            LOG.log(Level.FATAL, e);
            throw new RuntimeException();
        }

        return property;
    }
}
