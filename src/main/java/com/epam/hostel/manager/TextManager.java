package com.epam.hostel.manager;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

public class TextManager {
    private static final Logger LOG = LogManager.getLogger();
    private static final String PATH_TEXT = "properties.text";

    private ResourceBundle resourceBundle;

    public TextManager(Locale locale) {
        try {
            resourceBundle = ResourceBundle.getBundle(PATH_TEXT, locale);
        } catch (MissingResourceException e) {
            LOG.log(Level.FATAL, e);
            throw new RuntimeException(e);
        }
    }

    public String getProperty(String key) {
        String property;

        try {
            property = resourceBundle.getString(key);
        } catch (MissingResourceException e) {
            LOG.log(Level.FATAL, e);
            throw new RuntimeException(e);
        }

        return property;
    }
}
