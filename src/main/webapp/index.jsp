<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="utf-8"%>
<fmt:setLocale value="${sessionScope.get('locale')}"/>
<fmt:setBundle basename="properties.text"/>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="theme-color" content="#3F51B5">
    <title>EPAM <fmt:message key="label.title"/></title>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/theme.css">
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
</head>
<body>
<header>
    <%@ include file="jsp/header.jsp"%>
    <div class="header-image">
        <h1 class="header-image-text"><fmt:message key="label.header.text"/></h1>
    </div>
</header>
<%@ include file="jsp/navigation.jsp"%>
<div class="container-fluid">
    <div class="row">
        <article class="col-md-7 col-md-offset-1 col-sm-12 col-xs-12">
            <section class="">
                <h2><fmt:message key="label.title.rooms.shared"/></h2>
                <c:forEach var="sharedRoom" items="${sharedRooms}">
                    <div class="card">
                        <img src="${sharedRoom.imageUrl}">
                        <div class="card-content clearfix">
                            <div class="text-content clearfix">
                                <p><fmt:message key="label.title.room"/> №<c:out value="${sharedRoom.id}"/></p>
                                <p><fmt:message key="label.title.sleeps"/>: <c:out value="${sharedRoom.maxLivings}"/></p>
                                <p><fmt:message key="label.title.cost"/>: <c:out value="${sharedRoom.cost}"/>$</p>
                                <c:if test="${not empty sharedRoom.grade}">
                                    <p><fmt:message key="label.title.grade"/>: <c:out value="${sharedRoom.grade}"/></p>
                                </c:if>
                                <c:if test="${not empty sharedRoom.description}">
                                    <p class="description">
                                        <fmt:message key="label.title.description"/>: <c:out value="${sharedRoom.description}"/>
                                    </p>
                                </c:if>
                            </div>
                            <div class="links">
                                <a href="/EpamHostel?command=to_book&room=${sharedRoom.id}" class="ripple">
                                    <fmt:message key="label.link.book"/>
                                </a>
                            </div>
                        </div>
                    </div>
                </c:forEach>
            </section>
            <section class="">
                <h2><fmt:message key="label.title.rooms.private"/></h2>
                <c:forEach var="privateRoom" items="${privateRooms}">
                    <div class="card">
                        <img src="${privateRoom.imageUrl}">
                        <div class="card-content clearfix">
                            <div class="text-content clearfix">
                                <p><fmt:message key="label.title.room"/> №<c:out value="${privateRoom.id}"/></p>
                                <p><fmt:message key="label.title.sleeps"/>: <c:out value="${privateRoom.maxLivings}"/></p>
                                <p><fmt:message key="label.title.cost"/>: <c:out value="${privateRoom.cost}"/>$</p>
                                <c:if test="${not empty privateRoom.grade}">
                                    <p><fmt:message key="label.title.grade"/>: <c:out value="${privateRoom.grade}"/></p>
                                </c:if>
                                <c:if test="${not empty privateRoom.description}">
                                    <p class="description">
                                        <fmt:message key="label.title.description"/>: <c:out value="${privateRoom.description}"/>
                                    </p>
                                </c:if>
                            </div>
                            <div class="links">
                                <a href="/EpamHostel?command=to_book&room=${privateRoom.id}" class="ripple">
                                    <fmt:message key="label.link.book"/>
                                </a>
                            </div>
                        </div>
                    </div>
                </c:forEach>
            </section>
        </article>
        <aside class="col-md-3 col-sm-12 col-xs-12">
            <div class="card">
                <h4><fmt:message key="label.title.include"/></h4>
                <p><fmt:message key="label.title.include.linen"/></p>
            </div>
            <div class="card">
                <h4><fmt:message key="label.title.facilities"/></h4>
                <ul>
                    <li>
                        <p><fmt:message key="label.title.facilities.breakfast"/></p>
                    </li>
                    <li>
                        <p><fmt:message key="label.title.facilities.wifi"/></p>
                    </li>
                    <li>
                        <p><fmt:message key="label.title.facilities.lockers"/></p>
                    </li>
                    <li>
                        <p><fmt:message key="label.title.facilities.self.catering"/></p>
                    </li>
                    <li>
                        <p><fmt:message key="label.title.facilities.reception"/></p>
                    </li>
                    <li>
                        <p><fmt:message key="label.title.facilities.security"/></p>
                    </li>
                    <li>
                        <p><fmt:message key="label.title.facilities.adaptors"/></p>
                    </li>
                    <li>
                        <p><fmt:message key="label.title.facilities.tv" /></p>
                    </li>
                    <li>
                        <p><fmt:message key="label.title.facilities.phones" /></p>
                    </li>
                    <li>
                        <p><fmt:message key="label.title.facilities.common.room" /></p>
                    </li>
                    <li>
                        <p><fmt:message key="label.title.facilities.dvd" /></p>
                    </li>
                    <li>
                        <p><fmt:message key="label.title.facilities.dishwasher" /></p>
                    </li>
                    <li>
                        <p><fmt:message key="label.title.facilities.maps" /></p>
                    </li>
                    <li>
                        <p><fmt:message key="label.title.facilities.internet.access" /></p>
                    </li>
                    <li>
                        <p><fmt:message key="label.title.facilities.freezer" /></p>
                    </li>
                    <li>
                        <p><fmt:message key="label.title.facilities.hairdryers" /></p>
                    </li>
                    <li>
                        <p><fmt:message key="label.title.facilities.shower" /></p>
                    </li>
                    <li>
                        <p><fmt:message key="label.title.facilities.housekeeping" /></p>
                    </li>
                </ul>
            </div>
            <div class="card">
                <h4><fmt:message key="label.title.important" /></h4>
                <p>
                    <fmt:message key="label.title.important.content"/>
                </p>
            </div>
            <div class="card">
                <h4><fmt:message key="label.title.check.in.out" /></h4>
                <p><fmt:message key="label.title.check.in"/>: 00:00</p>
                <p><fmt:message key="label.title.check.out"/>: 10:00</p>
            </div>
            <div class="card">
                <h4><fmt:message key="label.title.policies" /></h4>
                <ul>
                    <li>
                        <p><fmt:message key="label.title.policies.child.friendly" /></p>
                    </li>
                    <li>
                        <p><fmt:message key="label.title.policies.credit.card" /></p>
                    </li>
                    <li>
                        <p><fmt:message key="label.title.policies.curfew" /></p>
                    </li>
                    <li>
                        <p><fmt:message key="label.title.policies.smoking" /></p>
                    </li>
                    <li>
                        <p><fmt:message key="label.title.policies.taxes" /></p>
                    </li>
                </ul>
            </div>
        </aside>
    </div>
</div>
<script src="js/behavior.js"></script>
</body>
</html>
