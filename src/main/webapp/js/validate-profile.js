window.onload = function () {
    //--------- Validate Old Password -----------
    var inputOldPassword = document.getElementById("oldPassword");

    inputOldPassword.onblur = function () {
        var result = validatePassword(this.value);

        if (result.length > 0) {
            var errorMsg = "";

            for(var i = 0; i < result.length; i++) {
                errorMsg += result[i] + "<br/>";
            }

            var error = document.getElementById("errorOldPass");
            error.innerHTML = errorMsg;
        }
    };

    inputOldPassword.onfocus = function () {
        var errorMsg = document.getElementById("errorOldPass");
        errorMsg.innerHTML = "";
    };

    //--------- Validate New Password -----------
    var inputNewPassword = document.getElementById("newPassword");

    inputNewPassword.onblur = function () {
        var result = validatePassword(this.value);

        if (result.length > 0) {
            var errorMsg = "";

            for(var i = 0; i < result.length; i++) {
                errorMsg += result[i] + "<br/>";
            }

            var error = document.getElementById("errorNewPass");
            error.innerHTML = errorMsg;
        }
    };

    inputNewPassword.onfocus = function () {
        var errorMsg = document.getElementById("errorNewPass");
        errorMsg.innerHTML = "";
    };

    //---------- Validate First Name -------------
    var inputFirstName = document.getElementById("firstName");

    inputFirstName.onblur = function () {
        var resultValid = validateName(this.value);

        if (resultValid !== null) {
            var errorMsg = document.getElementById("errorFirstName");
            errorMsg.innerHTML = resultValid;
        }
    };

    inputFirstName.onfocus = function () {
        var errorMsg = document.getElementById("errorFirstName");
        errorMsg.innerHTML = "";
    };

    //---------- Validate Last Name -----------
    var inputLastName = document.getElementById("lastName");

    inputLastName.onblur = function () {
        var resultValid = validateName(this.value);

        if (resultValid !== null) {
            var errorMsg = document.getElementById("errorLastName");
            errorMsg.innerHTML = resultValid;
        }
    };

    inputLastName.onfocus = function () {
        var errorMsg = document.getElementById("errorLastName");
        errorMsg.innerHTML = "";
    };

    //validating all on submit event

    var formChangePass = document.forms["change-pass-form"];
    var formChangeFirstName = document.forms["change-first-name-form"];
    var formChangeLastName = document.forms["change-last-name-form"];

    formChangePass.addEventListener("submit", function (event) {
        var flag = true;

        if (validatePassword(inputOldPassword.value).length > 0) {
            inputOldPassword.onblur();
            flag = false;
        }

        if (validatePassword(inputNewPassword.value).length > 0) {
            inputNewPassword.onblur();
            flag = false;
        }

        if(!flag) {
            event.preventDefault();
        }
    });

    formChangeFirstName.addEventListener("submit", function (event) {
        var flag = true;

        if (validateName(inputFirstName.value) !== null) {
            inputFirstName.onblur();
            flag = false;
        }

        if(!flag) {
            event.preventDefault();
        }
    });

    formChangeLastName.addEventListener("submit", function (event) {
        var flag = true;

        if (validateName(inputLastName.value) !== null) {
            inputLastName.onblur();
            flag = false;
        }

        if(!flag) {
            event.preventDefault();
        }
    });
};

function validatePassword(password) {
    var errorMsg = [];

    if (password.length < 6) {
        errorMsg.push("Password must be consist from 6 symbols at least!");
    }

    if (!/[a-z\u00f1\u0430-\u044f]/.test(password)) {
        errorMsg.push("Password must be contains one alphabetic symbol in lower case at least!");
    }

    if (!/[A-Z\u0410-\u042f\u00d1]/.test(password)) {
        errorMsg.push("Password must be contains one alphabetic symbol in upper case at least!");
    }

    if (!/\d/.test(password)) {
        errorMsg.push("Password must be contains one digit symbol at least!");
    }

    return errorMsg;
}

function validateName(name) {
    var regexp = /^[A-Z\u0410-\u042f\u00d1][a-zA-Z\u0410-\u044f\u00d1\u00f1]+$/;
    var result = regexp.test(name);
    var errorMsg = null;

    if (result !== true) {
        errorMsg = "Invalid value!";
    }

    return errorMsg;
}
