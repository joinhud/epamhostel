(function (window, $) {
    $(function() {
        $('.ripple').on('click', function (event) {
            // event.preventDefault();

            var $div = $('<div/>');
            var btnOffset = $(this).offset();
            var xPos = event.pageX - btnOffset.left;
            var yPos = event.pageY - btnOffset.top;

            $div.addClass('ripple-effect');
            var $ripple = $(".ripple-effect");

            $ripple.css("height", $(this).height());
            $ripple.css("width", $(this).width());

            $div
                .css({
                    top: yPos - (25 / 2),
                    left: xPos - (25 / 2),
                    background: $(this).data("ripple-color")
                })
                .appendTo($(this));

            window.setTimeout(function(){
                $div.remove();
            }, 1000);
        });
    });
})(window, jQuery);

$(function() {
    $('<div id="overlay" />').insertAfter('body');

    // Navigation Slide In
    $('.nav-icon').click(function() {
        $('nav').addClass('slide-in');
        $('html').css("overflow", "hidden");
        $('#overlay').show();
        return false;
    });

    // Navigation Slide Out
    $('#overlay').click(function() {
        $('nav').removeClass('slide-in');
        $('html').css("overflow", "auto");
        $('#overlay').hide();
        return false;
    });
});

$(function () {
    $('#paid').change(function () {
        var collapsedBlock = $('#collapsed-block');

        if (this.checked) {
            collapsedBlock.removeClass("closed");
            collapsedBlock.addClass("opened");
        } else {
            collapsedBlock.removeClass("opened");
            collapsedBlock.addClass("closed");
        }
    });
});
