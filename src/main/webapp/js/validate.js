window.onload = function () {
    //--------- Validate Email ------------
    var inputEmail = document.getElementById("email");

    inputEmail.onblur = function () {
        var resultValid = validateEmail(this.value);

        if (resultValid !== null) {
            var errorMsg = document.getElementById("errorEmailMsg");
            errorMsg.innerHTML = resultValid;
        }
    };

    inputEmail.onfocus = function () {
        var errorMsg = document.getElementById("errorEmailMsg");
        errorMsg.innerHTML = "";
    };

    //--------- Validate Password -----------
    var inputPassword = document.getElementById("password");

    inputPassword.onblur = function () {
        var result = validatePassword(this.value);

        if (result.length > 0) {
            var errorMsg = "";

            for(var i = 0; i < result.length; i++) {
                errorMsg += result[i] + "<br/>";
            }

            var error = document.getElementById("errorPasswordMsg");
            error.innerHTML = errorMsg;
        }
    };

    inputPassword.onfocus = function () {
        var errorMsg = document.getElementById("errorPasswordMsg");
        errorMsg.innerHTML = "";
    };

    //---------- Validate Password confirmation ----------
    var inputConfirm = document.getElementById("passConfirm");

    inputConfirm.onblur = function () {
        if (!checkPassConfirm(inputPassword.value, this.value)) {
            var error = document.getElementById("errorPassConfirmMsg");
            error.innerHTML = "Confirmation doesn't match the password!";
        }
    };

    inputConfirm.onfocus = function () {
        var errorMsg = document.getElementById("errorPassConfirmMsg");
        errorMsg.innerHTML = "";
    };

    //---------- Validate First Name -------------
    var inputFirstName = document.getElementById("firstName");

    inputFirstName.onblur = function () {
        var resultValid = validateName(this.value);

        if (resultValid !== null) {
            var errorMsg = document.getElementById("errorFirstNameMsg");
            errorMsg.innerHTML = resultValid;
        }
    };

    inputFirstName.onfocus = function () {
        var errorMsg = document.getElementById("errorFirstNameMsg");
        errorMsg.innerHTML = "";
    };

    //---------- Validate Last Name -----------
    var inputLastName = document.getElementById("lastName");

    inputLastName.onblur = function () {
        var resultValid = validateName(this.value);

        if (resultValid !== null) {
            var errorMsg = document.getElementById("errorLastNameMsg");
            errorMsg.innerHTML = resultValid;
        }
    };

    inputLastName.onfocus = function () {
        var errorMsg = document.getElementById("errorLastNameMsg");
        errorMsg.innerHTML = "";
    };

    //validating all on submit event

    var formSignIn = document.forms["signin-form"];
    var formRegister = document.forms["register-form"];

    formSignIn.addEventListener("submit", function (event) {
        var flag = true;

        if (validatePassword(inputPassword.value) !== null) {
            inputPassword.onblur();
            flag = false;
        }

        if (validateEmail(inputEmail.value) !== null) {
            inputEmail.onblur();
            flag = false;
        }

        if(!flag) {
            event.preventDefault();
        }
    });

    formRegister.addEventListener("submit", function (event) {
        var flag = true;

        if (validateEmail(inputEmail.value) !== null) {
            inputEmail.onblur();
            flag = false;
        }

        if (validatePassword(inputPassword.value).length > 0) {
            inputPassword.onblur();
            flag = false;
        }

        if (!checkPassConfirm(inputPassword.value, inputConfirm.value)) {
            inputConfirm.onblur();
            flag = false;
        }

        if (validateName(inputFirstName.value) !== null) {
            inputFirstName.onblur();
            flag = false;
        }

        if (validateName(inputLastName.value) !== null) {
            inputLastName.onblur();
            flag = false;
        }

        if(!flag) {
            event.preventDefault();
        }
    });
};

function validateEmail(email) {
    var regexp = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    var result = regexp.test(email);
    var errorMsg = null;

    if (result !== true) {
        errorMsg = "Invalid e-mail!";
    }

    return errorMsg;
}

function validatePassword(password) {
    var errorMsg = [];

    if (password.length < 6) {
        errorMsg.push("Password must be consist from 6 symbols at least!");
    }

    if (!/[a-z\u00f1\u0430-\u044f]/.test(password)) {
        errorMsg.push("Password must be contains one alphabetic symbol in lower case at least!");
    }

    if (!/[A-Z\u0410-\u042f\u00d1]/.test(password)) {
        errorMsg.push("Password must be contains one alphabetic symbol in upper case at least!");
    }

    if (!/\d/.test(password)) {
        errorMsg.push("Password must be contains one digit symbol at least!");
    }

    return errorMsg;
}

function checkPassConfirm(password, confirm) {
    return password === confirm;
}

function validateName(name) {
    var regexp = /^[A-Z\u0410-\u042f\u00d1][a-zA-Z\u0410-\u044f\u00d1\u00f1]+$/;
    var result = regexp.test(name);
    var errorMsg = null;

    if (result !== true) {
        errorMsg = "Invalid value!";
    }

    return errorMsg;
}