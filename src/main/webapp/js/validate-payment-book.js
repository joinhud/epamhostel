window.onload = function () {
    var paymentForm = document.forms["payment-form"];
    var inputPassword = document.getElementById("password");

    paymentForm.addEventListener("submit", function (event) {
        var result = validatePassword(inputPassword.value)

        if (result.length > 0) {
            var errorMsg = "";

            for(var i = 0; i < result.length; i++) {
                errorMsg += result[i] + "<br/>";
            }

            var error = document.getElementById("errorPassword");
            error.innerHTML = errorMsg;
        }
    });
};

function validatePassword(password) {
    var errorMsg = [];

    if (password.length < 6) {
        errorMsg.push("Password must be consist from 6 symbols at least!");
    }

    if (!/[a-z\u00f1\u0430-\u044f]/.test(password)) {
        errorMsg.push("Password must be contains one alphabetic symbol in lower case at least!");
    }

    if (!/[A-Z\u0410-\u042f\u00d1]/.test(password)) {
        errorMsg.push("Password must be contains one alphabetic symbol in upper case at least!");
    }

    if (!/\d/.test(password)) {
        errorMsg.push("Password must be contains one digit symbol at least!");
    }

    return errorMsg;
}