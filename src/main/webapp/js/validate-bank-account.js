window.onload = function () {
    //--------- Validate Bank Account ---------
    var inputAccount = document.getElementById("account");

    inputAccount.onblur = function () {
        var result = validateAccountNumber(this.value);

        if (result !== null) {
            var errorDiv = document.getElementById("errorAccount");
            errorDiv.innerHTML = result;
        }
    };

    inputAccount.onfocus = function () {
        var errorDiv = document.getElementById("errorAccount");
        errorDiv.innerHTML = "";
    };

    //--------- Validate Password -----------
    var inputPassword = document.getElementById("password");

    inputPassword.onblur = function () {
        var result = validatePassword(this.value);

        if (result.length > 0) {
            var errorMsg = "";

            for(var i = 0; i < result.length; i++) {
                errorMsg += result[i] + "<br/>";
            }

            var error = document.getElementById("errorPassword");
            error.innerHTML = errorMsg;
        }
    };

    inputPassword.onfocus = function () {
        var errorMsg = document.getElementById("errorPassword");
        errorMsg.innerHTML = "";
    };

    //---------- Validate Password confirmation ----------
    var inputConfirm = document.getElementById("passConfirm");

    inputConfirm.onblur = function () {
        if (!checkPassConfirm(inputPassword.value, this.value)) {
            var error = document.getElementById("errorPassConfirm");
            error.innerHTML = "Confirmation doesn't match the password!";
        }
    };

    inputConfirm.onfocus = function () {
        var errorMsg = document.getElementById("errorPassConfirm");
        errorMsg.innerHTML = "";
    };

    //validating all on submit event
    var formAddAccount = document.forms["add-account-form"];

    formAddAccount.addEventListener("submit", function (event) {
        var flag = true;

        if (validateAccountNumber(inputAccount.value) !== null) {
            inputAccount.onblur();
            flag = false;
        }

        if (validatePassword(inputPassword.value).length > 0) {
            inputPassword.onblur();
            flag = false;
        }

        if (!checkPassConfirm(inputPassword.value, inputConfirm.value)) {
            inputConfirm.onblur();
            flag = false;
        }

        if(!flag) {
            event.preventDefault();
        }
    });
};

function validateAccountNumber(account) {
    var errorMsg = null;
    var regexp = /^[0-9]{8}$/;
    var result = regexp.test(account);

    if (result !== true) {
        errorMsg = "Bank account must contains only 8 digits!";
    }

    return errorMsg;
}

function validatePassword(password) {
    var errorMsg = [];

    if (password.length < 6) {
        errorMsg.push("Password must be consist from 6 symbols at least!");
    }

    if (!/[a-z\u00f1\u0430-\u044f]/.test(password)) {
        errorMsg.push("Password must be contains one alphabetic symbol in lower case at least!");
    }

    if (!/[A-Z\u0410-\u042f\u00d1]/.test(password)) {
        errorMsg.push("Password must be contains one alphabetic symbol in upper case at least!");
    }

    if (!/\d/.test(password)) {
        errorMsg.push("Password must be contains one digit symbol at least!");
    }

    return errorMsg;
}

function checkPassConfirm(password, confirm) {
    return password === confirm;
}