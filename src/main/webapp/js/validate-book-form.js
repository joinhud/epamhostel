window.onload = function () {
    var formBook = document.forms["book-form"];

    formBook.addEventListener("submit", function (event) {
        var inputFromDate = document.getElementById("fromDate");
        var inputToDate = document.getElementById("toDate");
        var fromDate = Date.parse(inputFromDate.value);
        var toDate = Date.parse(inputToDate.value);
        var errorMsg = document.getElementById("errorMsg");
        var resultValid = validateDate(fromDate);
        resultValid += validateDate(toDate);
        resultValid += validateFromDate(fromDate, toDate);

        if (resultValid !== "") {
            errorMsg.innerHTML = resultValid;
            event.preventDefault();
        }
    });
};

function validateFromDate(fromDate, toDate) {
    var errorMsg = "";

    if (fromDate >= toDate) {
        errorMsg = "From date must be earlier than To date!";
    }

    return errorMsg;
}

function validateDate(date) {
    var errorMsg = "";

    if (date < new Date()) {
        errorMsg = "Incorrect date!";
    }

    return errorMsg;
}