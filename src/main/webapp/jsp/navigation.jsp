<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="utf-8"%>
<%@ taglib prefix="ctg" uri="customtags" %>
<html>
<head>
    <title>Navigation</title>
</head>
<body>
<nav>
    <ul class="menu">
        <div class="menu-header">
            <ctg:sign-in-button/>
        </div>
        <li class="menu-item">
            <a href="/EpamHostel">
                <fmt:message key="label.link.home"/>
            </a>
        </li>
        <c:if test="${sessionScope.get('role') != 'GUEST'}">
            <li class="menu-item">
                <a href="/EpamHostel?command=to_bookings">
                    <fmt:message key="label.title.bookings"/>
                </a>
            </li>
        </c:if>
        <c:if test="${sessionScope.get('role') == 'ADMIN'}">
            <li>
                <a href="/EpamHostel?command=to_rooms">
                    <fmt:message key="label.title.rooms"/>
                </a>
            </li>
            <li>
                <a href="/EpamHostel?command=to_users">
                    <fmt:message key="label.title.users"/>
                </a>
            </li>
        </c:if>
        <li class="menu-item">
            <a href="/EpamHostel?command=to_comments">
                <fmt:message key="label.title.comments"/>
            </a>
        </li>
    </ul>
</nav>
</body>
</html>
