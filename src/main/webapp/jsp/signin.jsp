<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="utf-8"%>
<fmt:setLocale value="${sessionScope.get('locale')}"/>
<fmt:setBundle basename="properties.text"/>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="theme-color" content="#3F51B5">
    <title>EPAM hostel sign in</title>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/theme.css">
    <script src="../js/jquery.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>
</head>
<body>
<header>
    <%@ include file="header.jsp"%>
</header>
<%@ include file="navigation.jsp"%>
<main>
    <div class="container-fluid">
        <div class="row">
            <article class="col-md-4 col-md-offset-4 col-sm-10 col-sm-offset-1 col-xs-10 col-xs-offset-1">
                <div class="card">
                    <div class="card-header">
                        <p><fmt:message key="label.button.sign.in"/></p>
                        <p><fmt:message key="label.title.register.or"/> <a href="/EpamHostel?command=to_register"> <fmt:message key="label.title.register"/></a></p>
                        <p>* - <fmt:message key="label.title.required" /></p>
                    </div>
                    <div class="card-content clearfix">
                        <form id="signin-form" action="EpamHostel" method="post">
                            <div id="errorEmailMsg" class="error-message">
                                ${errorEmailMsg}
                            </div>
                            <label id="emailLabel">
                                <fmt:message key="label.title.register.email"/> *
                            </label>
                            <input id="email" type="email" name="email" required>

                            <div id="errorPasswordMsg" class="error-message">
                                ${errorPasswordMsg}
                            </div>
                            <label id="passwordLabel">
                                <fmt:message key="label.title.register.password"/> *
                            </label>
                            <input id="password" type="password" name="password" required>
                            <input type="hidden" name="command" value="signin">
                            <hr/>
                            <div class="links">
                                <button type="submit" class="btn-link">
                                    <fmt:message key="label.button.sign.in"/>
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </article>
        </div>
    </div>
</main>
<script src="../js/behavior.js"></script>
<script src="../js/validate.js"></script>
</body>
</html>
