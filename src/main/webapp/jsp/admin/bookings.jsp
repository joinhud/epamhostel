<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="utf-8"%>
<fmt:setLocale value="${sessionScope.get('locale')}"/>
<fmt:setBundle basename="properties.text"/>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="theme-color" content="#3F51B5">
    <title>EPAM Hostel Admin</title>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="../../css/bootstrap.min.css">
    <link rel="stylesheet" href="../../css/theme.css">
</head>
<body>
<header>
    <%@ include file="../header.jsp"%>
</header>
<%@ include file="../navigation.jsp"%>
<main>
    <div class="container-fluid">
        <article>
            <section>
                <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                        <h2><fmt:message key="label.title.bookings"/></h2>
                        <div class="card">
                            <div class="card-content clearfix">
                                <%-- Table bookings --%>
                                    <div class="panel panel-primary">
                                        <div class="panel-heading">
                                            <div class="dropdown">
                                                <button class="btn btn-info dropdown-toggle" type="button" data-toggle="dropdown">
                                                    <c:choose>
                                                        <c:when test="${empty bookingsType}">
                                                            ALL BOOKINGS
                                                        </c:when>
                                                        <c:otherwise>
                                                            ${bookingsType}
                                                        </c:otherwise>
                                                    </c:choose>
                                                    <span class="caret"></span>
                                                </button>
                                                <ul class="dropdown-menu">
                                                    <li>
                                                        <a href="/EpamHostel?command=to_bookings">
                                                            <fmt:message key="label.title.all.bookings"/>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="/EpamHostel?command=to_bookings&type=processing">
                                                            <fmt:message key="label.title.state.processing"/>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="/EpamHostel?command=to_bookings&type=denied">
                                                            <fmt:message key="label.title.state.denied"/>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="/EpamHostel?command=to_bookings&type=accepted">
                                                            <fmt:message key="label.title.state.accepted"/>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="panel-body">
                                            <table class="table table-condensed">
                                                <thead>
                                                <tr>
                                                    <th><fmt:message key="label.title.room"/></th>
                                                    <th><fmt:message key="label.title.user"/></th>
                                                    <th><fmt:message key="label.title.bookdate"/></th>
                                                    <th><fmt:message key="label.title.book.numofliv"/></th>
                                                    <th><fmt:message key="label.title.date.from"/></th>
                                                    <th><fmt:message key="label.title.date.to"/></th>
                                                    <th><fmt:message key="label.title.book.paid"/></th>
                                                    <th><fmt:message key="label.title.totalcost"/>, $</th>
                                                    <th><fmt:message key="label.title.state"/></th>
                                                    <th colspan="2"><fmt:message key="label.title.action"/></th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <c:forEach var="booking" items="${bookings}">
                                                    <tr>
                                                        <td>
                                                            ${booking.key.roomNumber}
                                                        </td>
                                                        <td>
                                                            <a href="/EpamHostel?command=view_user&id=${booking.key.userId}">
                                                                    ${booking.value}
                                                            </a>
                                                        </td>
                                                        <td>${booking.key.bookingDate}</td>
                                                        <td>${booking.key.numberOfLiving}</td>
                                                        <td>${booking.key.fromDate}</td>
                                                        <td>${booking.key.toDate}</td>
                                                        <td>
                                                            <c:choose>
                                                                <c:when test="${booking.key.paid == 'true'}">
                                                                    <i class="material-icons md-dark md-18">done</i>
                                                                </c:when>
                                                                <c:otherwise>
                                                                    <i class="material-icons md-dark md-18">clear</i>
                                                                </c:otherwise>
                                                            </c:choose>
                                                        </td>
                                                        <td>${booking.key.totalCost}</td>
                                                        <td>${booking.key.state}</td>
                                                        <c:if test="${booking.key.state != 'PROCESSING'}">
                                                            <td>
                                                                <form action="EpamHostel" method="post">
                                                                    <input type="hidden" name="command" value="change_booking_state">
                                                                    <input type="hidden" name="state" value="PROCESSING">
                                                                    <input type="hidden" name="bookingsType" value="${bookingsType}">
                                                                    <input type="hidden" name="currPage" value="${currPage}">
                                                                    <input type="hidden" name="bookingId" value="${booking.key.id}">
                                                                    <button type="submit" class="btn btn-warning btn-sm">
                                                                        <fmt:message key="label.title.state.processing"/>
                                                                    </button>
                                                                </form>
                                                            </td>
                                                        </c:if>
                                                        <c:if test="${booking.key.state != 'ACCEPTED'}">
                                                            <td>
                                                                <form action="EpamHostel" method="post">
                                                                    <input type="hidden" name="command" value="change_booking_state">
                                                                    <input type="hidden" name="state" value="ACCEPTED">
                                                                    <input type="hidden" name="bookingsType" value="${bookingsType}">
                                                                    <input type="hidden" name="currPage" value="${currPage}">
                                                                    <input type="hidden" name="bookingId" value="${booking.key.id}">
                                                                    <button type="submit" class="btn btn-success btn-sm">
                                                                        <fmt:message key="label.title.accept"/>
                                                                    </button>
                                                                </form>
                                                            </td>
                                                        </c:if>
                                                        <c:if test="${booking.key.state != 'DENIED'}">
                                                            <td>
                                                                <form action="EpamHostel" method="post">
                                                                    <input type="hidden" name="command" value="change_booking_state">
                                                                    <input type="hidden" name="state" value="DENIED">
                                                                    <input type="hidden" name="bookingsType" value="${bookingsType}">
                                                                    <input type="hidden" name="currPage" value="${currPage}">
                                                                    <input type="hidden" name="bookingId" value="${booking.key.id}">
                                                                    <button type="submit" class="btn btn-danger btn-sm">
                                                                        <fmt:message key="label.title.deny"/>
                                                                    </button>
                                                                </form>
                                                            </td>
                                                        </c:if>
                                                    </tr>
                                                </c:forEach>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="panel-footer">
                                            <ul class="pagination">
                                                <li>
                                                    <c:if test="${currPage != 0}">
                                                        <a href="/EpamHostel?command=to_bookings&pageIndex=${currPage - 1}&type=${bookingsType}" aria-label="Previous">
                                                            <span aria-hidden="true">&laquo;</span>
                                                        </a>
                                                    </c:if>
                                                </li>
                                                <li class="active">
                                                    <a href="#">
                                                        <fmt:message key="label.title.page"/> ${currPage + 1} <fmt:message key="label.title.text.from"/> ${pagesCount} <span class="sr-only">(current)</span>
                                                    </a>
                                                </li>
                                                <li>
                                                    <c:if test="${currPage + 1 != pagesCount}">
                                                        <a href="/EpamHostel?command=to_bookings&pageIndex=${currPage + 1}&type=${bookingsType}" aria-label="Next">
                                                            <span aria-hidden="true">&raquo;</span>
                                                        </a>
                                                    </c:if>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                <%-- end Table bookings --%>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </article>
    </div>
</main>
<script src="../../js/jquery.min.js"></script>
<script src="../../js/bootstrap.min.js"></script>
<script src="../../js/behavior.js"></script>
</body>
</html>

