<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="utf-8"%>
<fmt:setLocale value="${sessionScope.get('locale')}"/>
<fmt:setBundle basename="properties.text"/>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="theme-color" content="#3F51B5">
    <title>EPAM Hostel Admin</title>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="../../css/bootstrap.min.css">
    <link rel="stylesheet" href="../../css/theme.css">
</head>
<body>
<header>
    <%@ include file="../header.jsp"%>
</header>
<%@ include file="../navigation.jsp"%>
<main>
    <div class="container-fluid">
        <article>
            <section>
                <div class="row">
                    <div class="col-md-6 col-md-offset-1">
                        <h2><fmt:message key="label.title.user.details"/></h2>
                        <div class="card">
                            <div class="card-content clearfix">
                                <h4><fmt:message key="label.title.register.email"/> : ${user.email}</h4>
                                <p><fmt:message key="label.title.change.first.name"/> : ${user.firstName}</p>
                                <p><fmt:message key="label.title.register.lastname"/> : ${user.lastName}</p>
                                <p><fmt:message key="label.title.discount"/> : ${user.discount} %</p>
                                <hr/>
                                <div class="panel panel-primary">
                                    <div class="panel-heading">
                                        <p><fmt:message key="label.title.discount"/></p>
                                        <p>* - <fmt:message key="label.title.required"/></p>
                                    </div>
                                    <div class="panel-body">
                                        <form action="EpamHostel" method="post">
                                            <div>
                                                ${errorDiscountMsg}
                                            </div>
                                            <label>
                                                <fmt:message key="label.title.discount.value"/> *
                                            </label>
                                            <input type="number" step="any" min="1" max="100" name="newDiscount" required>
                                            <input type="hidden" name="userId" value="${user.id}">
                                            <input type="hidden" name="command" value="change_discount">
                                            <hr/>
                                            <button type="submit" class="btn btn-primary">
                                                <fmt:message key="label.title.set.new.discount"/>
                                            </button>
                                        </form>
                                    </div>
                                </div>
                                <hr/>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <p><fmt:message key="label.title.ban"/></p>
                                        <p>* - <fmt:message key="label.title.required"/></p>
                                    </div>
                                    <div class="panel-body">
                                        <form action="EpamHostel" method="post">
                                            <div class="error-message">
                                                ${errorBanMsg}
                                            </div>
                                            <label>
                                                <fmt:message key="label.title.from.date"/> *
                                            </label>
                                            <input type="date" name="fromDate" required>

                                            <label>
                                                <fmt:message key="label.title.to.date"/> *
                                            </label>
                                            <input type="date" name="toDate" required>

                                            <input type="hidden" name="command" value="set_ban">
                                            <input type="hidden" name="userId" value="${user.id}">
                                            <hr/>
                                            <button type="submit" class="btn btn-primary">
                                                <fmt:message key="label.title.set.ban"/>
                                            </button>
                                        </form>
                                    </div>
                                </div>
                                <hr/>
                                <h4>Blocking Story</h4>
                                    <c:forEach var="blocking" items="${userBlockings}">
                                        <div class="panel panel-default">
                                            <div class="panel-heading"><fmt:message key="label.title.set.ban"/></div>
                                            <div class="panel-body">
                                                <p><fmt:message key="label.title.book.from"/>: ${blocking.fromDate}</p>
                                                <p><fmt:message key="label.title.book.to"/>: ${blocking.toDate}</p>
                                            </div>
                                        </div>
                                    </c:forEach>
                                <hr/>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="card">
                            <div class="card-content clearfix">
                                <h3><fmt:message key="label.header.user.bookings"/></h3>

                                <c:forEach var="booking" items="${userBookings}">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">Booking</div>
                                        <div class="panel-body">
                                            <p><fmt:message key="label.title.book.roomnum"/> ${booking.roomNumber}</p>
                                            <p><fmt:message key="label.title.bookdate"/>: ${booking.bookingDate}</p>
                                            <p><fmt:message key="label.title.book.numofliv"/>: ${booking.numberOfLiving}</p>
                                            <p class="inline"><fmt:message key="label.title.book.from"/>: ${booking.fromDate}</p>
                                            <p class="inline"><fmt:message key="label.title.book.to"/>: ${booking.toDate}</p>
                                            <p>
                                                <fmt:message key="label.title.book.paid"/>:
                                                <c:choose>
                                                    <c:when test="${booking.paid == 'true'}">
                                                        <i class="material-icons md-dark md-18">done</i>
                                                    </c:when>
                                                    <c:otherwise>
                                                        <i class="material-icons md-dark md-18">clear</i>
                                                    </c:otherwise>
                                                </c:choose>
                                            </p>
                                            <p><fmt:message key="label.title.totalcost"/>: ${booking.totalCost} $</p>
                                            <p><fmt:message key="label.title.state"/>: ${booking.state}</p>
                                        </div>
                                    </div>
                                </c:forEach>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </article>
    </div>
</main>
<script src="../../js/jquery.min.js"></script>
<script src="../../js/bootstrap.min.js"></script>
<script src="../../js/behavior.js"></script>
<script src="../../js/validate-profile.js"></script>
</body>
</html>
