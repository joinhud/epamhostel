<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="utf-8"%>
<fmt:setLocale value="${sessionScope.get('locale')}"/>
<fmt:setBundle basename="properties.text"/>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="theme-color" content="#3F51B5">
    <title>EPAM Hostel Admin</title>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="../../css/bootstrap.min.css">
    <link rel="stylesheet" href="../../css/theme.css">
</head>
<body>
<header>
    <%@ include file="../header.jsp"%>
</header>
<%@ include file="../navigation.jsp"%>
<main>
    <div class="container-fluid">
        <article>
            <section>
                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        <div class="card">
                            <div class="card-header">
                                <h3><fmt:message key="label.title.upload"/> ${room.id}</h3>
                            </div>
                            <div class="card-content clearfix">
                                <div>
                                    <form action="EpamHostel" method="post" enctype="multipart/form-data">
                                        <div class="error-message">
                                            ${errorUploadMsg}
                                        </div>
                                        <label>
                                            <fmt:message key="label.title.curr.image"/>: ${room.imageUrl}
                                        </label>
                                        <input type="file" name="image" accept=".jpg">
                                        <input type="hidden" name="command" value="upload_img">
                                        <input type="hidden" name="roomId" value="${room.id}">
                                        <hr/>
                                        <div class="links">
                                            <button type="submit" class="btn-link">
                                                <fmt:message key="label.title.download.new.image"/>
                                            </button>
                                            <a href="/EpamHostel?command=to_room&id=${room.id}" class="btn-link-default ripple">
                                                <fmt:message key="label.title.cancel"/>
                                            </a>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </article>
    </div>
</main>
<script src="../../js/jquery.min.js"></script>
<script src="../../js/bootstrap.min.js"></script>
<script src="../../js/behavior.js"></script>
</body>
</html>
