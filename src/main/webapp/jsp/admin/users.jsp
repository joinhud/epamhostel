<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="utf-8"%>
<fmt:setLocale value="${sessionScope.get('locale')}"/>
<fmt:setBundle basename="properties.text"/>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="theme-color" content="#3F51B5">
    <title>EPAM Hostel Admin</title>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="../../css/bootstrap.min.css">
    <link rel="stylesheet" href="../../css/theme.css">
</head>
<body>
<header>
    <%@ include file="../header.jsp"%>
</header>
<%@ include file="../navigation.jsp"%>
<main>
    <div class="container-fluid">
        <article>
            <section>
                <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                        <h2>Comments</h2>
                        <div class="card">
                            <div class="card-content clearfix">
                                <%-- Table users --%>
                                <div class="panel panel-primary">
                                    <div class="panel-heading">
                                        <div class="dropdown">
                                            <button class="btn btn-info dropdown-toggle" type="button" data-toggle="dropdown">
                                                <c:choose>
                                                    <c:when test="${empty usersType}">
                                                        ALL USERS
                                                    </c:when>
                                                    <c:otherwise>
                                                        ${usersType}
                                                    </c:otherwise>
                                                </c:choose>
                                                <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu">
                                                <li>
                                                    <a href="/EpamHostel?command=to_users">
                                                        <fmt:message key="label.title.all.users"/>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="/EpamHostel?command=to_users&type=baned">
                                                        <fmt:message key="label.title.baned.users"/>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="panel-body">
                                        <table class="table table-condensed">
                                            <thead>
                                            <tr>
                                                <th><fmt:message key="label.title.register.email"/></th>
                                                <th><fmt:message key="label.title.register.firstname"/></th>
                                                <th><fmt:message key="label.title.register.lastname"/></th>
                                                <th><fmt:message key="label.title.discount"/>, %</th>
                                                <th colspan="2"><fmt:message key="label.title.action"/></th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <c:forEach var="user" items="${users}">
                                                <tr>
                                                    <td>
                                                        ${user.email}
                                                    </td>
                                                    <td>
                                                        ${user.firstName}
                                                    </td>
                                                    <td>
                                                        ${user.lastName}
                                                    </td>
                                                    <td>
                                                        ${user.discount}
                                                    </td>
                                                    <td>
                                                        <a href="/EpamHostel?command=view_user&id=${user.id}" class="btn btn-info btn-sm">
                                                            <fmt:message key="label.title.details"/>
                                                        </a>
                                                    </td>
                                                </tr>
                                            </c:forEach>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="panel-footer">
                                        <ul class="pagination">
                                            <li>
                                                <c:if test="${currPage != 0}">
                                                    <a href="/EpamHostel?command=to_users&pageIndex=${currPage - 1}&type=${usersType}" aria-label="Previous">
                                                        <span aria-hidden="true">&laquo;</span>
                                                    </a>
                                                </c:if>
                                            </li>
                                            <li class="active">
                                                <a href="#">
                                                    <fmt:message key="label.title.page"/> ${currPage + 1} <fmt:message key="label.title.text.from"/> ${pagesCount} <span class="sr-only">(current)</span>
                                                </a>
                                            </li>
                                            <li>
                                                <c:if test="${currPage + 1 != pagesCount}">
                                                    <a href="/EpamHostel?command=to_users&pageIndex=${currPage + 1}&type=${usersType}" aria-label="Next">
                                                        <span aria-hidden="true">&raquo;</span>
                                                    </a>
                                                </c:if>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <%-- end Table users --%>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </article>
    </div>
</main>
<script src="../../js/jquery.min.js"></script>
<script src="../../js/bootstrap.min.js"></script>
<script src="../../js/behavior.js"></script>
</body>
</html>
