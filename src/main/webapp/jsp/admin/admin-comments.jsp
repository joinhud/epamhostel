<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="utf-8"%>
<fmt:setLocale value="${sessionScope.get('locale')}"/>
<fmt:setBundle basename="properties.text"/>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="theme-color" content="#3F51B5">
    <title>EPAM Hostel Admin</title>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="../../css/bootstrap.min.css">
    <link rel="stylesheet" href="../../css/theme.css">
</head>
<body>
<header>
    <%@ include file="../header.jsp"%>
</header>
<%@ include file="../navigation.jsp"%>
<main>
    <div class="container-fluid">
        <article>
            <section>
                <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                        <h2><fmt:message key="label.title.comments"/></h2>
                        <div class="card">
                            <div class="card-content clearfix">
                                <%-- Table comments --%>
                                <div class="panel panel-primary">
                                    <div class="panel-heading">
                                        <div class="dropdown">
                                            <button class="btn btn-info dropdown-toggle" type="button" data-toggle="dropdown">
                                                <c:choose>
                                                    <c:when test="${empty commentsType}">
                                                        ALL COMMENTS
                                                    </c:when>
                                                    <c:otherwise>
                                                        ${commentsType}
                                                    </c:otherwise>
                                                </c:choose>
                                                <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu">
                                                <li>
                                                    <a href="/EpamHostel?command=to_comments">
                                                        <fmt:message key="label.title.comment.all"/>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="/EpamHostel?command=to_comments&type=deleted">
                                                        <fmt:message key="label.title.comment.state.deleted"/>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="/EpamHostel?command=to_comments&type=notDeleted">
                                                        <fmt:message key="label.title.not.deleted"/>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="/EpamHostel?command=to_comments&type=today">
                                                        <fmt:message key="label.title.today.comments"/>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="panel-body">
                                        <table class="table table-condensed">
                                            <thead>
                                            <tr>
                                                <th><fmt:message key="label.title.user"/></th>
                                                <th><fmt:message key="label.title.comment.date"/></th>
                                                <th><fmt:message key="label.title.comment.comment"/></th>
                                                <th><fmt:message key="label.title.comment.state.deleted"/></th>
                                                <th><fmt:message key="label.title.action"/></th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <c:forEach var="comment" items="${comments}">
                                                <tr>
                                                    <td>
                                                        <a href="/EpamHostel?command=view_user&id=${comment.key.userId}">
                                                                ${comment.value}
                                                        </a>
                                                    </td>
                                                    <td>
                                                        ${comment.key.date}
                                                    </td>
                                                    <td>${comment.key.comment}</td>
                                                    <td>
                                                        <c:choose>
                                                            <c:when test="${comment.key.deleted == 'true'}">
                                                                <i class="material-icons md-dark md-18">done</i>
                                                            </c:when>
                                                            <c:otherwise>
                                                                <i class="material-icons md-dark md-18">clear</i>
                                                            </c:otherwise>
                                                        </c:choose>
                                                    </td>
                                                    <td>
                                                        <c:choose>
                                                            <c:when test="${comment.key.deleted == 'true'}">
                                                                <form action="EpamHostel" method="post">
                                                                    <input type="hidden" name="command" value="change_deleted_comment">
                                                                    <input type="hidden" name="deleted" value="false">
                                                                    <input type="hidden" name="commentsType" value="${commentsType}">
                                                                    <input type="hidden" name="currPage" value="${currPage}">
                                                                    <input type="hidden" name="commentId" value="${comment.key.id}">
                                                                    <button type="submit" class="btn btn-success btn-sm">
                                                                        <fmt:message key="label.title.restore"/>
                                                                    </button>
                                                                </form>
                                                            </c:when>
                                                            <c:otherwise>
                                                                <form action="EpamHostel" method="post">
                                                                    <input type="hidden" name="command" value="change_deleted_comment">
                                                                    <input type="hidden" name="deleted" value="true">
                                                                    <input type="hidden" name="commentsType" value="${commentsType}">
                                                                    <input type="hidden" name="currPage" value="${currPage}">
                                                                    <input type="hidden" name="commentId" value="${comment.key.id}">
                                                                    <button type="submit" class="btn btn-danger btn-sm">
                                                                        <fmt:message key="label.title.delete"/>
                                                                    </button>
                                                                </form>
                                                            </c:otherwise>
                                                        </c:choose>
                                                    </td>
                                                </tr>
                                            </c:forEach>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="panel-footer">
                                        <ul class="pagination">
                                            <li>
                                                <c:if test="${currPage != 0}">
                                                    <a href="/EpamHostel?command=to_comments&pageIndex=${currPage - 1}&type=${bookingsType}" aria-label="Previous">
                                                        <span aria-hidden="true">&laquo;</span>
                                                    </a>
                                                </c:if>
                                            </li>
                                            <li class="active">
                                                <a href="#">
                                                    <fmt:message key="label.title.page"/> ${currPage + 1} <fmt:message key="label.title.text.from"/> ${pagesCount} <span class="sr-only">(current)</span>
                                                </a>
                                            </li>
                                            <li>
                                                <c:if test="${currPage + 1 != pagesCount}">
                                                    <a href="/EpamHostel?command=to_comments&pageIndex=${currPage + 1}&type=${bookingsType}" aria-label="Next">
                                                        <span aria-hidden="true">&raquo;</span>
                                                    </a>
                                                </c:if>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <%-- end Table commetns --%>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </article>
    </div>
</main>
<script src="../../js/jquery.min.js"></script>
<script src="../../js/bootstrap.min.js"></script>
<script src="../../js/behavior.js"></script>
</body>
</html>