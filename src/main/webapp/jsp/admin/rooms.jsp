<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="utf-8"%>
<fmt:setLocale value="${sessionScope.get('locale')}"/>
<fmt:setBundle basename="properties.text"/>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="theme-color" content="#3F51B5">
    <title>EPAM Hostel Admin</title>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="../../css/bootstrap.min.css">
    <link rel="stylesheet" href="../../css/theme.css">
</head>
<body>
<header>
    <%@ include file="../header.jsp"%>
</header>
<%@ include file="../navigation.jsp"%>
<main>
    <div class="container-fluid">
        <article>
            <section>
                <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                        <h2>Rooms</h2>
                        <div class="card">
                            <div class="card-content clearfix">
                                <a href="/EpamHostel?command=to_add_room" class="btn-link">
                                    <fmt:message key="label.title.add.room"/>
                                </a>
                                <hr/>
                                <%-- Table rooms --%>
                                <div class="panel panel-primary">
                                    <div class="panel-heading">
                                        <fmt:message key="label.title.rooms"/>
                                    </div>
                                    <div class="panel-body">
                                        <table class="table table-hover">
                                            <thead>
                                                <tr>
                                                    <th><fmt:message key="label.title.book.roomnum"/></th>
                                                    <th><fmt:message key="label.title.image.url"/></th>
                                                    <th><fmt:message key="label.title.max.livings"/></th>
                                                    <th><fmt:message key="label.title.type"/></th>
                                                    <th><fmt:message key="label.title.cost"/> , $</th>
                                                    <th><fmt:message key="label.title.on.repair"/></th>
                                                    <th><fmt:message key="label.title.grade"/></th>
                                                    <th><fmt:message key="label.title.description"/></th>
                                                    <th colspan="2"><fmt:message key="label.title.action"/></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            <c:forEach var="room" items="${rooms}">
                                                <tr>
                                                    <td>${room.id}</td>
                                                    <td>${room.imageUrl}</td>
                                                    <td>${room.maxLivings}</td>
                                                    <td>
                                                        <c:choose>
                                                            <c:when test="${room.type == 'false'}">
                                                                <fmt:message key="label.title.type.shared"/>
                                                            </c:when>
                                                            <c:otherwise>
                                                                <fmt:message key="label.title.type.private"/>
                                                            </c:otherwise>
                                                        </c:choose>
                                                    </td>
                                                    <td>${room.cost}</td>
                                                    <td>
                                                        <c:choose>
                                                            <c:when test="${room.onRepair == 'true'}">
                                                                <i class="material-icons md-dark md-18">done</i>
                                                            </c:when>
                                                            <c:otherwise>
                                                                <i class="material-icons md-dark md-18">clear</i>
                                                            </c:otherwise>
                                                        </c:choose>
                                                    </td>
                                                    <td>
                                                        <c:choose>
                                                            <c:when test="${empty room.grade}">
                                                                <fmt:message key="label.title.none"/>
                                                            </c:when>
                                                            <c:otherwise>
                                                                ${room.grade}
                                                            </c:otherwise>
                                                        </c:choose>
                                                    </td>
                                                    <td>
                                                        <c:choose>
                                                            <c:when test="${empty room.description}">
                                                                <fmt:message key="label.title.none"/>
                                                            </c:when>
                                                            <c:otherwise>
                                                                ${room.description}
                                                            </c:otherwise>
                                                        </c:choose>
                                                    </td>
                                                    <td>
                                                        <form action="EpamHostel" method="get">
                                                            <input type="hidden" name="command" value="to_room">
                                                            <input type="hidden" name="id" value="${room.id}">
                                                            <button type="submit" class="btn btn-success btn-sm">
                                                                <fmt:message key="label.title.edit"/>
                                                            </button>
                                                        </form>
                                                    </td>
                                                    <td>
                                                        <form action="EpamHostel" method="post">
                                                            <input type="hidden" name="command" value="delete_room">
                                                            <input type="hidden" name="id" value="${room.id}">
                                                            <button type="submit" class="btn btn-danger btn-sm">
                                                                <fmt:message key="label.title.delete"/>
                                                            </button>
                                                        </form>
                                                    </td>
                                                </tr>
                                            </c:forEach>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <%-- end Table rooms --%>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </article>
    </div>
</main>
<script src="../../js/jquery.min.js"></script>
<script src="../../js/bootstrap.min.js"></script>
<script src="../../js/behavior.js"></script>
</body>
</html>
