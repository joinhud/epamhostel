<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="utf-8"%>
<fmt:setLocale value="${sessionScope.get('locale')}"/>
<fmt:setBundle basename="properties.text"/>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="theme-color" content="#3F51B5">
    <title>EPAM Hostel Admin</title>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="../../css/bootstrap.min.css">
    <link rel="stylesheet" href="../../css/theme.css">
</head>
<body>
<header>
    <%@ include file="../header.jsp"%>
</header>
<%@ include file="../navigation.jsp"%>
<main>
    <div class="container-fluid">
        <article>
            <section>
                <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                        <div class="card">
                            <div class="card-content clearfix">
                                <%-- Table bookings --%>
                                <div class="panel panel-primary">
                                    <div class="panel-heading">
                                        <fmt:message key="label.title.today.bookings"/>
                                    </div>
                                    <div class="panel-body">
                                        <table class="table table-condensed">
                                            <thead>
                                                <tr>
                                                    <th><fmt:message key="label.title.room"/></th>
                                                    <th><fmt:message key="label.title.user"/></th>
                                                    <th><fmt:message key="label.title.bookdate"/></th>
                                                    <th><fmt:message key="label.title.book.numofliv"/></th>
                                                    <th><fmt:message key="label.title.date.from"/></th>
                                                    <th><fmt:message key="label.title.date.to"/></th>
                                                    <th><fmt:message key="label.title.book.paid"/></th>
                                                    <th><fmt:message key="label.title.totalcost"/>, $</th>
                                                    <th><fmt:message key="label.title.state"/></th>
                                                    <th colspan="2"><fmt:message key="label.title.action"/></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <c:forEach var="booking" items="${todayBookings}">
                                                    <tr>
                                                        <td>
                                                            ${booking.key.roomNumber}
                                                        </td>
                                                        <td>
                                                            <a href="/EpamHostel?command=view_user&id=${booking.key.userId}">
                                                                ${booking.value}
                                                            </a>
                                                        </td>
                                                        <td>${booking.key.bookingDate}</td>
                                                        <td>${booking.key.numberOfLiving}</td>
                                                        <td>${booking.key.fromDate}</td>
                                                        <td>${booking.key.toDate}</td>
                                                        <td>
                                                            <c:choose>
                                                                <c:when test="${booking.key.paid == 'true'}">
                                                                    <i class="material-icons md-dark md-18">done</i>
                                                                </c:when>
                                                                <c:otherwise>
                                                                    <i class="material-icons md-dark md-18">clear</i>
                                                                </c:otherwise>
                                                            </c:choose>
                                                        </td>
                                                        <td>${booking.key.totalCost}</td>
                                                        <td>${booking.key.state}</td>
                                                        <td>
                                                            <form action="EpamHostel" method="post">
                                                                <input type="hidden" name="command" value="change_booking_state">
                                                                <input type="hidden" name="state" value="accepted">
                                                                <input type="hidden" name="bookingId" value="${booking.key.id}">
                                                                <button type="submit" class="btn btn-success btn-sm">
                                                                    <fmt:message key="label.title.accept"/>
                                                                </button>
                                                            </form>
                                                        </td>
                                                        <td>
                                                            <form action="EpamHostel" method="post">
                                                                <input type="hidden" name="command" value="change_booking_state">
                                                                <input type="hidden" name="state" value="denied">
                                                                <input type="hidden" name="bookingId" value="${booking.key.id}">
                                                                <button type="submit" class="btn btn-danger btn-sm">
                                                                    <fmt:message key="label.title.deny"/>
                                                                </button>
                                                            </form>
                                                        </td>
                                                    </tr>
                                                </c:forEach>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <%--------------------%>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </article>
    </div>
</main>
<script src="../../js/jquery.min.js"></script>
<script src="../../js/bootstrap.min.js"></script>
<script src="../../js/behavior.js"></script>
</body>
</html>