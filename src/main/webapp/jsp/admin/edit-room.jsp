<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="utf-8"%>
<fmt:setLocale value="${sessionScope.get('locale')}"/>
<fmt:setBundle basename="properties.text"/>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="theme-color" content="#3F51B5">
    <title>EPAM Hostel Admin</title>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="../../css/bootstrap.min.css">
    <link rel="stylesheet" href="../../css/theme.css">
</head>
<body>
<header>
    <%@ include file="../header.jsp"%>
</header>
<%@ include file="../navigation.jsp"%>
<main>
    <div class="container-fluid">
        <article>
            <section>
                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        <div class="card">
                            <div class="card-header">
                                <h2><fmt:message key="label.title.edit.room"/> ${room.id}</h2>
                                <p>* - Required fields</p>
                            </div>
                            <div class="card-content clearfix">
                                <form id="edit-room-form" action="EpamHostel" method="post">
                                    <div id="errorMaxLivings">
                                        ${errorMaxLivingsMsg}
                                    </div>
                                    <label>
                                        <fmt:message key="label.title.max.livings"/> *
                                    </label>
                                    <input type="number" name="maxLivings" min="1" value="${room.maxLivings}" required>

                                    <label>
                                        <fmt:message key="label.title.type"/> *
                                    </label>
                                    <select name="type" required>
                                        <option value="false"><fmt:message key="label.title.type.shared"/></option>
                                        <option value="true"><fmt:message key="label.title.type.private"/></option>
                                    </select>

                                    <div id="errorCost">
                                        ${errorCostMsg}
                                    </div>
                                    <label>
                                        <fmt:message key="label.title.cost"/> *
                                    </label>
                                    <input type="number" name="cost" min="0" step="any" value="${room.cost}" required>

                                    <label>
                                        <fmt:message key="label.title.on.repair"/> *
                                    </label>
                                    <select name="onRepair" required>
                                        <option>false</option>
                                        <option>true</option>
                                    </select>

                                    <label>
                                        <fmt:message key="label.title.grade"/>
                                    </label>
                                    <input type="text" name="grade" value="${room.grade}">

                                    <label>
                                        <fmt:message key="label.title.description"/>
                                    </label>
                                    <textarea name="description" form="edit-room-form">${room.description}</textarea>

                                    <%--Hidden inputs--%>
                                    <input type="hidden" name="command" value="edit_room">
                                    <input type="hidden" name="id" value="${room.id}">
                                    <%--end Hidden inputs--%>

                                    <hr/>
                                    <div class="links">
                                        <button type="submit" class="btn-link ripple">
                                            <fmt:message key="label.link.edit.room"/>
                                        </button>
                                        <a href="/EpamHostel?command=to_room&id=${room.id}" class="btn-link-default ripple">
                                            <fmt:message key="label.title.cancel"/>
                                        </a>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </article>
    </div>
</main>
<script src="../../js/jquery.min.js"></script>
<script src="../../js/bootstrap.min.js"></script>
<script src="../../js/behavior.js"></script>
</body>
</html>
