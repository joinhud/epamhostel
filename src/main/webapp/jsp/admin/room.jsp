<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="utf-8"%>
<fmt:setLocale value="${sessionScope.get('locale')}"/>
<fmt:setBundle basename="properties.text"/>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="theme-color" content="#3F51B5">
    <title>EPAM Hostel Admin</title>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="../../css/bootstrap.min.css">
    <link rel="stylesheet" href="../../css/theme.css">
</head>
<body>
<header>
    <%@ include file="../header.jsp"%>
</header>
<%@ include file="../navigation.jsp"%>
<main>
    <div class="container-fluid">
        <article>
            <section>
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <div class="card">
                            <div class="card-content clearfix">
                                <div class="img-eddition">
                                    <img class="img-responsive" src="${room.imageUrl}" alt="Room image">
                                </div>
                                <div>
                                    <h3><fmt:message key="label.title.book.roomnum"/> ${room.id}</h3>
                                    <p><fmt:message key="label.title.max.livings"/>: ${room.maxLivings}</p>
                                    <c:choose>
                                        <c:when test="${room.type == 'false'}">
                                            <p><fmt:message key="label.title.type"/>: <fmt:message key="label.title.type.shared"/></p>
                                        </c:when>
                                        <c:otherwise>
                                            <p><fmt:message key="label.title.type"/>: <fmt:message key="label.title.type.private"/></p>
                                        </c:otherwise>
                                    </c:choose>
                                    <p>Cost: ${room.cost}</p>
                                    <p>
                                        <fmt:message key="label.title.on.repair"/>:
                                        <c:choose>
                                            <c:when test="${room.onRepair == 'true'}">
                                                <i class="material-icons md-dark md-18">done</i>
                                            </c:when>
                                            <c:otherwise>
                                                <i class="material-icons md-dark md-18">clear</i>
                                            </c:otherwise>
                                        </c:choose>
                                    </p>
                                    <c:if test="${not empty room.grade}">
                                        <p><fmt:message key="label.title.grade"/>: ${room.grade}</p>
                                    </c:if>
                                    <c:if test="${not empty room.description}">
                                        <p class="description">
                                            <fmt:message key="label.title.description"/>: ${room.description}
                                        </p>
                                    </c:if>

                                    <div class="links">
                                        <a href="/EpamHostel?command=to_edit_room&id=${room.id}" class="btn-link ripple">
                                            <fmt:message key="label.link.edit.room"/>
                                        </a>
                                        <a href="/EpamHostel?command=to_upload_image&id=${room.id}" class="btn-link-default ripple">
                                            <fmt:message key="label.link.set.new.image"/>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </article>
    </div>
</main>
<script src="../../js/jquery.min.js"></script>
<script src="../../js/bootstrap.min.js"></script>
<script src="../../js/behavior.js"></script>
</body>
</html>
