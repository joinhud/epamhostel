<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="utf-8"%>
<html>
<head>
    <title>Header</title>
</head>
<body>
<div class="header clearfix">
    <div class="nav-icon ripple">
        <i class="material-icons md-light md-24">menu</i>
    </div>
    <div class="toolbar">
        <ul class="clearfix">
            <li>
                <div class="dropdown">
                    <a href="#" class="dropdown-toggle ripple" data-toggle="dropdown">
                        ${sessionScope.get('currLanguage')}
                        <b class="caret"></b>
                    </a>
                    <ul class="dropdown-menu">
                        <li>
                            <form action="EpamHostel" method="post">
                                <input type="hidden" name="locale" value="en_US">
                                <input type="hidden" name="query" value="${pageContext.request.queryString}">
                                <input type="hidden" name="command" value="change_language">
                                <button name="language" type="submit" value="label.button.english">
                                    <fmt:message key="label.button.english"/>
                                </button>
                            </form>
                        </li>
                        <li>
                            <form action="EpamHostel" method="post">
                                <input type="hidden" name="locale" value="ru_RU">
                                <input type="hidden" name="query" value="${pageContext.request.queryString}">
                                <input type="hidden" name="command" value="change_language">
                                <button name="language" type="submit" value="label.button.russian">
                                    <fmt:message key="label.button.russian"/>
                                </button>
                            </form>
                        </li>
                    </ul>
                </div>
            </li>
            <c:if test="${not empty sessionScope.get('user')}">
                <li class="link">
                    <form action="EpamHostel" method="post">
                        <input type="hidden" name="command" value="signout">
                        <button type="submit" class="ripple">
                            <fmt:message key="label.button.sign.out"/>
                        </button>
                    </form>
                </li>
            </c:if>
        </ul>
    </div>
</div>
</body>
</html>
