<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="utf-8"%>
<fmt:setLocale value="${sessionScope.get('locale')}"/>
<fmt:setBundle basename="properties.text"/>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="theme-color" content="#3F51B5">
    <title>EPAM hostel profile</title>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="../../css/bootstrap.min.css">
    <link rel="stylesheet" href="../../css/theme.css">
</head>
<body>
<header>
    <%@ include file="../header.jsp"%>
</header>
<%@ include file="../navigation.jsp"%>
<main>
    <div class="container-fluid">
        <article>
            <section>
                <div class="row">
                    <div class="col-md-offset-1">
                        <h2><fmt:message key="label.title.user.profile"/></h2>
                    </div>
                    <div class="col-md-6 col-md-offset-1">
                        <div class="card">
                            <div class="card-content clearfix">
                                <h4><fmt:message key="label.title.register.email"/> : ${sessionScope.get('user').email}</h4>
                                <p><fmt:message key="label.title.register.firstname"/> : ${sessionScope.get('user').firstName}</p>
                                <p><fmt:message key="label.title.register.lastname"/> : ${sessionScope.get('user').lastName}</p>
                                <p><fmt:message key="label.title.discount"/> : ${sessionScope.get('user').discount} %</p>
                                <hr/>
                                <c:if test="${not empty blockings}">
                                    <h4><fmt:message key="label.title.blocking.story"/></h4>
                                    <c:forEach var="blocking" items="${blockings}">
                                        <p><fmt:message key="label.title.ban"/></p>
                                        <p><fmt:message key="label.title.book.from"/> : ${blocking.fromDate}</p>
                                        <p><fmt:message key="label.title.book.to"/> : ${blocking.toDate}</p>
                                    </c:forEach>
                                    <hr/>
                                </c:if>
                                <h4><fmt:message key="label.title.change.password"/></h4>
                                <p>* - <fmt:message key="label.title.required"/></p>
                                <form name="change-pass-form" action="/EpamHostel" method="post">
                                    <div id="errorOldPass" class="error-message">
                                        ${errorOldPass}
                                    </div>
                                    <label>
                                        <fmt:message key="label.title.password.old"/> *
                                    </label>
                                    <input id="oldPassword" type="password" name="oldPassword" required>

                                    <div id="errorNewPass" class="error-message">
                                        ${errorNewPass}
                                    </div>
                                    <label>
                                        <fmt:message key="label.title.password.new"/> *
                                    </label>
                                    <input id="newPassword" type="password" name="newPassword" required>

                                    <input type="hidden" name="command" value="change_password">
                                    <button type="submit" class="btn-link">
                                        <fmt:message key="label.title.change.password"/>
                                    </button>
                                </form>
                                <c:if test="${sessionScope.get('role') != 'ADMIN'}">
                                    <hr/>
                                    <h4><fmt:message key="label.title.change.first.name"/></h4>
                                    <p>* - <fmt:message key="label.title.required"/></p>
                                    <form name="change-first-name-form" action="/EpamHostel" method="post">
                                        <div id="errorFirstName" class="error-message">
                                                ${errorFirstName}
                                        </div>
                                        <label>
                                            <fmt:message key="label.title.name.first.new"/> *
                                        </label>
                                        <input id="firstName" type="text" name="firstName" required>

                                        <input type="hidden" name="command" value="change_firstname">
                                        <button type="submit" class="btn-link">
                                            <fmt:message key="label.title.change.first.name"/>
                                        </button>
                                    </form>
                                    <hr/>
                                    <h4><fmt:message key="label.title.change.last.name"/></h4>
                                    <p>* - <fmt:message key="label.title.required"/></p>
                                    <form name="change-last-name-form" action="/EpamHostel" method="post">
                                        <div id="errorLastName" class="error-message">
                                                ${errorLastName}
                                        </div>
                                        <label>
                                            <fmt:message key="label.title.name.last.new"/> *
                                        </label>
                                        <input id="lastName" type="text" name="lastName" required>

                                        <input type="hidden" name="command" value="change_lastname">
                                        <button type="submit" class="btn-link">
                                            <fmt:message key="label.title.change.last.name"/>
                                        </button>
                                    </form>
                                </c:if>
                            </div>
                        </div>
                    </div>
                    <c:if test="${sessionScope.get('role') != 'ADMIN'}">
                        <div class="col-md-4">
                            <div class="card">
                                <h4><fmt:message key="label.title.user.accounts"/></h4>
                                <hr/>
                                <a href="/EpamHostel?command=to_add_account" class="btn-link"><fmt:message key="label.title.user.addbankaccount"/></a>
                            </div>
                            <c:forEach var="account" items="${accounts}">
                                <div class="card">
                                    <h4>${account.id}</h4>
                                    <p><fmt:message key="label.title.money"/> : ${account.money} $</p>
                                    <hr/>
                                    <form action="EpamHostel" method="post">
                                        <input type="hidden" name="command" value="delete_account">
                                        <input type="hidden" name="account" value="${account.id}">
                                        <button type="submit" class="btn-link inline">
                                            <fmt:message key="label.title.delete"/>
                                        </button>
                                        <a href="/EpamHostel?command=to_refill_account&id=${account.id}" class="btn-link inline">
                                            <fmt:message key="label.link.refill"/>
                                        </a>
                                    </form>
                                </div>
                            </c:forEach>
                        </div>
                    </c:if>
                </div>
            </section>
        </article>
    </div>
</main>
<script src="../../js/jquery.min.js"></script>
<script src="../../js/bootstrap.min.js"></script>
<script src="../../js/behavior.js"></script>
<script src="../../js/validate-profile.js"></script>
</body>
</html>
