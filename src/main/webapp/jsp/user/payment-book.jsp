<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="utf-8"%>
<fmt:setLocale value="${sessionScope.get('locale')}"/>
<fmt:setBundle basename="properties.text"/>
<html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="theme-color" content="#3F51B5">
    <title>EPAM Hostel payment book</title>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="../../css/bootstrap.min.css">
    <link rel="stylesheet" href="../../css/theme.css">
</head>
<body>
<header>
    <%@ include file="../header.jsp"%>
</header>
<%@ include file="../navigation.jsp"%>
<main>
    <div class="container-fluid">
        <div class="row">
            <article class="col-md-4 col-md-offset-4 col-sm-10 col-sm-offset-1 col-xs-10 col-xs-offset-1">
                <div class="card">
                    <div class="card-header">
                        <p><fmt:message key="label.header.confirm"/></p>
                        <p>* - <fmt:message key="label.title.required"/></p>
                    </div>
                    <div class="card-content clearfix">
                        <div class="error-message">
                            <p id="errorMsg">${errorMsg}</p>
                        </div>
                        <p><fmt:message key="label.title.book.roomnum"/> : ${booking.roomNumber}</p>
                        <p><fmt:message key="label.title.book.numofliv"/> : ${booking.numberOfLiving}</p>
                        <p><fmt:message key="label.title.book.from"/> : ${booking.fromDate}</p>
                        <p><fmt:message key="label.title.book.to"/> : ${booking.toDate}</p>
                        <p><fmt:message key="label.title.totalcost"/> : ${booking.totalCost} $</p>
                        <p><fmt:message key="label.title.accountnumber"/> : ${bankAccounts}</p>
                        <form id="payment-form" action="EpamHostel" method="post">
                            <div id="errorPassword" class="error-message">${errorPassword}</div>
                            <label>
                                <fmt:message key="label.title.enterpfssword"/> *
                            </label>
                            <input id="password" type="password" name="password">

                            <input type="hidden" name="number" value="${booking.roomNumber}">
                            <input type="hidden" name="numberOfLiving" value="${booking.numberOfLiving}">
                            <input type="hidden" name="fromDate" value="${booking.fromDate}">
                            <input type="hidden" name="toDate" value="${booking.toDate}">
                            <input type="hidden" name="totalCost" value="${booking.totalCost}">
                            <input type="hidden" name="bankAccount" value="${bankAccounts}">
                            <input type="hidden" name="command" value="pay_booking">

                            <button type="submit" class="btn-link">
                                <fmt:message key="label.title.pay"/>
                            </button>
                        </form>
                    </div>
                </div>
            </article>
        </div>
    </div>
</main>
<script src="../../js/jquery.min.js"></script>
<script src="../../js/bootstrap.min.js"></script>
<script src="../../js/behavior.js"></script>
<script src="../../js/validate-payment-book.js"></script>
</body>
</html>
