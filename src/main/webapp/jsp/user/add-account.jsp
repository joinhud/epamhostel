<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="utf-8"%>
<fmt:setLocale value="${sessionScope.get('locale')}"/>
<fmt:setBundle basename="properties.text"/>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="theme-color" content="#3F51B5">
    <title>EPAM Hostel Add Bank account</title>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="../../css/bootstrap.min.css">
    <link rel="stylesheet" href="../../css/theme.css">
</head>
<body>
<header>
    <%@ include file="../header.jsp"%>
</header>
<%@ include file="../navigation.jsp"%>
<main>
    <div class="container-fluid">
        <div class="row">
            <article class="col-md-4 col-md-offset-4 col-sm-10 col-sm-offset-1 col-xs-10 col-xs-offset-1">
                <div class="card">
                    <div class="card-header">
                        <p><fmt:message key="label.title.user.addbankaccount"/></p>
                        <p>* - <fmt:message key="label.title.required"/></p>
                    </div>
                    <div class="card-content clearfix">
                        <div class="error-message">
                            <p id="errorMsg">${errorMsg}</p>
                        </div>
                        <form id="add-account-form" action="EpamHostel" method="post">
                            <div id="errorAccount" class="error-message">${errorAccount}</div>
                            <label>
                                <fmt:message key="label.title.accountnumber"/> *
                            </label>
                            <input id="account" type="number" name="account" min="1" minlength="8" maxlength="8" required>

                            <div id="errorMoney" class="error-message">${errorMoney}</div>
                            <label>
                                <fmt:message key="label.title.money"/> *
                            </label>
                            <input id="money" type="number" name="money" min="0" step="any" required>

                            <div id="errorPassword" class="error-message">${errorPassword}</div>
                            <label>
                                <fmt:message key="label.title.register.password"/> *
                            </label>
                            <input id="password" type="password" name="password" required>

                            <div id="errorPassConfirm" class="error-message">${errorPassConfirm}</div>
                            <label>
                                <fmt:message key="label.title.register.confirm"/> *
                            </label>
                            <input id="passConfirm" type="password" name="passConfirm" required>

                            <input type="hidden" name="command" value="add_account">

                            <button type="submit" class="btn-link">
                                <fmt:message key="label.title.add"/>
                            </button>
                        </form>
                    </div>
                </div>
            </article>
        </div>
    </div>
</main>
<script src="../../js/jquery.min.js"></script>
<script src="../../js/bootstrap.min.js"></script>
<script src="../../js/behavior.js"></script>
<script src="../../js/validate-bank-account.js"></script>
</body>
</html>
