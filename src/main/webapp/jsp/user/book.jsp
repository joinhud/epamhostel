<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="utf-8"%>
<fmt:setLocale value="${sessionScope.get('locale')}"/>
<fmt:setBundle basename="properties.text"/>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="theme-color" content="#3F51B5">
    <title>EPAM Hostel book</title>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="../../css/bootstrap.min.css">
    <link rel="stylesheet" href="../../css/theme.css">
</head>
<body>
<header>
    <%@ include file="../header.jsp"%>
</header>
<%@ include file="../navigation.jsp"%>
<main>
    <div class="container-fluid">
        <div class="row">
            <article class="col-md-4 col-md-offset-4 col-sm-10 col-sm-offset-1 col-xs-10 col-xs-offset-1">
                <div class="card">
                    <div class="card-header">
                        <p><fmt:message key="label.title.book.bookroom"/></p>
                        <p>* - <fmt:message key="label.title.required"/></p>
                    </div>
                    <div class="card-content clearfix">
                        <div class="error-message">
                            <p id="errorMsg">${errorMsg}</p>
                        </div>
                        <form id="book-form" action="EpamHostel" method="post">
                            <p>
                                <fmt:message key="label.title.book.roomnum"/> ${roomNumber}
                            </p>
                            <input type="hidden" name="number" value="${roomNumber}">

                            <label for="numberOfLivings">
                                <fmt:message key="label.title.book.numofliv"/> *
                            </label>
                            <input id="numberOfLivings" type="number" name="numberOfLivings" min="1" required>

                            <label for="fromDate">
                                <fmt:message key="label.title.book.from"/> *
                            </label>
                            <input id="fromDate" type="date" name="fromDate" required/>

                            <label for="toDate">
                                <fmt:message key="label.title.book.to"/> *
                            </label>
                            <input id="toDate" type="date" name="toDate" required/>

                            <div class="checkbox">
                                <label class="inline">
                                    <input id="paid" type="checkbox" name="paid" class="inline"/>
                                    <fmt:message key="label.title.book.paid"/>
                                </label>
                            </div>

                            <jsp:useBean id="accounts" scope="request" type="java.util.ArrayList"/>
                            <c:choose>
                                <c:when test="${accounts.size() != 0}">
                                    <div id="collapsed-block" class="closed">
                                        <label>
                                            <fmt:message key="label.title.book.selectaccount"/> *
                                        </label>
                                        <select name="bankAccounts">
                                            <c:forEach var="bankAccount" items="${accounts}">
                                                <option value="${bankAccount.id}">${bankAccount.id}</option>
                                            </c:forEach>
                                        </select>
                                    </div>
                                </c:when>
                                <c:otherwise>
                                    <div id="collapsed-block" class="closed">
                                        <a href="/EpamHostel?command=to_add_account" class="ripple">
                                            <p class="btn-link form-link">+ <fmt:message key="label.link.bankaccountadd"/></p>
                                        </a>
                                    </div>
                                </c:otherwise>
                            </c:choose>

                            <input type="hidden" name="command" value="book">

                            <button type="submit" class="btn-link">
                                <fmt:message key="label.link.book"/>
                            </button>
                        </form>
                    </div>
                </div>
            </article>
        </div>
    </div>
</main>
<script src="../../js/jquery.min.js"></script>
<script src="../../js/bootstrap.min.js"></script>
<script src="../../js/behavior.js"></script>
<script src="../../js/validate-book-form.js"></script>
</body>
</html>
