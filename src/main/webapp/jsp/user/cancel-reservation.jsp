<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="utf-8"%>
<fmt:setLocale value="${sessionScope.get('locale')}"/>
<fmt:setBundle basename="properties.text"/>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="theme-color" content="#3F51B5">
    <title>EPAM Hostel cancel reservation</title>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="../../css/bootstrap.min.css">
    <link rel="stylesheet" href="../../css/theme.css">
</head>
<body>
<header>
    <%@ include file="../header.jsp"%>
</header>
<%@ include file="../navigation.jsp"%>
<main>
    <div class="container-fluid">
        <div class="row">
            <article class="col-md-4 col-md-offset-4 col-sm-10 col-sm-offset-1 col-xs-10 col-xs-offset-1">
                <div class="card">
                    <div class="card-header">
                        <h3><fmt:message key="label.header.cancelreserv"/></h3>
                    </div>
                    <div class="card-content clearfix">
                        <p><fmt:message key="label.title.book.roomnum"/> ${booking.roomNumber}</p>
                        <p><fmt:message key="label.title.bookdate"/>: ${booking.bookingDate}</p>
                        <p><fmt:message key="label.title.book.numofliv"/>: ${booking.numberOfLiving}</p>
                        <p class="inline"><fmt:message key="label.title.book.from"/>: ${booking.fromDate}</p>
                        <p class="inline"><fmt:message key="label.title.book.to"/>: ${booking.toDate}</p>
                        <p>
                            <fmt:message key="label.title.book.paid"/>:
                            <c:choose>
                                <c:when test="${booking.paid == 'true'}">
                                    <i class="material-icons md-dark md-18">done</i>
                                </c:when>
                                <c:otherwise>
                                    <i class="material-icons md-dark md-18">clear</i>
                                </c:otherwise>
                            </c:choose>
                        </p>
                        <p><fmt:message key="label.title.totalcost"/>: ${booking.totalCost} $</p>
                        <p><fmt:message key="label.title.status"/>: ${booking.state}</p>
                        <form id="cancel-reservation-form" action="EpamHostel" method="post">
                            <c:if test="${booking.paid == 'true'}">
                                <hr/>
                                <div class="error-message">
                                    ${errorMsg}
                                </div>
                                <c:choose>
                                    <c:when test="${accounts.size() != 0}">
                                        <label>
                                            <fmt:message key="label.title.choseaccaunt"/>
                                        </label>
                                        <select name="bankAccounts">
                                            <c:forEach var="bankAccount" items="${accounts}">
                                                <option value="${bankAccount.id}">${bankAccount.id}</option>
                                            </c:forEach>
                                        </select>
                                    </c:when>
                                    <c:otherwise>
                                        <div id="collapsed-block" class="closed">
                                            <a href="/EpamHostel?command=to_add_account" class="ripple">
                                                <p class="btn-link form-link">+ <fmt:message key="label.link.bankaccountadd"/></p>
                                            </a>
                                        </div>
                                    </c:otherwise>
                                </c:choose>
                            </c:if>
                            <hr/>
                            <input type="hidden" name="bookingId" value="${booking.id}">
                            <input type="hidden" name="command" value="cancel_reservation">
                            <a href="/EpamHostel?command=to_bookings" class="btn-link-default inline">
                                <fmt:message key="label.link.back"/>
                            </a>
                            <button type="submit" class="btn-link inline">
                                <fmt:message key="label.link.cancelreserv"/>
                            </button>
                        </form>
                    </div>
                </div>
            </article>
        </div>
    </div>
</main>
<script src="../../js/jquery.min.js"></script>
<script src="../../js/bootstrap.min.js"></script>
<script src="../../js/behavior.js"></script>
</body>
</html>
