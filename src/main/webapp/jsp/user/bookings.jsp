<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="utf-8"%>
<fmt:setLocale value="${sessionScope.get('locale')}"/>
<fmt:setBundle basename="properties.text"/>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="theme-color" content="#3F51B5">
    <title>EPAM Hostel bookings</title>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="../../css/bootstrap.min.css">
    <link rel="stylesheet" href="../../css/theme.css">
</head>
<body>
<header>
    <%@ include file="../header.jsp"%>
</header>
<%@ include file="../navigation.jsp"%>
<main>
    <div class="container-fluid">
        <article>
            <section>
                <div class="row">
                    <div class="col-md-offset-2">
                        <h2><fmt:message key="label.title.bookings"/></h2>
                    </div>
                    <c:forEach var="booking" items="${bookings}">
                        <div class="col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
                            <div class="card">
                                <div class="card-content clearfix">
                                    <div class="clearfix">
                                        <p><fmt:message key="label.title.book.roomnum"/> ${booking.roomNumber}</p>
                                        <p><fmt:message key="label.title.bookdate"/>: ${booking.bookingDate}</p>
                                        <p><fmt:message key="label.title.book.numofliv"/>: ${booking.numberOfLiving}</p>
                                        <p class="inline"><fmt:message key="label.title.book.from"/>: ${booking.fromDate}</p>
                                        <p class="inline"><fmt:message key="label.title.book.to"/>: ${booking.toDate}</p>
                                        <p>
                                            <fmt:message key="label.title.book.paid"/>:
                                            <c:choose>
                                                <c:when test="${booking.paid == 'true'}">
                                                    <i class="material-icons md-dark md-18">done</i>
                                                </c:when>
                                                <c:otherwise>
                                                    <i class="material-icons md-dark md-18">clear</i>
                                                </c:otherwise>
                                            </c:choose>
                                        </p>
                                        <p><fmt:message key="label.title.totalcost"/>: ${booking.totalCost} $</p>
                                        <p><fmt:message key="label.title.status"/>: ${booking.state}</p>
                                    </div>
                                    <c:if test="${booking.fromDate.isAfter(now)}">
                                        <div class="links">
                                            <a href="/EpamHostel?command=to_cancel_reservation&id=${booking.id}" class="ripple">
                                                <fmt:message key="label.link.cancelreserv"/>
                                            </a>
                                        </div>
                                    </c:if>
                                </div>
                            </div>
                        </div>
                    </c:forEach>
                </div>
            </section>
        </article>
    </div>
</main>
<script src="../../js/jquery.min.js"></script>
<script src="../../js/bootstrap.min.js"></script>
<script src="../../js/behavior.js"></script>
</body>
</html>
