<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="utf-8"%>
<fmt:setLocale value="${sessionScope.get('locale')}"/>
<fmt:setBundle basename="properties.text"/>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="theme-color" content="#3F51B5">
    <title>EPAM Hostel refill bank account</title>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="../../css/bootstrap.min.css">
    <link rel="stylesheet" href="../../css/theme.css">
</head>
<body>
<header>
    <%@ include file="../header.jsp"%>
</header>
<%@ include file="../navigation.jsp"%>
<main>
    <div class="container-fluid">
        <div class="row">
            <article class="col-md-4 col-md-offset-4 col-sm-10 col-sm-offset-1 col-xs-10 col-xs-offset-1">
                <div class="card">
                    <div class="card-header">
                        <h4><fmt:message key="label.title.refill"/></h4>
                        <p>* - <fmt:message key="label.title.required"/></p>
                    </div>
                    <div class="card-content clearfix">
                        <div class="error-message">
                            ${errorMsg}
                        </div>
                        <form action="EpamHostel" method="post">
                            <p><fmt:message key="label.title.account"/> : ${account.id}</p>
                            <p><fmt:message key="label.title.currbalance"/> : ${account.money} $</p>
                            <label>
                                <fmt:message key="label.title.sumofrefillng"/> *
                            </label>
                            <input id="refilling" type="number" name="refilling" min="0" step="any" required>

                            <input type="hidden" name="command" value="refill_account">
                            <input type="hidden" name="accountNumber" value="${account.id}">

                            <button type="submit" class="btn-link">
                                <fmt:message key="label.link.refill"/>
                            </button>
                        </form>
                    </div>
                </div>
            </article>
        </div>
    </div>
</main>
<script src="../../js/jquery.min.js"></script>
<script src="../../js/bootstrap.min.js"></script>
<script src="../../js/behavior.js"></script>
<script src="../../js/validate-bank-account.js"></script>
</body>
</html>
