<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="utf-8"%>
<fmt:setLocale value="${sessionScope.get('locale')}"/>
<fmt:setBundle basename="properties.text"/>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="theme-color" content="#3F51B5">
    <title>EPAM Hostel comments</title>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/theme.css">
</head>
<body>
<header>
    <%@ include file="header.jsp"%>
</header>
<%@ include file="navigation.jsp"%>
<main>
    <div class="container-fluid">
        <article>
            <section>
                <div class="row">
                    <div class="col-md-offset-2">
                        <h2><fmt:message key="label.title.comments"/></h2>
                    </div>
                    <div class="col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="card-content clearfix">
                                <div class="clearfix">
                                    <h4><fmt:message key="label.title.addcomments" /></h4>
                                    <c:choose>
                                        <c:when test="${not empty sessionScope.get('user')}">
                                            <c:choose>
                                                <c:when test="${baned == 'true'}">
                                                    <p><fmt:message key="label.title.baned"/></p>
                                                </c:when>
                                                <c:otherwise>
                                                    <p>* - <fmt:message key="label.title.required"/></p>
                                                    <form id="comment-form" action="EpamHostel" method="post">
                                                        <div class="error-message">
                                                                ${errorMsgAddComment}
                                                        </div>
                                                        <label>
                                                            <fmt:message key="label.title.yourcomment"/> *
                                                        </label>
                                                        <textarea name="commentText" form="comment-form" required></textarea>

                                                        <input type="hidden" name="command" value="add_comment">
                                                        <button type="submit" class="btn-link">
                                                            <fmt:message key="label.title.addcomments"/>
                                                        </button>
                                                    </form>
                                                </c:otherwise>
                                            </c:choose>
                                        </c:when>
                                        <c:otherwise>
                                            <p><fmt:message key="label.title.comment.signin"/></p>
                                            <a href="/EpamHostel?command=to_signin"><fmt:message key="label.button.sign.in"/></a>
                                        </c:otherwise>
                                    </c:choose>
                                </div>
                            </div>
                        </div>
                        <c:if test="${not empty userComments && not empty sessionScope.get('user')}">
                            <h2><fmt:message key="label.title.usercomment"/></h2>
                            <c:forEach var="userComment" items="${userComments}">
                                <div class="card">
                                    <div class="card-content clearfix">
                                        <div class="clearfix">
                                            <p><fmt:message key="label.title.comment.date"/> : ${userComment.date}</p>
                                            <c:if test="${userComment.deleted == 'true'}">
                                                <h4><fmt:message key="label.title.comment.state.deleted"/></h4>
                                            </c:if>
                                            <p><b><fmt:message key="label.title.comment.comment"/>: </b> ${userComment.comment}</p>
                                            <c:if test="${userComment.deleted == 'false'}">
                                                <form action="EpamHostel" method="post">
                                                    <input type="hidden" name="command" value="delete_comment">
                                                    <input type="hidden" name="id" value="${userComment.id}">
                                                    <button type="submit" class="btn-link">
                                                        <fmt:message key="label.title.delete"/>
                                                    </button>
                                                </form>
                                            </c:if>
                                        </div>
                                    </div>
                                </div>
                            </c:forEach>
                        </c:if>
                        <h2>All comments</h2>
                        <c:forEach var="comment" items="${comments}">
                            <div class="card">
                                <div class="card-content clearfix">
                                    <div class="clearfix">
                                        <h4><fmt:message key="label.title.user"/>: ${comment.value}</h4>
                                        <p><fmt:message key="label.title.comment.date"/>: ${comment.key.date}</p>
                                        <p><b><fmt:message key="label.title.comment.comment"/>: </b> ${comment.key.comment}</p>
                                    </div>
                                </div>
                            </div>
                        </c:forEach>
                    </div>
                </div>
            </section>
        </article>
    </div>
</main>
<script src="../js/jquery.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/behavior.js"></script>
</body>
</html>
