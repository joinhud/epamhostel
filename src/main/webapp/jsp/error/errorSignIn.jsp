<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Error</title>
</head>
<body>
    <main>
        <h1>Error</h1>
        <h3>Sorry, we have an error while validate your data. Please, try Sign In again later.</h3>
        <p>More details: ${error}</p>
        <a href="/EpamHostel">To Home page</a>
    </main>
</body>
</html>
