<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Error</title>
</head>
<body>
    <main>
        <h1>Error</h1>
        <h3>Sorry, we have an error while refilling your bank account. Please, try to refill account again later.</h3>
        <p>More details: ${error}</p>
        <a href="/EpamHostel">To Home page</a>
    </main>
</body>
</html>
