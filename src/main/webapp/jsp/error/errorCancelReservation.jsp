<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Error</title>
</head>
<body>
    <main>
        <h1>Error</h1>
        <h3>Sorry, we have an error while you try to cancel your booking. Please, try to cancel this booking later.</h3>
        <p>More details: ${error}</p>
        <a href="/EpamHostel">To Home page</a>
    </main>
</body>
</html>
