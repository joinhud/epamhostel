<%@ page isErrorPage="true" contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<html>
<head>
    <title>Error page</title>
</head>
<body>
    <div class="content">
        <h1>Error ${pageContext.errorData.statusCode}</h1>
    </div>
</body>
</html>
