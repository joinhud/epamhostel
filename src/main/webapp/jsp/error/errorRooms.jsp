<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Error</title>
</head>
<body>
    <main>
        <h1>Error</h1>
        <h3>Sorry, service is not working now. Please, try again later.</h3>
        <p>More details: ${error}</p>
    </main>
</body>
</html>
