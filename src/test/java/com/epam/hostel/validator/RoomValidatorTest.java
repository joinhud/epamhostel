package com.epam.hostel.validator;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

public class RoomValidatorTest {
    private static RoomValidator validator;

    @BeforeClass
    public static void initValidator() {
        validator = new RoomValidator();
    }

    @Test
    public void validRoomNumberCorrectValueTest() {
        String testValue = "20";
        String expected = "";
        String actual = validator.validRoomNumber(testValue);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void validRoomNumberNullValueTest() {
        String testValue = null;
        String expected = "Incorrect value for room number!";
        String actual = validator.validRoomNumber(testValue);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void validRoomNumberNegativeValueTest() {
        String testValue = "-3";
        String expected = "Incorrect value for room number!";
        String actual = validator.validRoomNumber(testValue);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void validRoomNumberNonDigitValueTest() {
        String testValue = "ha7";
        String expected = "Incorrect value for room number!";
        String actual = validator.validRoomNumber(testValue);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void validMaxLivingsCorrectValueTest() {
        String testValue = "456";
        String expected = "";
        String actual = validator.validMaxLivings(testValue);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void validMaxLivingsNullValueTest() {
        String testValue = null;
        String expected = "Number of max livings must contains only digits!";
        String actual = validator.validMaxLivings(testValue);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void validMaxLivingsNegativeValueTest() {
        String testValue = "-789";
        String expected = "Number of max livings must contains only digits!";
        String actual = validator.validMaxLivings(testValue);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void validMaxLivingsNonDigitValueTest() {
        String testValue = "one";
        String expected = "Number of max livings must contains only digits!";
        String actual = validator.validMaxLivings(testValue);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void validateCostRealNumberTest() {
        String testValue = "123.34";
        String expected = "";
        String actual = validator.validateCost(testValue);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void validateCostNotRealNumberTest() {
        String testValue = "123";
        String expected = "";
        String actual = validator.validateCost(testValue);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void validateCostNullValueTest() {
        String testValue = null;
        String expected = "Incorrect value for cost!";
        String actual = validator.validateCost(testValue);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void validateCostNegativeValueTest() {
        String testValue = "-3";
        String expected = "Incorrect value for cost!";
        String actual = validator.validateCost(testValue);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void validateCostNonDigitValueTest() {
        String testValue = "quadro";
        String expected = "Incorrect value for cost!";
        String actual = validator.validateCost(testValue);
        Assert.assertEquals(expected, actual);
    }
}