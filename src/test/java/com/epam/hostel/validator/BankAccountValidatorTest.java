package com.epam.hostel.validator;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

public class BankAccountValidatorTest {
    private static BankAccountValidator validator;

    @BeforeClass
    public static void initValidator() {
        validator = new BankAccountValidator();
    }

    @Test
    public void validateAccountNumberCorrectValueTest() {
        String testValue = "20900214";
        String expected = "";
        String actual = validator.validateAccountNumber(testValue);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void validateAccountNumberNullValueTest() {
        String testValue = null;
        String expected = "Bank account must contains only 8 digits!";
        String actual = validator.validateAccountNumber(testValue);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void validateAccountNumberNegativeValueTest() {
        String testValue = "-7281791";
        String expected = "Bank account must contains only 8 digits!";
        String actual = validator.validateAccountNumber(testValue);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void validateAccountNumberNonDigitValueTest() {
        String testValue = "jay*ko";
        String expected = "Bank account must contains only 8 digits!";
        String actual = validator.validateAccountNumber(testValue);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void validateAccountNumberIncorrectCharacterCountValueTest() {
        String testValue = "9009099088812";
        String expected = "Bank account must contains only 8 digits!";
        String actual = validator.validateAccountNumber(testValue);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void validateMoneyInputRealNumberTest() {
        String testValue = "123.34";
        String expected = "";
        String actual = validator.validateMoneyInput(testValue);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void validateMoneyInputNotRealNumberTest() {
        String testValue = "3223";
        String expected = "";
        String actual = validator.validateMoneyInput(testValue);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void validateMoneyInputNullTest() {
        String testValue = null;
        String expected = "Incorrect value";
        String actual = validator.validateMoneyInput(testValue);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void validateMoneyInputNegativeNumberTest() {
        String testValue = "-4431";
        String expected = "Incorrect value";
        String actual = validator.validateMoneyInput(testValue);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void validateMoneyInputNonDigitNumberTest() {
        String testValue = "lol";
        String expected = "Incorrect value";
        String actual = validator.validateMoneyInput(testValue);
        Assert.assertEquals(expected, actual);
    }
}