package com.epam.hostel.validator;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@Suite.SuiteClasses({RoomValidatorTest.class, BankAccountValidatorTest.class})
@RunWith(Suite.class)
public class ValidatorSuite {
}
